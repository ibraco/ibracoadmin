<?php
class Customer_model extends CI_Model {
	var $customerTable = "customer";
	var $customerDeviceTable = "customerDevice";
	var $teamPlayerTable = "teamPlayer";
	var $teamTable = "teams";
	var $tournamentTable = "tournament";
	public function __construct()
	{
		$this->load->database();
		
	}
	
	public function create_customer($insertArray)
	{
		//$this->load->helper('url');	
		  $this->db->insert($this->customerTable, $insertArray);
		  $id = $this->db->insert_id();
		if($id){

		 return $id;
		}
		else{
			 return false;
		}
	}
	public function update_customer($customerId,$updateArray){
	
		$this->db->where('customerId', $customerId);
		$id = $this->db->update($this->customerTable, $updateArray);
		if($id){

		 return $customerId;
		}
		else{
			 return $id;
		}
	}
	public function update_customerByUnit($unitNumber,$updateArray){
	
		$this->db->where('unitNumber', $unitNumber);
		$id = $this->db->update($this->customerTable, $updateArray);
		if($id){

		 return true;
		}
		else{
			 return $id;
		}
	}
	public function delete_customer($customerId){
		$this->db->where('customerId', $customerId);
		return $this->db->delete($this->customerTable);
	}
	
	
	public function get_customers($searchedValue= FALSE,$offset= null,$limit= null,$orderby = FALSE,$order=FALSE,$count='')
	{

		$this->db->select('*');
         $this->db->from($this->customerTable);
		if($searchedValue)
		{	

		  $this->db->where(" ( customerName LIKE '%$searchedValue%' OR customerEmail LIKE '%$searchedValue%' OR unitNumber LIKE '%$searchedValue%' OR IC_no LIKE '%$searchedValue%' OR customerMobile LIKE '%$searchedValue%' OR customerStatus LIKE '%$searchedValue%' OR customerCreatedDate LIKE '%$searchedValue%' )");
		}
		$this->db->where('customerStatus !=', 'delete');
		  if($limit)
            {
            	$this->db->limit($limit, $offset); 
            } 
		 if($orderby){
            	$this->db->order_by($orderby,$order);
            } 
          
		$q = $this->db->get();
//echo $this->db->last_query();
		
		if($q->num_rows() > 0)
		{
			if($count=='')
			{
				return $q->result_array();
			}
			else
			{
				return $q->num_rows();
			}
		}
		return array();
	}
	
	public function countAll()
		{		$this->db->where('customerStatus !=', 'delete');
		     $q = $this->db->get($this->customerTable);
			 
			 if($q->num_rows()>0)
			 {
				return $q->num_rows();
			 }
		}
		
	public function _getCustomer_by_customername($uname = FALSE){
		if($uname === FALSE)
			return FALSE;
		
		$query = $this->db->get_where($this->customerTable, array('customername' => $uname));
		return $query->row_array();
		
	}
	
	public function _getCustomer_by_phoneNumber($phoneNumber = FALSE,$countryCode = FALSE){
		if($phoneNumber === FALSE)
			return FALSE;
		if($countryCode )
			$array = array('customermobile' => $phoneNumber,'customerCountryCode'=> $countryCode,"customerStatus"=>'active');
		else
			$array =  array('customermobile' => $phoneNumber,"customerStatus"=>'active');
		$query = $this->db->get_where($this->customerTable,$array);
		//echo $this->db->last_query();exit;
		$data =  $query->row_array();
		return $data;
		
	}
	public function getCustomerByMobileNumber($customerMobile= FALSE){
		if($customerMobile === FALSE)
			return FALSE;
		
		$query = $this->db->get_where($this->customerTable, array('customerMobile' => $customerMobile,'customerStatus!='=>'delete'));
		return $query->row_array();
		
	}

	public function _isMobileNoinUse($customerCountryCode,$customerMobile,$customerId = FALSE){
		
			if($customerId){
				$query = $this->db->query('Select * from  pfr_'.$this->customerTable.' where customerCountryCode  = "'.$customerCountryCode.'" AND customerMobile = "'.$customerMobile.'" AND customerId != '.$customerId.'  AND customerStatus  in ("active","deactivate","inactive")');

			}else{
			$query = $this->db->query('Select * from  pfr_'.$this->customerTable.' where  customerMobile = "'.$customerMobile.'" AND customerStatus  in ("active","deactivate","inactive")');
			}
			if($query->num_rows() > 0){
				return  $query->row_array();
			}else{
				return FALSE;
			}
	}




	public function _getCustomer_by_customerid($customerId= FALSE){
		if($customerId === FALSE)
			return FALSE;
		
		$query = $this->db->get_where($this->customerTable, array('customerId' => $customerId,'customerStatus !='=>'delete'));
	
		if($query->num_rows() > 0){
				return  $query->row_array();
			}else{
				return FALSE;
			}
		
	}
	
		public function _getCustomer_by_facebookId($customerFacebookId= FALSE){
		if($customerFacebookId === FALSE)
			return FALSE;
		
		$query = $this->db->get_where($this->customerTable, array('customerFacebookId' => $customerFacebookId,'customerStatus !=' => 'delete'));

		return $query->row_array();
		
	}
	
		public function _getAllCustomer_by_facebookId($customerFacebookId= FALSE){
		if($customerFacebookId === FALSE)
			return FALSE;
		
		$query = $this->db->get_where($this->customerTable, array('customerFacebookId' => $customerFacebookId,'customerStatus !=' => 'delete'));

		return $query->result_array();
		
	}
	
	public function getCustomerByFacebookIdANDMobileNumber($customerFacebookId= FALSE,$customerMobile=FALSE){
		if($customerFacebookId === FALSE)
			return FALSE;
		elseif($customerMobile===FALSE)
		return FALSE;
		
		$query = $this->db->query("select * from pfr_customer where  (customerFacebookId ='". $customerFacebookId."' or customerMobile ='".$customerMobile."') and customerStatus !='delete'");

		return $query->row_array();
		
	}
	
	public function _isValid_customer($unitNumber,$pass)
	{
		$condition = array('unitNumber'=>$unitNumber, 'customerStatus !=' => 'delete' );
		$query = $this->db->get_where($this->customerTable,$condition);
		
		
		if($query->num_rows() > 0){
			$data = $query->row_array() ;
			
			if($data['customerStatus'] == 'inactive')
			{
				return  "statusInactive";

			}if($data['customerStatus'] == 'deactivate')
			{
				return  "statusDeactivate";

			}else{
				 $checked = password_verify($pass, $data['customerPassword'] );
				if ($checked) {
					  	return $data;
				} else {
						return FALSE;
				}
			
			}
		}
		return FALSE;
	}
	
	public function getCustomerByUnitNumber($unitNumber= FALSE){
		if($unitNumber === FALSE)
			return FALSE;
		
		$query = $this->db->get_where($this->customerTable, array('unitNumber' => $unitNumber,'customerStatus'=>'active'));
		
		return $query->row_array();
		
	}
	
	function check_customername($key,$customerId='')
	{
		if($id){
			$this->db->where('customerId != ',$customerId);
		}
	    $this->db->where('customername',$key);
	    $query = $this->db->get($this->customerTable);
	    if ($query->num_rows() > 0){
		return true;
	    }
	    else{
		return false;
	    }
	}
	function check_mail($key,$customerId='')
	{
		if($id){
			$this->db->where('customerId != ',$id);
		}
	    $this->db->where('email',$key);
	    $query = $this->db->get($this->customerTable);
	    if ($query->num_rows() > 0){
		return true;
	    }
	    else{
		return false;
	    }
	}
	
	public function check_oldPass($pass,$customerId){
		
		$this->db->where('customerId',$customerId);
		$this->db->where('customerStatus','active');
		$query = $this->db->get($this->customerTable);
		if ($query->num_rows() > 0){
		    $data = $query->row_array() ;
			$checked = password_verify($pass, $data['customerPassword'] );
				if ($checked) {
					  	return $data;
				} else {
					
						return FALSE;
				}
		}
		else{
		    return false;
		}
	}
	public function changePassword($customerId){
		$data = array(
			'password' => $this->input->post('password'),
		);
		$this->db->where('customerId', $id);
		return $this->db->update($this->customerTable, $data);
	}
	
	public function getAllUsers()
	{
		$this->db->select("customerId,customerName");
		$this->db->where('customerStatus','active');
		$q = $this->db->get($this->customerTable);
		if($q->num_rows()>0)
		{
			return $q->result_array();
		}
		return array();
	}
	
	function getUserDataBySocialId($key,$coulumn='')
	{
		if($coulumn&&$key){
			$this->db->where($coulumn. '= ',$key);
		
	    $this->db->where('customerStatus!=','delete');
	    $query = $this->db->get($this->customerTable);
		//echo $this->db->last_query();
	    if ($query->num_rows() > 0){
		return $query->row_array();
	    }
	    else{
		return array();
	    }
		}
		else{
		return array();
	    }
	}
	
	public function addTeamPlayer($array)
	{
		$this->db->insert($this->teamPlayerTable,$array);
		return $this->db->insert_id();
	}
	
	public function checkCustomerBYIC($ic_no,$id='')
	{
		
		if($ic_no)
		{
			$this->db->where('IC_no',$ic_no);
			if($id)
			$this->db->where('customerId!=',$id);
			
			$this->db->where('customerStatus!=','delete');
			$q= $this->db->get($this->customerTable);
			
			//echo $this->db->last_query();exit;
			if($q->num_rows()>0)
			{
				return $q->row_array();
			}
			
			
		}
		return array();
	}
	
	public function checkCustomerBYMobile($mobile,$id='')
	{
		
		if($mobile)
		{
			$this->db->where('customerMobile',$mobile);
			if($id)
			$this->db->where('customerId!=',$id);
			
			$this->db->where('customerStatus!=','delete');
			$q= $this->db->get($this->customerTable);
			
			//echo $this->db->last_query();exit;
			if($q->num_rows()>0)
			{
				return $q->row_array();
			}
			
			
		}
		return array();
	}
	
	public function checkAttendance($qrcode)
	{
		$output['status'] = FALSE;
		$output['message'] = 'No record found.';
		if($qrcode)
		{
			$this->db->select('*');
			$this->db->from($this->teamTable.' t');
			$this->db->join($this->tournamentTable.' to','t.tournamentId=to.tournamentId','inner');
			$this->db->where('t.qrcode',$qrcode);
			//$this->db->where('t.checkinStatus',0);
			$q = $this->db->get();
			if($q->num_rows()>0)
			{
				$data = $q->row_array();
				
				if($data['checkinStatus']==1)
				{
					$output['status'] = FALSE;
					$output['message'] = 'This player is already checkin.';
				}elseif(strtotime($data['startDate'])!=strtotime(date('Y-m-d')))
				{
					$output['status'] = FALSE;
					$output['message'] = 'You can checkin only on same date.';
				}elseif($data['checkinStatus']==0)
				{
					$this->db->update($data['teamId'],array('checkinStatus'=>1));
					$output['status'] = TRUE;
					$output['message'] = 'You can checkin only on same date.';
				}
				
				
			}
			
		}
		echo json_encode($output);
	}
	
	public function get_customersAttendance($searchedValue= FALSE,$offset= null,$limit= null,$orderby = FALSE,$order=FALSE,$count='')
	{

		$this->db->select('*');
         $this->db->from($this->tournamentTable.' to');
		 $this->db->join($this->teamTable.' t','to.tournamentId=t.tournamentId','inner');
		 
		 $this->db->where('t.teamStatus','active');
		 //$this->db->where('t.paymentStatus','success');
		 $this->db->where('to.startDate',date('Y-m-d'));
		if($searchedValue)
		{	

		  $this->db->where(" ( tournamentName LIKE '%$searchedValue%' OR teamName LIKE '%$searchedValue%' OR gameType LIKE '%$searchedValue%' OR category LIKE '%$searchedValue%' )");
		}
		
		  if($limit)
            {
            	$this->db->limit($limit, $offset); 
            } 
		 if($orderby){
            	$this->db->order_by($orderby,$order);
            } 
          
		$q = $this->db->get();
//echo $this->db->last_query();
		
		if($q->num_rows() > 0)
		{
			if($count=='')
			{
				return $q->result_array();
			}
			else
			{
				return $q->num_rows();
			}
		}
		return array();
	}
	
	public function get_customersPayment($searchedValue= FALSE,$offset= null,$limit= null,$orderby = FALSE,$order=FALSE,$count='')
	{

		$this->db->select('*');
         $this->db->from($this->teamTable.' t');
		 $this->db->join($this->tournamentTable.' to','to.tournamentId=t.tournamentId','inner');
		 
		 $this->db->where('t.teamStatus','active');
		 $this->db->where('to.tournamentStatus','active');
		 
		if($searchedValue)
		{	

		  $this->db->where(" ( to.tournamentName LIKE '%$searchedValue%' OR t.teamName LIKE '%$searchedValue%' OR t.gameType LIKE '%$searchedValue%' OR t.category LIKE '%$searchedValue%'  OR t.paymentType LIKE '%$searchedValue%')");
		}
		
		  if($limit)
            {
            	$this->db->limit($limit, $offset); 
            } 
		 if($orderby){
            	$this->db->order_by($orderby,$order);
            } 
          
		$q = $this->db->get();
//echo $this->db->last_query();
		
		if($q->num_rows() > 0)
		{
			if($count=='')
			{
				return $q->result_array();
			}
			else
			{
				return $q->num_rows();
			}
		}
		return array();
	}
	
	public function getPaymentDetais($teamId)
	{
		if($teamId)
		{
			$this->db->select('t.*,to.tournamentName,c.customerName');
			$this->db->from($this->teamTable.' t');
			$this->db->join($this->tournamentTable.' to','to.tournamentId=t.tournamentId','inner');
			$this->db->join($this->customerTable.' c','c.customerId=t.leaderId','inner');
			$this->db->where('teamId',$teamId);
			$this->db->where('t.teamStatus','active');
			$this->db->where('to.tournamentStatus','active');
			$q = $this->db->get();
			
			if($q->num_rows()>0)
			{
				$data = $q->row_array();
				$this->db->where('teamId',$data['teamId']);
				$q1 = $this->db->get('paymentDetails');
				if($q1->num_rows()>0)
				{
					$data['paymentDetails'] = $q1->row_array();
				}
				else
				{
					$data['paymentDetails']='';
				}
				return $data;
			}
		}
		return array();
		
	}
	
	public function getCustomerDetailsById($customerId)
	{
		if($customerId)
		{
			$this->db->where('customerId',$customerId);
			$q = $this->db->get($this->customerTable);
			if($q->num_rows()>0)
			{
				return $q->row_array();
			}
			
		}
		return array();
	}
	
	public function checkCustomerEmail($email,$id='')
	{
		if($id)
		$this->db->where('customerId!=',$id);
		if($email)
		$this->db->where('customerEmail',$email);
		
		$this->db->where('customerStatus!=','delete');
		$q = $this->db->get($this->customerTable);
		if($q->num_rows()>0)
		{
			return $q->result_array();
		}
		return array();
		
	}
	
	public function getAllCustomersList()
	{
		$this->db->select('*');
		$this->db->from($this->customerTable.' c');
		
		$this->db->where('c.customerStatus','active');
		$q = $this->db->get();
		if($q->num_rows()>0)
		{
			return $q->result_array();
		}
		return array();
		
	}
	
	
	public function addHandoverAppoinment($array)
	{
		if($array)
		{
			$this->db->insert('appointments',$array);
			return $this->db->insert_id();
		}
		return false;
	}
	
	public function updateHandoverAppoinment($appointmentId,$array)
	{
		if($array)
		{
			$this->db->where('appointmentId',$appointmentId);
			return $this->db->update('appointments',$array);
			
		}
		
	}

	public function updateHandoverAppoinmentByParentId($appointmentId,$array)
	{
		if($array)
		{
			$this->db->where('parentId',$appointmentId);
			return $this->db->update('appointments',$array);
			
		}
		
	}
	
	public function getCustomerAppointmentList($customerId)
	{
		if($customerId)
		{
			$this->db->where('customerId',$customerId);
			$this->db->where('appointmentStatus!=','jointinspection');
			$this->db->where('multidefect',0);
			$this->db->order_by('appointmentId','desc');
			$q = $this->db->get('appointments');
			if($q->num_rows()>0)
			{
				$data= $q->result_array();
				foreach($data as &$val)
				{
					$this->db->where('appointmentId',$val['appointmentId']);
					$this->db->where('appointmentCommentStatus','active');
					$this->db->order_by('appointmentCommentId','desc');
					$q1 = $this->db->get('appointmentComments');
					if($q1->num_rows()>0)
					{
						 $data1 = $q1->result_array();
						foreach($data1 as &$vall)
						{
							$this->db->where('commentId',$vall['appointmentCommentId']);
							$q2 = $this->db->get('appointmentCommentImage');
							if($q2->num_rows()>0)
							{
								$vall['commentImage'] = $q2->result_array();
							}
						}
						
						
					}else
					{
						$data1=array();
					}
					$this->db->where('appointmentId',$val['appointmentId']);
					$sql = $this->db->get('log');
					if($sql->num_rows()>0)
					{
						$data2 = $sql->result_array();
					}else
					{
						$data2 = array();
					}
					$mergeArray = array_merge($data1,$data2);
			
					usort($mergeArray, function($a,$b){
						return strtotime($b["addedOn"]) - strtotime($a["addedOn"]);
						});
					$val['commentDetails'] = $mergeArray;
					
					$this->db->where('departmentEmailId',$val['contractorId']);
					$q21 = $this->db->get('departmentEmail');
					if($q21->num_rows()>0)
					{
						$contratorDetails = $q21->row_array();
						$val['contractorName'] = $contratorDetails['contactName'];
					}else
					{
						$val['contractorName'] ='';
					}
				
					
					if($val['parentId']>0)
					{
						$this->db->where('customerId',$customerId);
						$this->db->where('appointmentId',$val['parentId']);
						$this->db->where('multidefect',0);
						$this->db->order_by('appointmentId','desc');
						$q3 = $this->db->get('appointments');
						if($q3->num_rows()>0)
						{
							$val['parentAppointment']= $q3->result_array();
							foreach($val['parentAppointment'] as &$vall)
							{
								$this->db->where('appointmentId',$vall['appointmentId']);
								$this->db->where('appointmentCommentStatus','active');
								$this->db->order_by('appointmentCommentId','desc');
								$q4 = $this->db->get('appointmentComments');
								if($q4->num_rows()>0)
								{
									 $data11 = $q4->result_array();
									foreach($data11 as &$valll)
									{
										$this->db->where('commentId',$valll['appointmentCommentId']);
										$q5 = $this->db->get('appointmentCommentImage');
										if($q5->num_rows()>0)
										{
											$valll['commentImage'] = $q5->result_array();
										}
									}
									
									
								}
								else
								{
									$data11=array();
								}
								$this->db->where('appointmentId',$vall['appointmentId']);
								$sql21 = $this->db->get('log');
								if($sql21->num_rows()>0)
								{
									$data21 = $sql21->result_array();
								}else
								{
									$data21 = array();
								}
								$mergeArray1 = array_merge($data11,$data21);
						
								usort($mergeArray1, function($a,$b){
									return strtotime($b["addedOn"]) - strtotime($a["addedOn"]);
									});
								
								$vall['commentDetails'] = $mergeArray1;
							}
						}
					
					}
				}
				
				
				return $data;
			}
		}
		return array();
	}
	
	public function _getAppointmentById($appointmentId)
	{
		if($appointmentId)
		{
			$this->db->where('appointmentId',$appointmentId);
			
			$q = $this->db->get('appointments');
			if($q->num_rows()>0)
			{
				$data=  $q->row_array();
				$sql = $this->db->get_where('customer',array('customerId'=>$data['customerId'],'customerStatus'=>'active'));
				$data['customerDetails'] = $sql->row_array();
				
				$this->db->where('departmentEmailId',$data['contractorId']);
				$q21 = $this->db->get('departmentEmail');
				if($q21->num_rows()>0)
				{
					$contratorDetails = $q21->row_array();
					$data['contractorName'] = $contratorDetails['contactName'];
				}else
				{
					$data['contractorName'] ='';
				}
				return $data;
				
			}
		}
		return array();
	}
	
	public function addAppointmentComment($array)
	{
		$this->db->insert('appointmentComments',$array);
		return $this->db->insert_id();
	}
	
	public function appointmentFile($array)
	{
		$this->db->insert('appointmentCommentImage',$array);
		return $this->db->insert_id();
	}
	
	public function _getAppointmentCommentsDetails($appointmentId)
	{
		if($appointmentId)
		{
			$this->db->where('appointmentId',$appointmentId);
			$this->db->where('appointmentCommentStatus','active');
			$this->db->order_by('appointmentCommentId','desc');
			$q = $this->db->get('appointmentComments');
			if($q->num_rows()>0)
			{
				$data = $q->result_array();
				foreach($data as &$val)
				{
					$this->db->where('commentId',$val['appointmentCommentId']);
					$q = $this->db->get('appointmentCommentImage');
					if($q->num_rows()>0)
					{
						$val['commentImage'] = $q->result_array();
					}
				}
				
				
			}else{
				$data = array();
			}
			
			$this->db->where('appointmentId',$appointmentId);
			$sql = $this->db->get('log');
			if($sql->num_rows()>0)
			{
				$data2 = $sql->result_array();
			}else
			{
				$data2 = array();
			}
			$mergeArray = array_merge($data,$data2);
	
			usort($mergeArray, function($a,$b){
				return strtotime($b["addedOn"]) - strtotime($a["addedOn"]);
				});
			
			
			
			return $mergeArray;
		}
		return array();
	}
	
	public function _getAppointmentByDate($appointmentDate)
	{
		if($appointmentDate)
		{
			$this->db->select('*,ap.addedOn as creationDate');
			$this->db->from('appointments ap');
			$this->db->join('customer c','c.customerId=ap.customerId','inner');
			$this->db->join('departmentEmail dp','dp.departmentEmailId=ap.contractorId','left');
			$this->db->where('date_format(ap.appointmentDate,"%Y-%m-%d")',$appointmentDate);
			$this->db->where('ap.multidefect',0);
			
			$this->db->where('ap.appointmentStatus!=','jointinspection');
			$this->db->order_by('ap.appointmentId','desc');
			$q = $this->db->get();
			if($q->num_rows()>0)
			{
				$data= $q->result_array();
				foreach($data as &$val)
				{
					$this->db->where('appointmentId',$val['appointmentId']);
					$this->db->where('appointmentCommentStatus','active');
					$this->db->order_by('appointmentCommentId','desc');
					$q1 = $this->db->get('appointmentComments');
					if($q1->num_rows()>0)
					{
						 $data1 = $q1->result_array();
						foreach($data1 as &$vall)
						{
							$this->db->where('commentId',$vall['appointmentCommentId']);
							$q2 = $this->db->get('appointmentCommentImage');
							if($q2->num_rows()>0)
							{
								$vall['commentImage'] = $q2->result_array();
							}
						}
						
						
					}else{
						$data1 = array();
					}
					$this->db->where('appointmentId',$val['appointmentId']);
					$sql = $this->db->get('log');
					if($sql->num_rows()>0)
					{
						$data2 = $sql->result_array();
					}else
					{
						$data2 = array();
					}
					$mergeArray = array_merge($data1,$data2);
			
					usort($mergeArray, function($a,$b){
						return strtotime($b["addedOn"]) - strtotime($a["addedOn"]);
						});
					$val['commentDetails'] = $mergeArray;
					if($val['parentId']>0)
					{
						//$this->db->where('customerId',$customerId);
						$this->db->where('appointmentId',$val['parentId']);
						$this->db->where('multidefect',0);
						$this->db->order_by('appointmentId','desc');
						$q3 = $this->db->get('appointments');
						if($q3->num_rows()>0)
						{
							$val['parentAppointment']= $q3->result_array();
							foreach($val['parentAppointment'] as &$vall)
							{
								$this->db->where('appointmentId',$vall['appointmentId']);
								$this->db->where('appointmentCommentStatus','active');
								$this->db->order_by('appointmentCommentId','desc');
								$q4 = $this->db->get('appointmentComments');
								if($q4->num_rows()>0)
								{
									$vall['commentDetails'] = $data1 = $q4->result_array();
									foreach($vall['commentDetails'] as &$valll)
									{
										$this->db->where('commentId',$valll['appointmentCommentId']);
										$q5 = $this->db->get('appointmentCommentImage');
										if($q5->num_rows()>0)
										{
											$valll['commentImage'] = $q5->result_array();
										}
									}
									
									
								}
							}
						}
					
					}
				}
				
				
				return $data;
			}
		}
		return array();
	}
	public function insertLog($data)
	{
		$this->db->insert('log',$data);
		return $this->db->insert_id();
	}
	
	public function saveNotification($array)
	{
		$this->db->insert('notification',$array);
		return $this->db->insert_id();
	}
	
	public function checkAppointmentTime($appointmentTime,$appoinmentDate,$manpower)
	{
		if($appointmentTime)
		{
			$this->db->where('appointmentTime',$appointmentTime);
			$this->db->where('appointmentDate',$appoinmentDate);
			$q = $this->db->get('appointments');
			
			
			if($q->num_rows()>0)
			{
				if($manpower>$q->num_rows())
				{
					return false;
				}else
				{
					return true;
				}
			}
			else
			{ 
				return false;
			}
			
		}
	}
	
	public function getParentAppointmentDetails($appointmentId)
	{
		if($appointmentId)
		{
			$this->db->where('appointmentId',$appointmentId);
			$this->db->order_by('appointmentId','desc');
			$q = $this->db->get('appointments');
			if($q->num_rows()>0)
			{
				$result = $q->row_array();
				
					$data = array();
					if($result['parentId']>0)
					{
						//$this->db->where('customerId',$customerId);
						$this->db->where('appointmentId',$result['parentId']);
						$this->db->where('multidefect',0);
						$this->db->order_by('appointmentId','desc');
						$q3 = $this->db->get('appointments');
						if($q3->num_rows()>0)
						{
							$data= $q3->result_array();
							foreach($data as &$vall)
							{
								$this->db->where('appointmentId',$vall['appointmentId']);
								$this->db->where('appointmentCommentStatus','active');
								$this->db->order_by('appointmentCommentId','desc');
								$q4 = $this->db->get('appointmentComments');
								if($q4->num_rows()>0)
								{
									 $data11 = $q4->result_array();
									foreach($data11 as &$valll)
									{
										$this->db->where('commentId',$valll['appointmentCommentId']);
										$q5 = $this->db->get('appointmentCommentImage');
										if($q5->num_rows()>0)
										{
											$valll['commentImage'] = $q5->result_array();
										}
									}
									
									
								}
								else
								{
									$data11=array();
								}
								$this->db->where('appointmentId',$vall['appointmentId']);
								$sql21 = $this->db->get('log');
								if($sql21->num_rows()>0)
								{
									$data21 = $sql21->result_array();
								}else
								{
									$data21 = array();
								}
								$mergeArray1 = array_merge($data11,$data21);
						
								usort($mergeArray1, function($a,$b){
									return strtotime($b["addedOn"]) - strtotime($a["addedOn"]);
									});
								
								$vall['commentDetails'] = $mergeArray1;
							}
						}
					return $data;
					}
				
				
				
				
			}
		}
		return array();
	}
	
	public function _getAppointmentBYHours($type)
	{
		$this->db->select('*');
		$this->db->from('appointments ap');
		$this->db->join('customer c','c.customerId=ap.customerId','inner');
		$this->db->join('departmentEmail de','de.departmentEmailId=ap.contractorId','inner');
		if($type=='24 Hours'){
			$this->db->where('ap.appointmentStatus','accept');
			$this->db->where('ap.appointmentDate = CURDATE() + INTERVAL 1 DAY');
		}elseif($type='5 Days')
		{
			$this->db->where('(ap.appointmentStatus="resolved" or ap.appointmentStatus="completed")');
			$this->db->where('date_format(ap.statusUpdatedOn,"%Y-%m-%d") = CURDATE() - INTERVAL 5 DAY');
		}
		elseif($type='10 Days')
		{
			$this->db->where('(ap.appointmentStatus="resolved" or ap.appointmentStatus="completed")');
			$this->db->where('date_format(ap.statusUpdatedOn,"%Y-%m-%d") = CURDATE() - INTERVAL 10 DAY');
		}
		elseif($type='13 Days')
		{
			$this->db->where('(ap.appointmentStatus="resolved" or ap.appointmentStatus="completed")');
			$this->db->where('date_format(ap.statusUpdatedOn,"%Y-%m-%d") = CURDATE() - INTERVAL 13 DAY');
		}
		$q = $this->db->get();
		//echo $this->db->last_query();
		if($q->num_rows()>0)
		{
			return $q->result_array();
		}
		
	}
	
	public function checkAppointmentBycustomerId($customerId)
	{
		if($customerId)
		{
			$this->db->where('customerId',$customerId);
			$this->db->where_in('appointmentStatus',array('open','accept','completed'));
			$this->db->where('appointmentType','handover');
			$this->db->order_by('appointmentId','desc');
			$q = $this->db->get('appointments');
			
			//echo $this->db->last_query();exit;
			if($q->num_rows()>0)
			{
					return true;
			}
			else
			{ 
				return false;
			}
			
		}
	}
	
	public function checkPublicHolidays($appoinmentDate,$type='')
	{
		if($appoinmentDate)
		{
			$this->db->where('holidayDate',date('Y-m-d',strtotime(str_replace('/','-',$appoinmentDate))));
			$this->db->where('holidayStatus','active');
			$q = $this->db->get('holiday');
			//echo $this->db->last_query();
			if($q->num_rows()>0)
			{
				return true;
			}else
			{
				$this->db->where('calenderType',$type);
				$this->db->where('categorySettingStatus','active');
				$q = $this->db->get('calenderSettings');
				//echo $this->db->last_query();
				if($q->num_rows()>0)
				{
					$data = $q->row_array();
					
						$days = explode(',',$data['days']);
						 $selectedDay = date('w',strtotime(str_replace('/','-',$appoinmentDate)));
						if(!in_array($selectedDay,$days))
						{
							return true;
						}else
						{
							return false;
						}
					
				}
			}
			
		}
	}
}
	
?>