<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option_model extends CI_Model {
	var $optionTable = "options";
	
	public function __construct()
	{
		$this->load->database();
		
	}
	
	public function insertOption($insertArray)
	{
		
		return $this->db->insert($this->optionTable, $insertArray);
	}
	
	public function updateOption($optionId,$updatArray){
		$this->db->where('optionId', $optionId);
		return $this->db->update($this->optionTable, $updatArray);
	}
	
	public function deleteOption($optionId){
		$this->db->where('optionId', $optionId);
		return $this->db->delete($this->optionTable);
	}
	


   public function getOptionByName($optionName){
		$this->db->select('optionValue');
   		$q = $this->db->get_where($this->optionTable, array('optionName' => $optionName, 'optionStatus =' => 'active'));
		if($q->num_rows() > 0)
		{
            $result = $q->result_array()[0]['optionValue'];
		    return $result;
		}else{
		return 0;
		}

   }
	
}
