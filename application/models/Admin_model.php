<?php
class Admin_model extends CI_model {

	var $adminTable = "admin";
	var $optionTable = "options";
	public function __construct()
	{
		$this->load->database();
		
	}
	
	public function update_admin($id,$updatArray){
		$this->db->where('admin_id', $id);
		return $this->db->update($this->adminTable, $updatArray);
	}
	public function checkUserdata($userid,$oldPass)
	{
		  $q = $this->db->get_where($this->adminTable,array('admin_id'=>$userid));
		  $userData = $q->row_array();
		$checked = password_verify($oldPass, $userData['admin_password']);
		if($checked == 1 ){
				
					return TRUE;
				}
	}
	
	public function changePassword($newPass,$id){
		$data = array(
			'admin_password' => password_hash($newPass, PASSWORD_DEFAULT, ['cost' => 11]),
		);
		$this->db->where('admin_id', $id);
		return $this->db->update($this->adminTable, $data);
	}
	
	public function _getAdmin_by_admin_email($email = FALSE){
		if($email === FALSE)
			return FALSE;
		
		$query = $this->db->get_where($this->adminTable, array('admin_email' => $email));
		return $query->row_array();
		
	}
	
	public function _getAdmin_by_admin_Id($id = FALSE){
		if($id === FALSE)
			return FALSE;
		
		$query = $this->db->get_where($this->adminTable, array('admin_id' => $id));
		return $query->row_array();
		
	}
	public function _isValid_admin()
	{

		$email = $this->input->post('email');
		$userPassword = $this->input->post('password');
		
		$condition = array('admin_email'=>$email, 'admin_status'=>'active');
		$query = $this->db->get_where($this->adminTable,$condition);
	
	//echo $this->db->last_query();exit;
		if($query->num_rows() > 0){
			$result = $query->result_array();
				
			foreach($result as $user)
			{
				$checked  = password_verify($userPassword, $user['admin_password']);
				
				if($checked == 1 ){
				
					return TRUE;
				}
			}
				
		}
		return FALSE;
	}
	
	public function insert_CalenderSettings($array)
	{
		$this->db->insert('calenderSettings',$array);
		return $this->db->insert_id();
	}
	
	public function update_CalenderSettings($calenderSettingId,$array)
	{
		$this->db->where('calenderSettingId',$calenderSettingId);
		return $this->db->update('calenderSettings',$array);
		 
	}
	
	public function getCalenderSetting()
	{
		$this->db->where('categorySettingStatus','active');
		$q = $this->db->get('calenderSettings');
		if($q->num_rows()>0)
		{
			return $q->result_array();
		}
		return array();
	}
	
	public function getCalenderSettingById($calenderSettingId)
	{
		$this->db->where('calenderSettingId',$calenderSettingId);
		$q = $this->db->get('calenderSettings');
		if($q->num_rows()>0)
		{
			return $q->row_array();
		}
		return array();
	}
	
	public function addDepartmentEmail($array)
	{
		$this->db->insert('departmentEmail',$array);
		return $this->db->insert_id();
	}
	
	public function updateDepartmentEmail($departmentEmailId,$array)
	{
		$this->db->where('departmentEmailId',$departmentEmailId);
		return $this->db->update('departmentEmail',$array);
		
	}
	public function checkDepartmentEmail($email,$id='')
	{
		if($id)
		{
			$this->db->where('departmentEmailId!=',$id);
		}
		$this->db->where('email',$email);
		$q = $this->db->get('departmentEmail');
		if($q->num_rows()>0)
		{
			return $q->result_array();
		}
		
		return false;
	}
	
	public function getDepartmentEmail($searchedValue= FALSE,$offset= null,$limit= null,$orderby = FALSE,$order=FALSE,$count='',$category='')
	{
		$this->db->where('emailStatus','active');
		if($searchedValue)
		{	

		  $this->db->where(" ( ccontactName LIKE '%$searchedValue%' OR categories LIKE '%$searchedValue%'  OR email LIKE '%$searchedValue%' )");
		}
		if($limit)
            {
            	$this->db->limit($limit, $offset); 
            } 
		 if($orderby){
            	$this->db->order_by($orderby,$order);
            }

            if($category)
            {
            	$this->db->where('categories',$category);
            }
		$q = $this->db->get('departmentEmail');
		
		if($q->num_rows() > 0)
		{
			if($count=='')
			{
				return $q->result_array();
			}
			else
			{
				return $q->num_rows();
			}
		}
		
		return array();
	}
	
	public function getDepartmentEmailById($emailId)
	{
		$this->db->where('departmentEmailId',$emailId);
		$q = $this->db->get('departmentEmail');
		if($q->num_rows()>0)
		{
			return $q->row_array();
		}
		return array();
	}
	
	public function deleteEmail($emailId)
	{
		$this->db->where('departmentEmailId',$emailId);
		$this->db->update('departmentEmail',array('emailStatus'=>'delete'));
	}
	
	public function getAdminNotificationList($count='')
	{
		$this->db->select('*');
		$this->db->from('notification n');
		$this->db->join('appointments ap','ap.appointmentId=n.appointmentId','inner');
		//$this->db->where('ap.appointmentStatus','open');
		$this->db->where('n.notificationType','admin');
		//$this->db->where('n.notificationStatus','unread');
		//$this->db->order_by('n.notificationStatus','desc');
		$this->db->order_by('n.notificationId','desc');
		$q = $this->db->get();
		if($q->num_rows()>0)
		{
			$data = $q->result_array();
			
			if($count=='count')
			{
				return $q->num_rows();
			}
			foreach($data as &$val){
			
					$this->db->where('departmentEmailId',$val['contractorId']);
					$q2 = $this->db->get('departmentEmail');
					if($q2->num_rows()>0)
					{
						$contratorDetails = $q2->row_array();
						$val['contractorName'] = $contratorDetails['contactName'];
					}else
					{
						$val['contractorName'] ='';
					}
				
			}
			
			
			
			
			
			
			return $data;
		}
		return array();
		
	}

	public function updateNotification($notificationId,$array)
	{
		if($notificationId)
		{
			$this->db->where('notificationId',$notificationId);
			$this->db->update('notification',$array);
		}
	}
	public function getAppointmentList($searchedValue= FALSE,$offset= null,$limit= null,$orderby = FALSE,$order=FALSE,$count='',$status='',$date='',$cat='',$issue='')
	{
		$this->db->select('*,ap.addedOn as creationDate');
		$this->db->from('appointments ap');
		$this->db->join('customer c','c.customerId=ap.customerId','inner');
		$this->db->join('departmentEmail d','d.departmentEmailId=ap.contractorId','left');
		$this->db->where('c.customerStatus','active');
		$this->db->where('ap.multiDefect',0);
		if($status=='pending')
		{
			$this->db->where('ap.appointmentStatus','accept');
			if($date=='today')
			{
				$this->db->where('date_format(ap.statusUpdatedOn,"%Y-%m-%d")',date('Y-m-d'));
			}elseif($date=='weekly')
			{
				$this->db->where(' YEARWEEK(ap.statusUpdatedOn) = YEARWEEK(CURDATE())');
			}elseif($date=='monthly')
			{
				$this->db->where('date_format(ap.statusUpdatedOn,"%Y-%m")',date('Y-m'));
			}elseif($date=='aging')
			{
				$this->db->where('date_format(ap.statusUpdatedOn,"%Y-%m-%d")<=CURDATE() - INTERVAL 14 DAY');
			}
			
		}elseif($status=='resolved')
		{
			$this->db->where('(ap.appointmentStatus="resolved" or ap.appointmentStatus="completed")');
			if($date=='today')
			{
				$this->db->where('date_format(ap.statusUpdatedOn,"%Y-%m-%d")',date('Y-m-d'));
			}elseif($date=='weekly')
			{
				$this->db->where(' YEARWEEK(ap.statusUpdatedOn) = YEARWEEK(CURDATE())');
			}elseif($date=='monthly')
			{
				$this->db->where('date_format(ap.statusUpdatedOn,"%Y-%m")',date('Y-m'));
			}elseif($date=='aging')
			{
				$this->db->where('date_format(ap.statusUpdatedOn,"%Y-%m-%d")<=CURDATE() - INTERVAL 14 DAY');
			}
		}elseif($status=='booked')
		{
			
			if($date=='today')
			{
				$this->db->where('date_format(ap.addedOn,"%Y-%m-%d")',date('Y-m-d'));
			}elseif($date=='weekly')
			{
				$this->db->where(' YEARWEEK(ap.addedOn) = YEARWEEK(CURDATE())');
			}elseif($date=='monthly')
			{
				$this->db->where('date_format(ap.addedOn,"%Y-%m")',date('Y-m'));
			}elseif($date=='aging')
			{
				$this->db->where('date_format(ap.addedOn,"%Y-%m-%d")<=CURDATE() - INTERVAL 14 DAY');
			}
		}else
		{
			$this->db->where('ap.appointmentStatus!=','jointinspection');
		}
		
		if($cat){

			$this->db->where('defectCategory',$cat);

		}
		if($issue)
		{
			$this->db->where('defectIssue',$issue);
		}
		if($searchedValue)
		{	

		  $this->db->where(" ( ap.ticketNo LIKE '%$searchedValue%' OR c.customerName LIKE '%$searchedValue%'  OR ap.appointmentTime LIKE '%$searchedValue%' OR c.unitNumber LIKE '%$searchedValue%' OR ap.appointmentType LIKE '%$searchedValue%' OR ap.appointmentStatus LIKE '%$searchedValue%' OR ap.defectCategory LIKE '%$searchedValue%' OR ap.defectLevel LIKE '%$searchedValue%' OR ap.defectLocation LIKE '%$searchedValue%' OR ap.defectIssue LIKE '%$searchedValue%' OR ap.appointmentStatus LIKE '%$searchedValue%' OR d.contactName LIKE '%$searchedValue%' )");
		}
		if($limit)
		{
			$this->db->limit($limit, $offset); 
		} 
		if($orderby){
			$this->db->order_by($orderby,$order);
		} 
		$q = $this->db->get();
		//echo $this->db->last_query();
		if($q->num_rows()>0)
		{
			if($count=='')
			{
				$data =  $q->result_array();
				//foreach($data as &$val)
				//{
				//	$this->db->where('departmentEmailId',$val['contractorId']);
				//	$q1 = $this->db->get('departmentEmail');
				//	if($q1->num_rows()>0)
				//	{
				//		$contratorDetails = $q1->row_array();
				//		$val['contractorName'] = $contratorDetails['contactName'];
				//	}else
				//	{
				//		$val['contractorName'] ='';
				//	}
				//}
				return $data;
			}
			else
			{
				return $q->num_rows();
			}
		}
		
		return array();
	}
	
	public function getAppointmentCount($date,$type='',$status='')
	{
		if($type=='today'){
		$this->db->where('date_format(addedOn,"%Y-%m-%d")',$date);
		}elseif($type=='thisweek')
		{
			$this->db->where(' YEARWEEK(addedOn) = YEARWEEK(CURDATE())');
		}elseif($type=='resolved')
		{
			$this->db->where('appointmentStatus','resolved');
		}
		elseif($type=='pending')
		{
			$this->db->where('appointmentStatus','accept');
		}
		$q = $this->db->get('appointments');
		
			return $q->num_rows();
		
		
	}
	
	public function getappointmentByDate($date)
	{
		if($date)
		$this->db->where('date_format(addedOn,"%Y-%m")',$date);
		$this->db->where('multiDefect',0);
		$this->db->where('appointmentStatus!=','jointinspection');
		$this->db->order_by('appointmentId','desc');
		$q = $this->db->get('appointments');
		//echo $this->db->last_query();
		if($q->num_rows()>0)
		{
			$data =  $q->result_array();
			$result = array();
			foreach($data as $val){
			$time = explode(' - ',$val['appointmentTime']);
			$startTime = $time[0];
			$endTime = isset($time[1])?$time[1]:0;
			$record['id'] = $val['appointmentId'];
			$record['title'] = $val['appointmentType']=='handover'?'Handover':'Defect';
			$record['start'] = date('Y-m-d H:i:s',strtotime($val['appointmentDate'].' '.$startTime));
			$record['end'] = date('Y-m-d H:i:s',strtotime($val['appointmentDate'].' '.$endTime));
			$record['className'] = 'bg-'.$val['appointmentStatus'];
			$record['appointmentDate'] = $val['appointmentDate'];
			
			$result[] = $record;
			}
			
			return $result;
		}
	}
	
	public function getAllUnitNumber($unitNumber='')
	{
		
		$this->db->select('DISTINCT(unitNumber),customerId,customerName');
		if($unitNumber)
		{
			$this->db->where('unitNumber',$unitNumber);
		}
		else
		{
			$this->db->where('unitNumber!=""');
		}
		$q = $this->db->get('customer');
		if($q->num_rows()>0)
		{
			$data= $q->result_array();
			foreach($data as $val)
			{
				$record[$val['customerId']] = $val;
			}
			
			return $record;
		}
		return array();
	}
	
	public function getCalenderSettings($type='')
	{
		$this->db->where('calenderType',$type);
		$this->db->where('categorySettingStatus','active');
		$q = $this->db->get('calenderSettings');
		if($q->num_rows()>0)
		{
			return $q->row_array();
		}
		
		return array();
		
	}
	
	public function getNotificationCount()
	{
		$this->db->select('*');
		$this->db->from('notification n');
		$this->db->join('appointments ap','ap.appointmentId=n.appointmentId','inner');
		$this->db->where('ap.appointmentStatus','open');
		$this->db->where('n.notificationType','admin');
		$this->db->order_by('n.notificationId','desc');
		$q = $this->db->get();
		if($q->num_rows()>0)
		{
				return $q->num_rows();
		}
	}
	
	public function getPublicHoliday()
	{
		$this->db->where('holidayStatus','active');
		$q = $this->db->get('holiday');
		if($q->num_rows()>0)
		{
			return $q->result_array();
		}
	}
	
	public function deleteCommentImage($id)
	{
		if($id)
		{
			$this->db->where('appointmentCommentImageId',$id);
			$this->db->delete('appointmentCommentImage');
			return true;
		}
		
		return false;
	}
}
?>