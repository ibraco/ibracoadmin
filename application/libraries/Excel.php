<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * CodeIgniter Push Notification for iOS Library
 *
 * A CodeIgniter library to interact with apns
 *
 * @package         CodeIgniter
 * @category        Libraries
 * @author          Rishiraj

 */
require_once APPPATH."/libraries/PHPExcel.php"; 
 
class Excel extends PHPExcel { 
    public function __construct() { 
        parent::__construct(); 
    } 
}