<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['appointment']['upload_path'] ='./uploads/appointment/';
$config['appointment']['allowed_types'] = 'gif|jpg|png|jpeg';
$config['appointment']['max_size']	= '20048';
$config['appointment']['overwrite']	= TRUE;
$config['appointment']['max_width'] = '10024';
$config['appointment']['max_height'] = '10768';
//$config['customer']['encrypt_name'] = TRUE;

$config['asd']['upload_path'] ='./uploads/asdprofile/';
$config['asd']['allowed_types'] = 'gif|jpg|png|jpeg';
$config['asd']['max_size']	= '2048';
$config['asd']['overwrite']	= TRUE;
$config['asd']['max_width'] = '1024';
$config['asd']['max_height'] = '768';


$config['claimVideo']['upload_path'] ='uploads/video/';
$config['claimVideo']['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc';
$config['claimVideo']['max_size']	= '2120';
$config['claimVideo']['overwrite']	= TRUE;

$config['paymentImage']['upload_path'] ='uploads/tournament/payment/';
$config['paymentImage']['allowed_types'] = 'gif|jpg|png|jpeg|pdf|doc';
$config['paymentImage']['max_size']	= '2120';
$config['paymentImage']['overwrite']	= TRUE;



$config['promotionImage']['upload_path'] ='./uploads/promotion/';
$config['promotionImage']['allowed_types'] = 'gif|jpg|png|jpeg';
$config['promotionImage']['max_size']	= '5120';
$config['promotionImage']['overwrite']	= FALSE;


?>