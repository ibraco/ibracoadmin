<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'customer/login';
$route['admin/calendarView'] = 'admin/calenderView';
$route['admin/tenant'] = 'customer/userList';
$route['admin/ajaxculist'] = 'customer/ajaxCustomerListRequest';
$route['admin/addCustomer'] = 'customer/addCustomer';
$route['admin/submitCustomer'] = 'customer/submitCustomer';
$route['admin/removeTenant'] = 'customer/customerAction';
$route['admin/importTenant'] = 'admin/importTenant';
$route['admin/uploadTenantData'] = 'admin/uploadTenantData';
$route['admin/viewTenant/(:any)'] = 'admin/viewTenant/(:any)';
$route['admin/settings'] = 'admin/settings';
$route['admin/calendarSetting'] = 'admin/calenderSetting';
$route['admin/updateSettings'] = 'admin/updateSettings';
$route['admin/addEmail'] = 'admin/addEmail';
$route['admin/addDepartmentEmail'] = 'admin/addDepartmentEmail';
$route['admin/ajaxemallist'] = 'admin/ajaxemallist';
$route['admin/removeEmail'] = 'admin/removeEmail';
$route['admin/editAppointment/(:any)'] = 'admin/editAppointment/(:any)';
$route['admin/submitHandoverRequest'] = 'customer/submitHandoverRequest';
$route['admin/updateHandover/(:any)'] = 'admin/submitHandoverRequest/(:any)';
$route['admin/rejectAppointment'] = 'admin/rejectAppointment';
$route['admin/acceptAppointment/(:any)/(:any)'] = 'admin/appointmentAction/(:any)/(:any)';
$route['admin/rejectAppointment/(:any)/(:any)'] = 'admin/appointmentAction/(:any)/(:any)';
$route['admin/appointmentList'] = 'admin/appointmentList';
$route['admin/getappointmentEvent'] = 'admin/getappointmentEvent';
$route['admin/getAppointmentDetails'] = 'admin/getAppointmentDetails';
$route['admin/editDefectAppointment/(:any)'] = 'admin/editDefectAppointment/(:any)';
$route['admin/notification'] = 'admin/notification';
$route['admin/handover'] = 'admin/handover';
$route['admin/addHandoverRequest'] = 'admin/addHandoverRequest';
$route['admin/excelLoad'] = 'admin/excelLoad';
$route['admin/sendEmail'] = 'admin/sendEmail';
$route['admin/defect'] = 'admin/defect';
$route['admin/defectList'] = 'customer/defectList';
$route['admin/submitDefectRequest'] = 'admin/submitDefectRequest';
$route['admin/deleteCommentImage'] = 'admin/deleteCommentImage';
$route['admin/logout'] = 'admin/logout';
$route['admin/login'] = 'admin/login';
$route['admin'] = 'admin/login';



$route['customer/handover'] = 'customer/handover';
$route['customer/editAppointment/(:any)'] = 'customer/editHandover/(:any)';
$route['customer/updateHandover/(:any)'] = 'customer/editHandover/(:any)';
$route['customer/dashboard'] = 'customer/dashboard';
$route['customer/profile'] = 'customer/profile';
$route['customer/editprofile'] = 'customer/editprofile';
$route['customer/changePassword'] ='customer/changePassword';
$route['customer/resetpassword'] = 'customer/resetpassword';
$route['customer/submitHandoverRequest'] = 'customer/submitHandoverRequest';
$route['customer/deleteAppointment'] = 'customer/deleteAppointment';
$route['customer/forgotpasswprd'] = 'customer/forgotpassword';
$route['customer/defect'] = 'customer/defect';
$route['customer/defectList'] = 'customer/defectList';
$route['customer/addCategory'] = 'customer/addCategory';
$route['customer/editDefectAppointment/(:any)'] = 'customer/editDefectAppointment/(:any)';
$route['customer/updateDefectHandover/(:any)'] = 'customer/editDefectAppointment/(:any)';
$route['customer/remindAppointment'] = 'customer/remindAppointment';
$route['customer/appointmentAction/(:any)/(:any)'] = 'admin/appointmentAction/(:any)/(:any)';
$route['customer/logout'] = 'customer/logout';
$route['customer/login'] = 'customer/login';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
