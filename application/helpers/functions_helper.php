<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

function _print_r($array){
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}
	
function custom_error($error_code = 404){
		
		$error['code'] = $error_code;
		
		switch($error_code){
		
		case 601:
		$error['message'] = "Your account has been deactivated.";
		break;
		
		case 602:
		$error['message'] ="Login credentials are not valid. Please try again.";
		break;
		
		case 603:
		$error['message'] ="Your account is not yet verified. Please register again.";
		break;

		case 604:
		$error['message'] =	"Your account has been deactivated or inactive.";
		break;
		
		case 605:
		$error['message'] =	"Mobile number and password is required.";
		break;

		case 606:
		$error['message'] =	"You reached maximum limit of attempt. Please try again tomorrow.";
		break;
	
		case 607:
		$error['message'] =	"Sending OTP to this mobile number has been failed. Please try again.";
		break;
		
		case 608:
		$error['message'] =	"The mobile number is already registered.";
		break;

		case 609:
		$error['message'] =	"OTP is required.";
		break;

		
		case 610:
		$error['message'] =	"Invalid action.";
		break;
	
		case 611:
		$error['message'] =	"Invalid OTP.";
		break;
		
		case 612:
		$error['message'] =	"OTP has been expired.";
		break;

		case 613:
		$error['message'] =	"Maximum limit reached please try again tommorrow.";
		break;

		case 614:
		$error['message'] =	"Name is required.";
		break;

		case 615:
		$error['message'] =	"Password is required.";
		break;

		case 616:
		$error['message'] =	"Mobile number is required.";
		break;

		case 620:
		$error['message'] = "No court is available on this time slot. Please select another time.";
		break;

		case 621:
		$error['message'] = "All courts have been booked for this time slot. Please select another time slot.";
		break;

		case 622:
		$error['message'] = "Invalid data request. Please login";
		break;

		case 623:
		$error['message'] = "Courts in  selected date(s) are not available.";
		break;
		case 624:
		$error['message'] = "No record found.";
		break;
		case 625:
		$error['message'] = "You can not create game for past date.";
		break;
		case 626:
		$error['message'] = "Please enter venue name.";
		break;
		case 627:
		$error['message'] = "Please input valid start time and end time.";
		break;
		case 628:
		$error['message'] = "Please select sport type.";
		case 629:
		$error['message'] = "You already have scheduled game for this time slot.";
		break;
		case 630:
		$error['message'] = "Start time can not be less than current time for current date.";
		break;
		case 631:
		$error['message'] = "Open invitation has been full.";
		break;
		case 632:
		$error['message'] = "You have entered wrong current password.";
		break;
		case 633:
		$error['message'] = "Error connecting with facebook account.";
		break;
		case 634:
		$error['message'] = "The mobile number you entered is not found.";
		break;
		case 635:
		$error['message'] = "The new mobile number you entered is already in use.";
		break;
		case 636:
		$error['message'] = "You entered invalid reward code.";
		break;
		case 637:
		$error['message'] = "You have already redeem your reward code.";
		break;
		case 638:
		$error['message'] = "Your Friend has already accepted this game invitation.";
		break;
		case 639:
		$error['message'] = "You have already send invitation to this friend.";
		break;
		case 640:
		$error['message'] = "You have already removed your friend from this game.";
		break;
		case 641:
		$error['message'] = "Either the game has been canclelled or you have been removed from the game.";
		break;
		case 642:
		$error['message'] = "We are sorry, but your current payment method could not be processed. Please try again.";
		break;
		case 643:
		$error['message'] = "Please enter title.";
		case 644:
		$error['message'] = "You can't leave game an hour before the game start.";
		break;
		case 645:
		$error['message'] = "You are not allowed to edit running game.";
		break;
		case 646:
		$error['message'] = "You can't add friend during running game.";
		break;
		case 647:
		$error['message'] = "You can't remove friend during running game.";
		break;
		case 648:
		$error['message'] = "Password length should be more than or equal to 7 characters.";
		break;
		case 649:
		$error['message'] = "Password should be alpha numeric.";
		break;
		case 650:
		$error['message'] = "Authorization failed";
		break;
		case 651:
		$error['message'] = "Your game should not exceeed more than 8 hours.";
		break;

		case 652:
		$error['message'] = "No payment method added.";
		break;
		case 653:
		$error['message'] = "Please enter Slot Available.";
		break;
		
		case 653:
		$error['message'] = "Unknown or expired card.";
		break;


		case 654:
		$error['message'] = "No card Added.";
		break;

		case 401:
		$error['message'] = "Unauthorised Access.";
		break;
		case 400:
		$error['message'] = "Bad Request. Invalid input is provided.";

		break;
		case 500:
		$error['message'] = "Internal Server Error.";

		break;
		case 404:
		$error['message'] = "404! Not found.";

		break;
		case 701:
		$error['message'] = "This QRCode has been expired.";
		break;
		case 702:
		$error['message'] = "You have already use this QRCode.";
		break;
	
		case 801:
		$error['message'] = "Team Name is required.";
		break;
		
		case 802:
		$error['message'] = "Team Leader is required.";
		break;
		
		case 803:
		$error['message'] = "IC Number is required.";
		break;
		
		case 804:
		$error['message'] = "Telephone Number is required.";
		break;
		
		case 805:
		$error['message'] = "Payment Type is required.";
		break;
		
		case 806:
		$error['message'] = "Team Name is already exist.";
		break;
		
		case 807:
		$error['message'] = "This customer is inactive. Plz contact to Admin.";
		break;
		
		case 808:
		$error['message'] = "You can not register more then one game on same time.";
		break;
		
		case 809:
		$error['message'] = "Registration is closed for this tournament.";
		break;
		
		case 810:
		$error['message'] = "Player Name is required.";
		break;
		
		case 811:
		$error['message'] = "You can not enter less then 8 payers.";
		break;
		
		case 812:
		$error['message'] = "No file selected.";
		break;

		default:
				$error['message'] =	"Service not available.";
		}
		
		return $error;
	}

/**
 *
 * Helper Function to send email
 *
 * @param    post =>'to','subject','message'
 * @return   HTMl
 *
 */
				
function sendEmail($to, $subject, $message,$addCC='',$type='',$orderId='')
	{
		$CI =& get_instance();
		$CI->load->library('email');

		$config = Array(
		  'protocol' => 'smtp',
		  'smtp_host' => $CI->optionModel->getOptionByName('smtpHost'),//'ssl://smtp.googlemail.com',
		  'smtp_port' => $CI->optionModel->getOptionByName('smtpPort'),//"465",
		  'smtp_user' => $CI->optionModel->getOptionByName('smtpUser'),//"developergaurav1432@gmail.com", // change it to yours
		  'smtp_pass' => $CI->optionModel->getOptionByName('smtpPass'),//"developer1432", // change it to yours
		  'mailtype' => 'html',
		  'charset' => 'iso-8859-1',
		  'wordwrap' => FALSE,
		  'newline' => "\r\n"
		);
		$CI->email->initialize($config);
		$CI->email->clear(TRUE);
		$CI->load->library('email', $config);
		$CI->email->set_newline("\r\n");
		$CI->email->from("admin@ibraco-entrust.com.my",'IBRACO'); // change it to yours
		$CI->email->to($to);// change it to yours
		$CI->email->subject($subject);
		$CI->email->message(($message));
		if($addCC)
		{
			$CI->email->cc($addCC);		}

		if($CI->email->send())
		{
			//print_r($CI->email);exit;
			return TRUE;
		}
		else
		{
			//print_r($CI->email);exit;
			return FALSE;
		}


	}

	function csTeam()
	{
		//array('gaurav@chimaera.my,gauravtiwari1432@gmail.com');
		$email  = array('dewi.hamden@ibraco.com,anglina.benicy@ibraco.com,indra@ibraco.com');
		return $email;
	}

	function ICSBTeam()
	{
		$email = array('kusnah.kusairi@ibraco.com');
		return $email;
	}

	function PICTeam()
	{
		$email = array('indra@ibraco.com');
		return $email;
	}


	// function show_404(){
	// 	$this->load->view('common/loginheader');
	// 	$this->load->view('404');
	// 	$this->load->view('common/loginfooter');
	// }
	
function _showMessage($message,$status){
	$CI =& get_instance();
		if($status == 'warning')
		{
			return $CI->session->set_flashdata('msg', '<div class="alert alert-warning alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Warning! </b>'.$message.'</div>');
		}
		if($status == 'success')
		{
			return $CI->session->set_flashdata('msg', '<div class="alert alert-success alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Success! </b>'.$message.'</div>');
		}
		if($status == 'error')
		{
			return $CI->session->set_flashdata('msg', '<div class="alert alert-danger alert-dismissable">
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Error! </b>'.$message.'</div>');
		}
	}

	function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function sendNotification($playerid,$title,$message){
    
    if(!empty($playerid) && ($playerid!="")){
	 
	// Replace with real client registration IDs 
	$registrationIDs = $playerid;
	
	// Message to be sent
	$sender_userid = '';
	$sender_username='';
	
	
    
		$content = array(
			"en" => 'English Message'
			);
		
		$fields = array(
			'app_id' => "47b0deb4-7e33-44e6-a8f1-f434f96b521f",
			'include_player_ids' => array($registrationIDs),
			'data' => array("title" =>$title,'body'=>$message),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
    	//print("\nJSON sent:\n");
    	//print($fields);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
												   'Authorization: Basic MWU2ODc1NzEtMzMxYi00MGY1LWE5YWUtYTI1NmM4MGIxM2Rh'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
   }
   
 function _getAppointmentTicketNo($type,$ticketNo='')
   {
	$CI =& get_instance();
		if($type=='handover')
		{
			$CI->db->where('appointmentType','handover');
			$CI->db->where('parentId',0);
			$CI->db->order_by('appointmentId','desc');
			$q = $CI->db->get('appointments');
			
			if($q->num_rows()>0)
			{
				$handoverData = $q->row_array();
				
				 $ticketNo = $handoverData['ticketNo'];
				 $suffix = substr($ticketNo,0,2);
				 $num = (int)substr($ticketNo,2);
				$billNo = $num + 1;
				$length = 4;
				 $unifiedPin = 'H-'.''.sprintf("%'.0".$length."d",$billNo);
				return $unifiedPin;
			}else
			{
				return 'H-0001';
			}
		}elseif($type=='defect')
		{
			$CI->db->where('appointmentType','defect');
			$CI->db->where('parentId',0);
			$CI->db->order_by('appointmentId','desc');
			$q = $CI->db->get('appointments');
			if($q->num_rows()>0)
			{
				$handoverData = $q->row_array();
				$ticketNo = $handoverData['ticketNo'];
				 $suffix = substr($ticketNo,0,2);
				 $num = (int)substr($ticketNo,2);
				$billNo = $num + 1;
				$length = 4;
				$unifiedPin = 'D-'.''.sprintf("%'.0".$length."d",$billNo);
				return $unifiedPin;
			}else
			{
				return 'D-0001';
			}
		}
		elseif($ticketNo)
		{
			$suffix = substr($ticketNo,0,2);
				 $num = (int)substr($ticketNo,2);
				$billNo = $num + 1;
				$length = 6;
				$unifiedPin = 'D-'.''.sprintf("%'.0".$length."d",$billNo);
				return $unifiedPin;
		}
   }
   
    function defectList()
	{
		$typeOfDefect = array(
			array('name'=>'Roller Shutter','img'=>'RollerShutter.png'),
			array('name'=>'Doors Frame','img'=>'DoorFrame.png'),
			array('name'=>'Doors Panel','img'=>'DoorsPanel.png'),
			array('name'=>'Door Ironmongery - lock set','img'=>'DoorIronmongery-lock set.png'),
			array('name'=>'Ceiling/Sofit','img'=>'Sofit.png'),
			array('name'=>'Wall Tiles','img'=>'WallTiles.png'),
			array('name'=>'Floor Slab','img'=>'FloorSlab.png'),
			array('name'=>'External Wall','img'=>'ExternalWall.png'),
			array('name'=>'Window Frame','img'=>'WindowFrame.png'),
			array('name'=>'Window','img'=>'Window.png'),
			array('name'=>'Internal Wall Finishes','img'=>'InternalWallFinishes.png'),
			array('name'=>'Leaking','img'=>'leaking.png'),
			array('name'=>'Staircase','img'=>'Staircase.png'),
			array('name'=>'Plumbing issue','img'=>'Plumbingissue.png'),
			array('name'=>'Socket/Electrical (M & E)','img'=>'Electrical.png')
							  );
		
		return $typeOfDefect;
	}
	function defectLevel($name='')
	{
		$defectLevel = array(
							 'Roller Shutter'=>array('level'=>array('Ground Floor'),
													 'location'=>array('Front','Back','Side'),
													 'issues'=>array('Does not close / open smoothly','Cannot lock','Does not fit properly','Screw/rivet missing','Rusty / Scratch / Damage','Dented','Warping','Frame Issues')),
							 'Doors Frame'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
												  'location'=>array('Toilet','Fire Door'),
												  'issues'=>array('Cracked','Dented','Scratch','Damage','Pin Hole','Joint Gap','Peeling','Fading','Patchy','Hinges not Align','Unsatisfactory Finish')),
							 'Doors Panel'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
												  'location'=>array('Toilet','Fire Door'),
												  'issues'=>array('Cracked','Dented','Scratch','Damage','Pin Hole','Joint Gap','Peeling','Fading','Patchy','Top and Bottom not painted','Warping/Buldging','Unsatisfactory Finish')),
							 'Door Ironmongery - lock set'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
																  'location'=>array('Toilet','Fire Door'),
																  'issues'=>array('Rusted','Dented','Scratched','Damaged','Functionality Issues','Fire Door Strip damaged/shortage','Stability Issues','Unsatisfactory Finish')),
							 'Ceiling/Sofit'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
													'location'=>array(),
													'issues'=>array('Waves','Bubble holes','level-height issues','Damaged','Stain Mark','Peeling','Fading','chalking','Patchy','Towal Marks','Unsatisfactory Finishes')),
							 'Wall Tiles'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
												 'location'=>array('Toilet'),
												 'issues'=>array('Gap','Chip','Hollow','Uneven/color/size','Lippage','Crack','Finishing work unsatisfactory')),
							 'Floor Slab'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
												 'location'=>array(),
												 'issues'=>array('Crack line','Hollow/hollow spot','Uneven')),
							 'External Wall'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
													'location'=>array('Front','Back','Side'),
													'issues'=>array('Uneven','Crack','Patchy','Paint Unsatisfactory','Finishing Unsatisfactory')),
							 'Window Frame'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
												   'location'=>array('Toilet','Front','Back','Side'),
												   'issues'=>array('Joint Gap','Scratch','Stain Marks','Dented','Gasket issue','Hole')),
							 'Window'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
											 'location'=>array('Toilet','Front','Back','Side'),
											 'issues'=>array('Cannot Close properly','Hinges rusty / not smooth','Glass scratch','Handle Broken / Loose / Scratch','Crack','Stain Marks','Not Level / Not Allign','Rubber Seal / Gasket Issue','Finishing work unsatisfactory')),
							 'Internal Wall Finishes'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
															 'location'=>array('Front','Back','Side'),
															 'issues'=>array('Uneven','Stain Mark','Crack Line','Hollow','Paint Unsatisfactory','Patchy','buldging','Finishing work unsatisfactory')
															 ),
							 'Leaking'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
											  'location'=>array('Front','Back','Side'),
											  'issues'=>array('Sofit','Ceiling','Window','Wall')),
							 'Staircase'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
												'location'=>array('Front','Back'),
												'issues'=>array('Step height / depth not uniform','Landing area not level/Ponding','Tiles crack','Nosing tiles lippage/damage /crack/hollow','Hollow','Handrail rusty','Handrail loose','Slab soffit patchy','Paint Unsatisfactory','Finishing work unsatisfactory')),
							 'Plumbing issue'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
													 'location'=>array(),
													 'issues'=>array('Leaking','Joint not fit properly','Blockage','Not Allign','Functionality Issues','Pipe Bend','Damage','OverFlow Discharge Pipe')),
							 'Socket/Electrical (M & E)'=>array('level'=>array('Ground Floor','First Floor','Second Floor','Third Floor'),
																'location'=>array(),
																'issues'=>array('Position','Joint Gap','Not Level / Not Allign','Functionality Issues'))
							 
							 );
		
		if($name)
		return $defectLevel[$name];
		else
		return $defectLevel;
	}
?>