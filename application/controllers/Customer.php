<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
		$this->output->set_header('Pragma: no-cache');
		$this->load->model('Admin_model','adminModel');
		$this->load->model('Customer_model','customerModel');
		
	}
	
public function login()
{
	$data['title'] = 'Customer Login';
	if($_SERVER['REQUEST_METHOD']=='POST')
	{
		
		$this->form_validation->set_rules('unitNumber','Unit Number','trim|required');
		$this->form_validation->set_rules('password','Password','trim|required');
		if($this->form_validation->run()===TRUE)
		{
			$customerData = $this->customerModel->_isValid_customer($this->input->post('unitNumber'),$this->input->post('password'));
			if( $customerData== 'statusInactive'){
				_showMessage('Your account is inactive.','error');
				redirect('customer/login');
			}elseif($customerData===FALSE){
				
				_showMessage('User name or password is not valid.','error');
				redirect('customer/login');
			}else{
				
				$userData = $this->customerModel->getCustomerByUnitNumber($this->input->post('unitNumber'));
				
				if($userData)
				{
					$updateLastLogin = array('customerLastLogin'=>date('Y-m-d H:i:s'));
					$lastLogin = ($userData['customerLastLogin']!='0000-00-00 00:00:00')?date('M d,Y h:i a',strtotime($userData['customerLastLogin'])):date('M d,Y h:i a');
					$this->customerModel->update_customer($userData['customerId'],$updateLastLogin );
				
				
				$Data['cu'] = array('userName' => $userData['customerName'],'userId'=>$userData['customerId'],'fullName'=>$userData['customerName'], 'unitNumber'=>$userData['unitNumber'],'userEmail'=>$userData['customerEmail'],'userRole'=> 'customer','lastLogin'=>$lastLogin );
				$this->session->set_userdata($Data);	
				redirect('customer/dashboard');
				}
			}
		}
	}
	$this->load->view('customer/login',$data);
}

public function logout()
{
	$this->session->unset_userdata('cu');
		redirect('customer/login');
}
	
	public function dashboard()
	{
		if($this->session->userdata('cu')['userRole'] == "customer"){
		$data['title'] = 'Welcome';
		$data['customerData'] = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
		$data['appointmentList'] = $this->customerModel->getCustomerAppointmentList($this->session->userdata('cu')['userId']);
		//_print_r($data['appointmentList']);
		$this->template($data['title'],'dashboard',$data);
		//_print_r($data['customerData']);
		}
		else
		{
			redirect('customer/logout');
		}
	}
	
	public function handover()
	{
		if($this->session->userdata('cu')['userRole'] == "customer"){
			
			
			$data['title'] = 'Handover';
			$data['customerDetails'] = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
			$this->template($data['title'],'handover',$data);
		}
		else
		{
			redirect('customer/logout');
		}
	}
	
	public function editHandover()
	{
		if($this->session->userdata('cu')['userRole'] == "customer"){
			
			$id = $this->uri->segment(3);
			$appointmentId=str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
			$appointmentId = $this->encryption->decrypt($appointmentId);
			 
			 $appointmentDetails = $this->customerModel->_getAppointmentById($appointmentId);
			 $appointmentDetails['commentDetails'] = $this->customerModel->_getAppointmentCommentsDetails($appointmentId);
			 
			 
			 if(@$appointmentDetails['customerId']==$this->session->userdata('cu')['userId'])
			 {
				if($appointmentDetails['appointmentStatus']=='open'||$appointmentDetails['appointmentStatus']=='resolved'||$appointmentDetails['appointmentStatus']=='completed')
				$data['appointmentDetails'] = $appointmentDetails;
			 }
			 $data['emailList'] = $this->adminModel->getDepartmentEmail();
			$data['title'] = 'Handover';
			if($_SERVER['REQUEST_METHOD']=='POST')
            {
				$id = $this->uri->segment(3);
				$appointmentId=str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
				$appointmentId = $this->encryption->decrypt($appointmentId);
				$appointmentDate = $this->input->post('appointmentDate');
                $appointmentTime = $this->input->post('appointmentTime');
				$appointmentStatus = $this->input->post('appointmentStatus');
				if($appointmentId){
					
					$array = array('appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appointmentDate))),'appointmentTime'=>$appointmentTime,'appointmentStatus'=>($appointmentStatus!='')?$appointmentStatus:$appointmentDetails['appointmentStatus']);
					if($appointmentStatus!='')
					{
						$array['statusUpdatedOn'] = date('Y-m-d H:i:s'); 
					}
					$this->customerModel->updateHandoverAppoinment($appointmentId,$array);
					$customerDetails = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
					$logText = $customerDetails['unitNumber'].' has '.$appointmentStatus.' the appointment';
					if($this->input->post('appointmentStatus')=='jointinspection')
					{
						$array = array('parentId'=>$appointmentId,'customerId'=>$this->session->userdata('cu')['userId'],'appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appointmentDate))),'appointmentTime'=>$appointmentTime,'appointmentNotes'=>$appointmentDetails['appointmentNotes'],'appointmentType'=>$appointmentDetails['appointmentType'],'appointmentStatus'=>'open','defectCategory'=>$appointmentDetails['defectCategory'],'defectLevel'=>$appointmentDetails['defectLevel'],'defectLocation'=>$appointmentDetails['defectLocation'],'defectIssue'=>$appointmentDetails['defectIssue']);
						$array['ticketNo'] = $appointmentDetails['ticketNo'].'A';
						
						$insertId = $this->customerModel->addHandoverAppoinment($array);
						$logText = $customerDetails['unitNumber'].' requested for a joint inspection.';
						
						//save notification
					$notificationText = 'New '.ucfirst($appointmentDetails['appointmentType']).'  Appointment request by '.$customerDetails['unitNumber'];
					$notificationArray= array('appointmentId'=>$insertId,'notificationType'=>'tenant','notificationStatus'=>'unread','tenantId'=>$this->session->userdata('cu')['userId'],'notificationText'=>$notificationText);
					$this->customerModel->saveNotification($notificationArray);
					}
					//save log
					if($appointmentStatus!=''){
					$logArray = array('logText'=>$logText,'appointmentId'=>$appointmentId,'action'=>$appointmentStatus,'tanentId'=>$this->session->userdata('cu')['userId']);
					$this->customerModel->insertLog($logArray);
					
					
					 $subject = html_entity_decode(stripslashes('IBRACO:UPDATE APPOINTMENT'));
                   
                    $link['appoinmentDate'] = $appointmentDate;
                    $link['appoinmentTime'] = $appointmentTime;
                    $link['defect'] = $appointmentDetails['defectCategory'].'. '.$appointmentDetails['defectLevel'].'. '.$appointmentDetails['defectLocation'].'. '.$appointmentDetails['defectIssue'];
                    $customerDetails = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
                    $link['customerName'] = $customerDetails['customerName'];
                    $link['ownerName'] = $customerDetails['ownerName'];
                    $link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
                    $link['appointmentStatus'] = $appointmentStatus;
                    $message =   $this->load->view('email/statusChange',$link,true);
                    $to = $this->optionModel->getOptionByName('adminEmail');
                    $cc='';
                    if($appointmentStatus=='cancelled')
                    {
                    	$csTeam = csTeam();
                    	$ICSBTeam = ICSBTeam();
                    	$PIC = PICTeam();

                    	$cc  = array_merge($csTeam,$ICSBTeam,$PIC);
                    }
                    sendEmail($to,$subject,$message,$cc);
					}
					_showMessage('Record updated successfully.','success');
					redirect('customer/dashboard/');
				}
			}
			$this->template($data['title'],'editHandover',$data);
		}
		else
		{
			redirect('customer/logout');
		}
	}
	
	
	
	public function profile()
	{
		if($this->session->userdata('cu')['userRole'] == "customer"){
			$data['title'] = 'Profile';
			$data['customerDetails'] = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
			//_print_r($data['customerDetails']);
		$this->template($data['title'],'profile',$data);
		}
		else
		{
			redirect('customer/logout');
		}
	}
	
	public function editprofile()
	{
		if($this->session->userdata('cu')['userRole'] == "customer"){
			
			$data['customerDetails'] = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
			if($_SERVER['REQUEST_METHOD']=='POST')
			{
				$phoneNumber = $this->input->post('phoneNumber');
				$customerMobile  = preg_replace('/[^+|0-9]/s', '', $phoneNumber);
							
							   $firstletter =  substr($customerMobile,0,1);
								if( $firstletter == "0"){
									$phoneNumber = substr($customerMobile,1);
									$countryCode = substr($customerMobile,0,1);
								}else if( $firstletter == "+")
								{
									$phoneNumber = substr($customerMobile,3);
									$countryCode = substr($customerMobile,0,3);
								}else{
									$phoneNumber = $customerMobile;
									$countryCode = "0"; 
								}
				$data['customerDetails']['customerCountryCode'] = $countryCode;
				$data['customerDetails']['customerMobile'] = $phoneNumber;
				$data['customerDetails']['customerAlternatecontactName'] = $this->input->post('customerAlternatecontactName');
				$data['customerDetails']['customerAlternateMobile'] = $this->input->post('customerAlternateMobile');
				$data['customerDetails']['customerEmail'] = $this->input->post('customerEmail');
				
				$this->form_validation->set_rules('phoneNumber','Primary contact number','trim|required');
				$this->form_validation->set_rules('customerAlternatecontactName','Emergency contact','trim|required');
				$this->form_validation->set_rules('customerAlternateMobile','Emergency contact Number','trim|required');
				$this->form_validation->set_rules('customerEmail','Email','trim|required');
				if($this->form_validation->run()===TRUE)
				{
					
					$customerId = $this->session->userdata('cu')['userId'];
					$this->customerModel->update_customer($customerId,$data['customerDetails']);
					_showMessage('Record Updated Successfully','success');
					redirect('customer/profile');
				}
				
			}
			//_print_r($data['customerDetails']);
			$this->load->view('customer/editprofile',$data);
		}
		else
		{
			redirect('customer/logout');
		}
	}
	
	public function resetpassword()
	{
		if($this->session->userdata('cu')['userRole']=='customer')
		{
			if($_SERVER['REQUEST_METHOD']=='POST')
			{
				$oldPassword = $this->input->post('oldpassword');
				$newPassword = $this->input->post('newpassword');
				$cPassword = $this->input->post('cpassword');
				$this->form_validation->set_rules('oldpassword','Old Password','trim|required|callback_verifyPassword');
				$this->form_validation->set_rules('newpassword','New Password','trim|required');
				$this->form_validation->set_rules('cpassword','Confirm New Password','trim|required|matches[newpassword]');
				$customerId = $this->session->userdata('cu')['userId'];
				
				
				if($this->form_validation->run()===TRUE)
				{
					$newPassword = password_hash($newPassword, PASSWORD_DEFAULT, ['cost' => 11]);
					$array = array('customerPassword'=>$newPassword);
					$this->customerModel->update_customer($customerId,$array);
					_showMessage('Password has been changed successfully.','success');
					redirect('customer/changePassword');
					
				}
				$data['title'] = 'Profile';
			
			//_print_r($data['customerDetails']);
		$this->template($data['title'],'changePassword',$data);
			}
		}
		
	}
	
	public function verifyPassword()
	{
		$oldPass = $this->input->post('oldpassword');
		$user_id = $this->session->userdata('cu')['userId'];
		if($oldPass!=''){
		$userData= $this->customerModel->check_oldPass($oldPass,$user_id);
		if(!$userData)
		{
			$this->form_validation->set_message('verifyPassword','Old Password does not match.');
			return false;
		
		}
		else
		{
			return true;
		
		}
		}
	}
	
	public function submitHandoverRequest()
	{
		$json_data = array(
						"status"=>'fail',
						"type"=>'',
						"message"            => "Somthing went wrong. Please try again.");
		$appoinmentDate = $this->input->post('appoinmentDate');
		$appoinmentTime = $this->input->post('appoinmentTime');
		$notes = $this->input->post('notes');
		$appointmentId = $this->input->post('appointmentId');
		$calenderSettings = $this->adminModel->getCalenderSettings('handover');
		if($this->customerModel->checkAppointmentBycustomerId($this->session->userdata('cu')['userId']))
		{
			$json_data = array(
						"status"=>'fail',
						"type"=>'',
						"message" => "You can't request more than one handover request.");
		}elseif($this->customerModel->checkAppointmentTime($appointmentTime,$appointmentDate,$calenderSettings['manpower'])){

			$json_data = array(
						"status"=>'fail',
						"type"=>'',
						"message" => "This time slot is not available.");

		} elseif($this->customerModel->checkPublicHolidays($appoinmentDate,'handover'))
		{
			$json_data = array(
						"status"=>'fail',
						"type"=>'',
						"message" => "The selected date is non-working day. Please choose another date.");
		}else {
		
				$array = array('customerId'=>$this->session->userdata('cu')['userId'],'appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appoinmentDate))),'appointmentTime'=>$appoinmentTime,'appointmentNotes'=>$notes,'appointmentType'=>'handover','appointmentStatus'=>'open','statusUpdatedOn'=>date('Y-m-d H:i:s'));
				if($appointmentId==''){
					$ticketNo = _getAppointmentTicketNo('handover');
					$array['ticketNo']=$ticketNo;
				}
				if($appointmentId)
				{
					$this->customerModel->updateHandoverAppoinment($appointmentId,$array);
					$json_data = array(
								"status"=>'success',
								"type"=>'',
								"message"            => "Record updated successfully.");
				}else
				{
					
				$insertId = $this->customerModel->addHandoverAppoinment($array);
				if($insertId)
				{
					
					$subject = html_entity_decode(stripslashes('IBRACO:NEW APPOINTMENT'));
					
					$appointmentId = $this->encryption->encrypt($insertId);
					$appointmentId =str_replace(array('+', '/', '='), array('-', '_', '~'), $appointmentId);
					
					$link['accept'] = base_url('admin/editDefectAppointment/'.$insertId.'/accept');
					$link['reject'] = base_url('admin/rejectAppointment/'.$appointmentId.'/rejected');
					$link['appoinmentDate'] = $appoinmentDate;
					$link['appoinmentTime'] = $appoinmentTime;
					$link['ownerName'] = $this->session->userdata('cu')['userName'];
					$customerDetails = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
					$link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
					$message =   $this->load->view('email/Accept _ rejectemail',$link,true);
					$to = $this->optionModel->getOptionByName('adminEmail');
					$csTeam = csTeam();
                	$ICSBTeam = ICSBTeam();
                	$PIC = PICTeam();

                	$cc  = array_merge($csTeam,$ICSBTeam,$PIC);
					sendEmail($to,$subject,$message,$cc);
					
					//save log
					$logText = $customerDetails['unitNumber'].' created an appointment.';
					$logArray = array('logText'=>$logText,'appointmentId'=>$insertId,'action'=>'open','tanentId'=>$this->session->userdata('cu')['userId']);
					$this->customerModel->insertLog($logArray);
					
					//save notification
					$notificationText = 'New Handover Appointment request by '.$customerDetails['unitNumber'];
					$notificationArray= array('appointmentId'=>$insertId,'notificationType'=>'admin','notificationStatus'=>'unread','tenantId'=>$this->session->userdata('cu')['userId'],'notificationText'=>$notificationText);
					$this->customerModel->saveNotification($notificationArray);
					$json_data = array(
								"status"=>'success',
								"type"=>'',
								"message"            => "Record inserted successfully.");
					_showMessage('Record inserted successfully.','success');		
				}
				}
		}
		echo json_encode($json_data);
	}
	
	public function deleteAppointment()
	{
		$id = $this->uri->segment(3);
		$appointmentId=str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
		$appointmentId = $this->encryption->decrypt($appointmentId);
		
		$array = array('appointmentStatus'=>'cancelled');
		$this->customerModel->updateHandoverAppoinment($appointmentId,$array);
		
	}
	
	public function changePassword()
	{
		if($this->session->userdata('cu')['userRole'] == "customer"){
			$data['title'] = 'Profile';
			$data['customerDetails'] = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
			//_print_r($data['customerDetails']);
		$this->template($data['title'],'changePassword',$data);
		}
		else
		{
			redirect('customer/logout');
		}	
	}
	
	public function userList()
	{
		if($this->session->userdata('ad')['userRole'] == "admin"){
		$data['title'] = 'Users List';
		$data['notificationCount'] = $this->adminModel->getNotificationCount('count');
		$this->adminTemplate($data['title'],'users',$data);
		}
		else
		{
			redirect('admin/logout');
		}
		
	}
	
	/**
*
* User List ajax request
*@response: json
*/

	public function ajaxCustomerListRequest()
	{
		$columns = array( 
		// datatable column index  => database column name
			0 =>'customerName', 
			1 =>'unitNumber',
			2 =>'IC_no',
			3 =>'customerMobile',
			4=>'customerAlternatecontactName',
			5 =>'customerAlternateMobile',
			6 =>'customerEmail',
			7 =>'customerId'
		);
		
		$customer = array();
		$requestData= $this->input->post();
		$totalRecord = $totalFilteredRecord = $this->customerModel->countAll();

		if( !empty($requestData['search']['value']) ) {

			$data = $this->customerModel->get_customers($requestData['search']['value'] ,$requestData['start'] ,$requestData['length'], $columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir'] );
			$i=0;
			$totalFilteredRecord = $this->customerModel->get_customers($requestData['search']['value'],'','','','','count');
			foreach($data as $key=>$record){

				$customer[$i][] =$record['customerName'];
				$customer[$i][] ='<a class="hyp-link" href="'.base_url('admin/viewTenant/').$record['customerId'].'">'.$record['unitNumber'].'</a>';
				$customer[$i][] =$record['IC_no'];
				$customer[$i][] =$record['customerCountryCode'].' '.$record['customerMobile'];
				$customer[$i][] = $record['customerAlternatecontactName'];
				$customer[$i][] = $record['customerAlternateMobile'];
				$customer[$i][] =$record['customerEmail'];
				$customer[$i][] =$record['emailSent'];
				
				//if($record['customerStatus']=='active')
				//	$customer[$i][] = '<label class="badge badge-success">'.$record['customerStatus'].'</label>';
				//	if($record['customerStatus']=='inactive')
				//	$customer[$i][] = '<label class="badge badge-danger">'.$record['customerStatus'].'</label>';
				//	$customer[$i][] = $record['customerStatus'];
				$customer[$i][] =$record['customerId'];
				
				$i++;

			}
			//$totalFilteredRecord  = $i;
		
		}else{
			$data = $this->customerModel->get_customers('',$requestData['start'] ,$requestData['length'], $columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir']);
			$i=0;
			if($data ){
				foreach($data as $key=>$record){

					$customer[$i][] =$record['customerName'];
					$customer[$i][] = '<a class="hyp-link" href="'.base_url('admin/viewTenant/').$record['customerId'].'">'.$record['unitNumber'].'</a>';
					$customer[$i][] =$record['IC_no'];
					$customer[$i][] =$record['customerCountryCode'].' '.$record['customerMobile'];
					$customer[$i][] = $record['customerAlternatecontactName'];
					$customer[$i][] = $record['customerAlternateMobile'];
					$customer[$i][] =$record['customerEmail'];
					$customer[$i][] =$record['emailSent'];
					//if($record['customerStatus']=='active')
					//$customer[$i][] = '<label class="badge badge-success">'.$record['customerStatus'].'</label>';
					//if($record['customerStatus']=='inactive')
					//$customer[$i][] = '<label class="badge badge-danger">'.$record['customerStatus'].'</label>';
					//$customer[$i][] = $record['customerStatus'];
					$customer[$i][] =$record['customerId'];
					
					$i++;

				}
			}
		}
		if($customer){
			$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $totalRecord ,
                "recordsFiltered" => $totalFilteredRecord,
                "data"            => $customer
            );
		}else{
			$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $totalRecord ,
                "recordsFiltered" => 0,
                "data"            => array()
            );

		}
		echo json_encode($json_data);

	}
	
	
public function addCustomer()
{
	$customerId = $this->input->post('customerId');
	$data['customerDetails'] = $this->customerModel->getCustomerDetailsById($customerId);
	//_print_r($data['customerDetails']);
	$this->load->view('admin/addCustomer',$data);
}
	
public function submitCustomer()
{
	$customerId = $this->input->post('customerId');
	$customerName = $this->input->post('customerName');
	$ownerName = $this->input->post('ownerName');
	$unitNumber = $this->input->post('unitNumber');
	$IC_no = $this->input->post('IC_no');
	$customerMobile = $this->input->post('customerMobile');
	$customerAlternatecontactName = $this->input->post('customerAlternatecontactName');
	$customerAlternateMobile = $this->input->post('customerAlternateMobile');
	$customerEmail = $this->input->post('customerEmail');
	$authorized_person = $this->input->post('authorized_person');
	$authorized_person_contact_no = $this->input->post('authorized_person_contact_no');
	$vacant_possession_letter_date = date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('vacant_possession_letter_date'))));
	$occupation_permit_date = date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('occupation_permit_date'))));
	$handover_date = date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('handover_date'))));
	$defect_liability_expiry_date = date('Y-m-d',strtotime(str_replace('/','-',$this->input->post('defect_liability_expiry_date'))));
	$json_data = array(
						"status"=>'fail',
						"message"            =>'Please try again.'
                        );
	$customerMobile  = preg_replace('/[^+|0-9]/s', '', $customerMobile);
							
							   $firstletter =  substr($customerMobile,0,1);
								if( $firstletter == "0"){
									$phoneNumber = substr($customerMobile,1);
									$countryCode = substr($customerMobile,0,1);
								}else if( $firstletter == "+")
								{
									$phoneNumber = substr($customerMobile,3);
									$countryCode = substr($customerMobile,0,3);
								}else{
									$phoneNumber = $customerMobile;
									$countryCode = "0"; 
								}
	$array = array('customerName'=>$customerName,'unitNumber'=>$unitNumber,'IC_no'=>$IC_no,'customerMobile'=>$phoneNumber,'customerCountryCode'=>$countryCode,'customerAlternatecontactName'=>$customerAlternatecontactName,'customerAlternateMobile'=>$customerAlternateMobile,'customerEmail'=>$customerEmail,'defect_liability_expiry_date'=>$defect_liability_expiry_date,'handover_date'=>$handover_date,'occupation_permit_date'=>$occupation_permit_date,'vacant_possession_letter_date'=>$vacant_possession_letter_date,'authorized_person_contact_no'=>$authorized_person_contact_no,'authorized_person'=>$authorized_person,'ownerName'=>$ownerName);
	
	if($customerId=='')
	{
		$this->customerModel->create_customer();
		$json_data = array(
						"status"=>'success',
						"message"            =>'Record Inserted Successfully.'
                        );
	}elseif($customerId)
	{
		$this->customerModel->update_customer($customerId,$array);
		$json_data = array(
						"status"=>'success',
						"message"            =>'Record Updated Successfully.'
                        );
	}
	
	echo json_encode($json_data);
	
}
	/**
 *
 * @Function Delete company
 *
 * @params none
 * return true
 *
 */	
	

function customerAction(){

if($this->session->userdata('ad')['userRole'] == "admin"){
	$customerId = $this->input->post('customerId');
	
	if(@$customerId)
		{
			if($this->input->post('customerStatus')=='active')
			{
				$data['customerDetail'] = array('customerPasscode'=>'','customerStatus'=>'active');
			}else{
			$data['customerDetail'] = array('customerStatus' =>$this->input->post('customerStatus'));
			}
			$true = $this->customerModel->update_customer(@$customerId,$data['customerDetail']);
			
		}
	}
}

	/**
	 *
	 * View User Detail
	 *
	*/
	
public function viewCustomerDetail(){

		//$this->output->enable_profiler(TRUE);
	
	
		if('GET' == $_SERVER['REQUEST_METHOD'] && @$this->uri->segment(3) && ($this->session->userdata('ad')['userRole'] == "admin"))
		{

			$data['title'] = 'View Customer Details';
			$customerDetail = $this->customerModel->_getCustomer_by_customerid(@$this->uri->segment(3));
			//$tournamentDetails = $this->tournamentModel->getPlayerTournamentList($this->uri->segment(3));
			
			$data['customerDetail'] = $customerDetail;
			//$data['tournamentDetails'] = $tournamentDetails;
			if(!empty($data))
			{				
				$this->adminTemplate($data['title'],"user/viewUser", $data);
			}
			
		
		}else
		{
			
			redirect('admin/logout');
		
		}
	}
	
	public function editCustomer()
	{
		if($this->session->userdata('ad')['userRole'] == "admin")
		{
			$data['ttile'] = 'Add Customer Details';
			if($this->uri->segment(3))
			{
				$customerId = $this->uri->segment(3);
				$data['ttile'] = 'Edit Customer Details';
				$data['customerDetails'] = $this->customerModel->_getCustomer_by_customerid($customerId);
				
			}
			if($_SERVER['REQUEST_METHOD']=='POST')
			{
				$data['customerDetails']['customerName'] = $this->input->post('customerName');
				$data['customerDetails']['IC_no'] = $this->input->post('IC_no');
				$data['customerDetails']['customerMobile'] = $this->input->post('customerMobile');
				
				$this->form_validation->set_rules('customerName','Customer Name','trim|required');
				$this->form_validation->set_rules('IC_no','IC Number','trim|required|callback__checkCustomerIC');
				$this->form_validation->set_rules('customerMobile','Mobile Number','trim|required|callback__checkCustomerMobile');
				
				if($this->form_validation->run()===TRUE)
				{
					$array = array('customerName'=>$data['customerDetails']['customerName'],'IC_no'=>$data['customerDetails']['IC_no'],'customerMobile'=>$data['customerDetails']['customerMobile']);
					if($this->uri->segment(3))
					{
						$this->customerModel->update_customer($this->uri->segment(3),$array);
						_showMessage('Record updated successfully.','success');
						redirect('admin/editcustomer/'.$this->uri->segment(3));
					}else
					{
						$array['customerStatus'] = 'active';
						$this->customerModel->create_customer($array);
						_showMessage('Record inserted successfully.','success');
						redirect('admin/addcustomer');
					}
				}
				
			}
			$this->adminTemplate($data['ttile'],'user/addCustomer',$data);
		}else
		{
			
			redirect('admin/logout');
		
		}
	}
	
	public function _checkCustomerIC()
	{
		 $ic_no = $this->input->post('IC_no');
		 $id = $this->uri->segment(3)?$this->uri->segment(3):'';
		 
		 if($this->customerModel->checkCustomerBYIC($ic_no,$id))
		 {
			$this->form_validation->set_message('_checkCustomerIC','This IC number is already exist.');
			return false;
		 }else
		 {
			return true;
		 }
	
		
	}
	
	public function _checkCustomerMobile()
	{
		$customerMobile = $this->input->post('customerMobile');
		 $id = $this->uri->segment(3)?$this->uri->segment(3):'';
		 
		 if($this->customerModel->checkCustomerBYMobile($customerMobile,$id))
		 {
			$this->form_validation->set_message('_checkCustomerMobile','This mobile number is already exist.');
			return false;
		 }else
		 {
			return true;
		 }
	}
	
	
	
	
	public function forgotpassword()
	{
		$data['title'] = 'Forgot Password';
		if($_SERVER['REQUEST_METHOD']=='POST')
		{
			$unitNumber = $this->input->post('unitNumber');
			
			$this->form_validation->set_rules('unitNumber','Unit Number','trim|required|callback__checkEmailByUnitNumber');
			
			if($this->form_validation->run()===TRUE)
			{
				$customerDetails = $this->customerModel->getCustomerByUnitNumber($unitNumber);
				$token = 'ibraco_'.(random_string('alnum',6));
				$newPassword = password_hash($token, PASSWORD_DEFAULT, ['cost' => 11]);
				$array = array('customerPassword'=>$newPassword);
				$this->customerModel->update_customer($customerDetails['customerId'],$array);
				$subject = 'Ibraco|Forgot Password';
				$link['name'] = $customerDetails['customerName'];
				$link['description'] = '<p>I have reset your password. Please use this password for login under Ibraco.</p><p>Password:'.$token.'</p>';
				$message =   $this->load->view('email/commonEmail',$link,true);
				$to = $customerDetails['customerEmail'];
				sendEmail($to,$subject,$message);
				
				_showMessage('I have sent you new password on your registered email. Please check your email.Thank You.','success');
				redirect('customer/forgotpassword');
			}
			
			
			
		}
		$this->load->view('customer/forgotpassword',$data);
		
	}
	
	public function _checkEmailByUnitNumber()
	{
		$unitNumber = $this->input->post('unitNumber');
		if($unitNumber)
		{
			$customerDetails = $this->customerModel->getCustomerByUnitNumber($unitNumber);
			if($customerDetails&&$customerDetails['customerEmail']!='')
			{
				return true;
			}else
			{
				$this->form_validation->set_message('_checkEmailByUnitNumber','We don\'t have your regitered email. Please contact to Management. ');
				return false;
			}
		}
	}
	
	public function defect()
	{
		if($this->session->userdata('cu')['userRole'] == "customer"){
		$data['title'] = 'Defect';
		$data['customerDetails'] = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
		$this->template($data['title'],'defect',$data);
		}
		else
		{
			redirect('customer/logout');
		}
		
	}
	public function editDefectAppointment()
	{
		if($this->session->userdata('cu')['userRole'] == "customer"){
			
			$id = $this->uri->segment(3);
			$appointmentId=str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
			$appointmentId = $this->encryption->decrypt($appointmentId);
			 
			 $appointmentDetails = $this->customerModel->_getAppointmentById($appointmentId);
			 $appointmentDetails['commentDetails'] = $this->customerModel->_getAppointmentCommentsDetails($appointmentId);
			 
			 
			 if(@$appointmentDetails['customerId']==$this->session->userdata('cu')['userId'])
			 {
				if($appointmentDetails['appointmentStatus']=='open'||$appointmentDetails['appointmentStatus']=='resolved')
				$data['appointmentDetails'] = $appointmentDetails;
			 }
			 $data['emailList'] = $this->adminModel->getDepartmentEmail();
			$data['title'] = 'Defect';
			if($_SERVER['REQUEST_METHOD']=='POST')
            {
				$id = $this->uri->segment(3);
				$appointmentId=str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
				$appointmentId = $this->encryption->decrypt($appointmentId);
				$appointmentDate = $this->input->post('appointmentDate');
                $appointmentTime = $this->input->post('appointmentTime');
				$appointmentStatus = $this->input->post('appointmentStatus');
				$this->form_validation->set_rules('appointmentDate','Date','trim|required');
				$this->form_validation->set_rules('appointmentTime','Time','trim|required');
				if($this->form_validation->run()===true){
				if($appointmentId){
					
					$array = array('appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appointmentDate))),'appointmentTime'=>$appointmentTime,'appointmentStatus'=>($appointmentStatus!='')?$appointmentStatus:$appointmentDetails['appointmentStatus']);
					if($appointmentStatus!='')
					{
						$array['statusUpdatedOn'] = date('Y-m-d H:i:s'); 
					}
					$this->customerModel->updateHandoverAppoinment($appointmentId,$array);
					$customerDetails = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
					$logText = $customerDetails['unitNumber'].' has '.$appointmentStatus.' the appointment';
					if($this->input->post('appointmentStatus')=='jointinspection')
					{
						$array = array('parentId'=>$appointmentId,'customerId'=>$this->session->userdata('cu')['userId'],'appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appointmentDate))),'appointmentTime'=>$appointmentTime,'appointmentNotes'=>$appointmentDetails['appointmentNotes'],'appointmentType'=>$appointmentDetails['appointmentType'],'appointmentStatus'=>'open');
						$array['ticketNo'] = $appointmentDetails['ticketNo'].'A';
						$insertId = $this->customerModel->addHandoverAppoinment($array);
						$logText = $customerDetails['unitNumber'].' requested for a joint inspection.';
						
						//save notification
						$notificationText = 'New'.ucfirst($appointmentDetails['appointmentType']).'  Appointment request by '.$customerDetails['unitNumber'];
						$notificationArray= array('appointmentId'=>$insertId,'notificationType'=>'tenant','notificationStatus'=>'unread','tenantId'=>$this->session->userdata('cu')['userId'],'notificationText'=>$notificationText);
						$this->customerModel->saveNotification($notificationArray);
					}
					//save log
					if($appointmentStatus!=''){
					$logArray = array('logText'=>$logText,'appointmentId'=>$appointmentId,'action'=>$appointmentStatus,'tanentId'=>$this->session->userdata('cu')['userId']);
					$this->customerModel->insertLog($logArray);
					$subject = html_entity_decode(stripslashes('IBRACO:UPDATE APPOINTMENT'));
                   
                    $link['appoinmentDate'] = $appointmentDate;
                    $link['appoinmentTime'] = $appointmentTime;
					$link['defect'] = $appointmentDetails['defectCategory'].'. '.$appointmentDetails['defectLevel'].'. '.$appointmentDetails['defectLocation'].'. '.$appointmentDetails['defectIssue'];
                    $link['appointmentType'] = 'defect';
                    $customerDetails = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
                    $link['customerName'] = $customerDetails['customerName'];
                    $link['ownerName'] = $customerDetails['ownerName'];
                    $link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
                    $link['appointmentStatus'] = $appointmentStatus;
                    $message =   $this->load->view('email/statusChange',$link,true);
                    $to = $this->optionModel->getOptionByName('adminEmail');
                    if($appointmentStatus=='cancelled')
                    {
                    	$csTeam = csTeam();
	                	$ICSBTeam = ICSBTeam();
	                	$PIC = PICTeam();

	                	$cc  = array_merge($csTeam,$ICSBTeam,$PIC);
                    }
                    sendEmail($to,$subject,$message,$cc);
					}
					_showMessage('Record updated successfully.','success');
					redirect('customer/dashboard/');
				}
				}
			}
			$this->template($data['title'],'editDefectHandover',$data);
		}
		else
		{
			redirect('customer/logout');
		}
	}
	public function defectList()
	{
		$data['title'] = '';
		$data['defectList'] = defectList();
		
			$data['title'] = 'Handover';
		$this->load->view('defectList',$data);
		
	}
	
	public function submitDefectRequest()
	{
		 //_print_r($this->input->post());
        //_print_r($_FILES);
		$json_data = array(
						"status"=>'fail',
						"type"=>'',
						"message"            => "Somthing went wrong. Please try again.");
		$appoinmentDate = $this->input->post('appoinmentDate');
		$appoinmentTime = $this->input->post('appoinmentTime');
		$notes = $this->input->post('notes');
		$appointmentId = $this->input->post('appointmentId');
		$category = $this->input->post('category')?explode(',',$this->input->post('category')):'';
		$level = $this->input->post('level')?explode(',',$this->input->post('level')):'';
		$location = $this->input->post('location')?explode(',',$this->input->post('location')):'';
		$issue = $this->input->post('issue')?explode(',',$this->input->post('issue')):'';
		if($this->customerModel->checkPublicHolidays($appoinmentDate,'handover'))
		{
			$json_data = array(
						"status"=>'fail',
						"type"=>'',
						"message" => "The selected date is non-working day. Please choose another date.");
		}else{
		if(isset($category)){
			
			foreach($category as $key=>$cat)
			{
				$defectValue[] = array('category'=>$cat,'level'=>isset($level[$key])?$level[$key]:'','location'=>isset($location[$key])?$location[$key]:'','issue'=>isset($issue[$key])?$issue[$key]:'');
			}
		}
		
		if(count($defectValue)==1)
        {
            foreach($defectValue as $val)
			{
				$array = array('customerId'=>$this->session->userdata('cu')['userId'],'appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appoinmentDate))),'appointmentTime'=>$appoinmentTime,'appointmentNotes'=>$notes,'appointmentType'=>'defect','defectCategory'=>$val['category'],'defectLevel'=>$val['level'],'defectLocation'=>$val['location'],'defectIssue'=>$val['issue'],'appointmentStatus'=>'open');
				$link['defect'] = $val['category'].'. '.$val['level'].'. '.$val['location'].'. '.$val['issue'];
			}
        }else
        {
		$array = array('customerId'=>$this->session->userdata('cu')['userId'],'appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appoinmentDate))),'appointmentTime'=>$appoinmentTime,'appointmentNotes'=>$notes,'appointmentType'=>'defect','appointmentStatus'=>'open');
		}
				if($appointmentId==''){
					$ticketNo = _getAppointmentTicketNo('defect');
					$array['ticketNo']=$ticketNo;
				}
				 if(!empty($_FILES['file'])){
					
					$array['filename'] =$fileName = $this->uploadFile($_FILES['file']);
				 }
		$insertId = $this->customerModel->addHandoverAppoinment($array);
		
		if(isset($defectValue)&&count($defectValue)>1)
		{
			$defectTicketNo = $ticketNo.'01';
			$dn = 1;
			$defect= '<table>';
			foreach($defectValue as $val)
			{
				
				$array = array('customerId'=>$this->session->userdata('cu')['userId'],'parentId'=>$insertId,'ticketNo'=>$defectTicketNo,'appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appoinmentDate))),'appointmentTime'=>$appoinmentTime,'appointmentNotes'=>$notes,'appointmentType'=>'defect','defectCategory'=>$val['category'],'defectLevel'=>$val['level'],'defectLocation'=>$val['location'],'defectIssue'=>$val['issue'],'appointmentStatus'=>'open');
				$defectTicketNo =  _getAppointmentTicketNo('',$defectTicketNo);
				if(isset($fileName))
				{
					$array['filename'] =$fileName;
				}
				$insert_id = $this->customerModel->addHandoverAppoinment($array);
				if($insert_id){
				$subject = html_entity_decode(stripslashes('IBRACO:NEW DEFECT APPOINTMENT'));
					
					$appointmentId = $this->encryption->encrypt($insert_id);
					$appointmentId =str_replace(array('+', '/', '='), array('-', '_', '~'), $appointmentId);
					
					$link['accept'] = base_url('admin/editDefectAppointment/'.$insert_id.'/accept');
					$link['reject'] = base_url('admin/rejectAppointment/'.$appointmentId.'/rejected');
					$link['appoinmentDate'] = $appoinmentDate;
					$link['appoinmentTime'] = $appoinmentTime;
					$link['appointmentType'] = 'defect';
					
					$defect .= '<tr><td>'.$dn.'. </td><td>'.$val['category'].'. '.$val['level'].'. '.$val['location'].'. '.$val['issue'].'</td></tr>';
					
					$customerDetails = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
					$link['ownerName'] = $customerDetails['ownerName'];
					$link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
//					$message =   $this->load->view('email/Accept _ rejectemail',$link,true);
//					$to = $this->optionModel->getOptionByName('adminEmail');
//					$csTeam = csTeam();
//                	$ICSBTeam = ICSBTeam();
//                	$PIC = PICTeam();

//                	$cc  = array_merge($csTeam,$ICSBTeam,$PIC);
//					sendEmail($to,$subject,$message,$cc);
					
					//save log
					$logText = $customerDetails['unitNumber'].' created an appointment.';
					$logArray = array('logText'=>$logText,'appointmentId'=>$insert_id,'action'=>'open','tanentId'=>$this->session->userdata('cu')['userId']);
					$this->customerModel->insertLog($logArray);
					
					//save notification
					$notificationText = 'New Defect Appointment request by '.$customerDetails['unitNumber'];
					$notificationArray= array('appointmentId'=>$insert_id,'notificationType'=>'admin','notificationStatus'=>'unread','tenantId'=>$this->session->userdata('cu')['userId'],'notificationText'=>$notificationText);
					$this->customerModel->saveNotification($notificationArray);
				}
				
				$dn++;
			}
			$defect.='</table>';
			$link['defect'] = $defect;
			$message =   $this->load->view('email/Accept _ rejectemail',$link,true);
					$to = $this->optionModel->getOptionByName('adminEmail');
					$csTeam = csTeam();
                	$ICSBTeam = ICSBTeam();
                	$PIC = PICTeam();
			$cc  = array_merge($csTeam,$ICSBTeam,$PIC);
					sendEmail($to,$subject,$message,$cc);
			$this->customerModel->updateHandoverAppoinment($insertId,array('multiDefect'=>1));
			$json_data = array(
								"status"=>'success',
								"type"=>'',
								"message"            => "Record inserted successfully.");
					_showMessage('Record inserted successfully.','success');	
		}
		if($insertId&&count($defectValue)==1)
				{
					
					$subject = html_entity_decode(stripslashes('IBRACO:NEW DEFECT APPOINTMENT'));
					
					$appointmentId = $this->encryption->encrypt($insertId);
					$appointmentId =str_replace(array('+', '/', '='), array('-', '_', '~'), $appointmentId);
					
					$link['accept'] = base_url('admin/editDefectAppointment/'.$insertId.'/accept');
					$link['reject'] = base_url('admin/rejectAppointment/'.$appointmentId.'/rejected');
					$link['appoinmentDate'] = $appoinmentDate;
					$link['appoinmentTime'] = $appoinmentTime;
					$link['appointmentType'] = 'defect';
					
					$customerDetails = $this->customerModel->getCustomerDetailsById($this->session->userdata('cu')['userId']);
					$link['ownerName'] = $customerDetails['ownerName'];
					$link['customerName'] = $customerDetails['customerName'];
					$link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
					$message =   $this->load->view('email/Accept _ rejectemail',$link,true);
					$to = $this->optionModel->getOptionByName('adminEmail');
					$csTeam = csTeam();
                	$ICSBTeam = ICSBTeam();
                	$PIC = PICTeam();

                	$cc  = array_merge($csTeam,$ICSBTeam,$PIC);
					sendEmail($to,$subject,$message,$cc);
					
					//save log
					$logText = $customerDetails['unitNumber'].' created an appointment.';
					$logArray = array('logText'=>$logText,'appointmentId'=>$insertId,'action'=>'open','tanentId'=>$this->session->userdata('cu')['userId']);
					$this->customerModel->insertLog($logArray);
					
					//save notification
					$notificationText = 'New Defect Appointment request by '.$customerDetails['unitNumber'];
					$notificationArray= array('appointmentId'=>$insertId,'notificationType'=>'admin','notificationStatus'=>'unread','tenantId'=>$this->session->userdata('cu')['userId'],'notificationText'=>$notificationText);
					$this->customerModel->saveNotification($notificationArray);
					$json_data = array(
								"status"=>'success',
								"type"=>'',
								"message"            => "Record inserted successfully.");
					_showMessage('Record inserted successfully.','success');		
				}
		}
				
				echo json_encode($json_data);
		
	}
	
	public function remindAppointment()
	{
		
		$appoinmentList = $this->customerModel->_getAppointmentBYHours('24 Hours');
		if($appoinmentList)
		{
			foreach($appoinmentList as $val)
			{
				$time = explode(' - ',$val['appointmentTime']);
				$appointmentTime = strtotime(date('Y-m-d H:i',strtotime($val['appointmentDate'].' '.$time[0])));
				$currentDate = strtotime(date('Y-m-d H:i',strtotime('+1 Day')));
				if(($appointmentTime==$currentDate)&&$val['customerEmail']){
					$email = array($val['customerEmail'],$val['email'],$this->optionModel->getOptionByName('adminEmail'));
					$link['name'] = $val['unitNumber'];
					$link['description'] = '<p>Kindly be reminded of the following appointment:</p><p>Appointment Type: Inspection & Handover of keys / Defect Inspection</p><p>Unit Number:'.$val['unitNumber'].'</p><p>Owner Name:</p><p>Date:'.$val['appointmentDate'].'</p><p>Time:'.$val['appointmentTime'].'</p>';
					$message =   $this->load->view('email/commonEmail',$link,true);
					foreach($email as $em){
					$to = $em;
					$csTeam = csTeam();
                	$ICSBTeam = ICSBTeam();
                	$PIC = PICTeam();

                	$cc  = array_merge($csTeam,$ICSBTeam,$PIC);
					sendEmail($to,$subject,$message,$cc);
					}
				}
			}
		}
	}
	
	public function sendRemindforCloseAppointment()
	{
		$days = $this->input->get('days');
		$appoinmentList = $this->customerModel->_getAppointmentBYHours($days.' Days');
		if($appoinmentList)
		{
			foreach($appoinmentList as $val)
			{
				if($val['customerEmail'])
				{
					$appointmentId = $this->encryption->encrypt($val['appointmentId']);
					$appointmentId =str_replace(array('+', '/', '='), array('-', '_', '~'), $appointmentId);
					$subject= 'Service Ticket Resolved';
					$email = array($val['customerEmail'],$val['email'],$this->optionModel->getOptionByName('adminEmail'));
					$link['unitNumber'] = $val['unitNumber'];
					$link['ownerName'] = $val['ownerName'];
					$link['appointmentDate'] = $val['appointmentDate'];
					$link['appointmentTime'] = $val['appointmentTime'];
					$link['addedOn'] = $val['addedOn'];
					$link['defectCategory'] = $val['defectCategory'];
					$link['statusUpdatedOn'] = $val['statusUpdatedOn'];
					$link['attended_by'] = $val['contactName'];
					$link['accept'] = base_url('customer/editDefectAppointment/'.$appointmentId.'/jointinspection');
					$link['reject'] = base_url('customer/appointmentAction/'.$appointmentId.'/closed');
					$link['description'] = '<p>Kindly be informed that should no action is taken after 14 days from the date of service ticket resolved, we shall assume that you are satisfied with the rectification work and the Service Ticket will be closed automatically.</p>';
					$message =   $this->load->view('email/reminderEmail',$link,true);
					foreach($email as $em){
						$to = $em;
						$csTeam = csTeam();
                	$ICSBTeam = ICSBTeam();
                	$PIC = PICTeam();

                	$cc  = array_merge($csTeam,$ICSBTeam,$PIC);
						sendEmail($to,$subject,$message,$cc);
					}
				}
			}
		}
	}
	
	public function template($title='',$template,$data)
	{
		$data['title'] = $title;
		$this->load->view('customer/common/header',$data);
		$this->load->view('customer/common/sidebar',$data);
		$this->load->view('customer/'.$template,$data);
		$this->load->view('customer/common/footer',$data);
	}
	
	public function adminTemplate($title='',$template,$data)
	{
		$data['title'] = $title;
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/'.$template,$data);
		$this->load->view('admin/common/footer',$data);
	}
	
	private function uploadFile($file)
{

if ( ! empty($_FILES))
		{
			$filename = array();
			$this->config->load('upload');
			$config = $this->config->item('appointment');
 			$config['upload_path'] = $config['upload_path'];
			if(!is_dir($config['upload_path'])) //create the folder if it's not already exists
   				 {
      				mkdir($config['upload_path'],0755,TRUE);
   				 } 
			$this->load->library('upload');
			$files           = $file;
			
			$errors = 0;

			// codeigniter upload just support one file
			// to upload. so we need a litte trick
			
				$_FILES['file']['name'] = mktime(date("h"),date("i"),date("s"),date("m"),date("d"),date("y"))."_".$files['name'] ;
				$_FILES['file']['type'] = $files['type'];
				$_FILES['file']['tmp_name'] = $files['tmp_name'];
				$_FILES['file']['error'] = $files['error'];
				$_FILES['file']['size'] = $files['size'];
				// we have to initialize before upload
				$this->upload->initialize($config);

				if (! $this->upload->do_upload("file")) {
					$errors++;
				}else{
					$finfo=$this->upload->data();
					$filename = ($finfo['file_name']);
				}
			
			if(isset($filename) && !empty($filename)){	
				return $filename;
			}

			if ($errors > 0) {
				return  FALSE;
			}

		}	
	}
}