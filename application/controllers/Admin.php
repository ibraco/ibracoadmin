<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
    
    public function __construct()
    {
        
        parent::__construct();
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
		$this->output->set_header('Pragma: no-cache');
		$this->load->model('Admin_model','adminModel');
		$this->load->model('Customer_model','customerModel');
		
		//$this->load->library('PushiOS_lib');
		
    }
	
	public function login()
	{
		if($_SERVER['REQUEST_METHOD']=='POST')
		{
			$data['userDetails']['email'] = $this->input->post('email');
			$data['userDetails']['password'] = $this->input->post('password');
			
			$this->form_validation->set_rules('email','Email','trim|required');
			$this->form_validation->set_rules('password','Password','trim|required');
			
			if($this->form_validation->run()===TRUE)
			{
				if($this->adminModel->_isValid_admin() === FALSE){
				_showMessage('User name or password is not valid.','error');
				redirect('admin/login');
			}
			else
			{	
				$userData = $this->adminModel->_getAdmin_by_admin_email($this->input->post('email'));
				$updateLastLogin = array('admin_last_login'=>date('Y-m-d H:i:s'));
					$lastLogin = ($userData['admin_last_login']!='0000-00-00 00:00:00')?date('M d,Y h:i a',strtotime($userData['admin_last_login'])):date('M d,Y h:i a');
					$this->adminModel->update_admin($userData['admin_id'],$updateLastLogin );
				
				
				$Data['ad'] = array('userName' => $userData['admin_username'],'userId'=>$userData['admin_id'],'fullName'=>$userData['admin_fullname'], 'niceName'=>$userData['admin_nicename'],'userEmail'=>$userData['admin_email'],'userRole'=> 'admin','lastLogin'=>$lastLogin );
				$this->session->set_userdata($Data);
                if ($this->session->userdata('last_page')!='') {
                        # code...
                    redirect($this->session->userdata('last_page'));
                    }	
				redirect('admin/dashboard');
			}
			}
			
		}
		$this->load->view('admin/login');
		
	}
	
	public function dashboard()
	{
		
		if($this->session->userdata('ad')['userRole']=='admin')
		{
			$data['title'] ='Admin Dasboard';
            $today = date('Y-m-d');
            $data['todayBookedAppointment'] = $this->adminModel->getAppointmentCount($today,'today');
            $data['weekBookedAppointment'] = $this->adminModel->getAppointmentCount($today,'thisweek');
            $data['resolvedAppointment'] = $this->adminModel->getAppointmentCount($today,'resolved');
            $data['pendingAppointment'] = $this->adminModel->getAppointmentCount($today,'pending');
            $data['defectList'] = defectList();
			$this->adminTemplate($data['title'],'dashboard',$data);
		}
		else
		{
			redirect('admin/logout');
		}
	}
	
    public function calenderView()
    {
        if($this->session->userdata('ad')['userRole']=='admin')
		{
			$data['title'] ='Calender View';
            $data['calendarSetting'] = $this->adminModel->getCalenderSettings('handover');
            $data['publicHolidays'] = $this->adminModel->getPublicHoliday();
            if(isset($data['calendarSetting']['days']))
            $data['days'] = json_encode(explode(',',$data['calendarSetting']['days'])); 
			$this->adminTemplate($data['title'],'calenderView',$data);
		}
		else
		{
			redirect('admin/logout');
		}
    }
    
    public function importTenant()
    {
        $this->load->view('admin/importTenant');
    }
    
    public function uploadTenantData()
    {
        if($_SERVER['REQUEST_METHOD']=='POST')
        {
            //_print_r($_FILES);
            if(!empty($_FILES)){
            $this->load->library('PHPExcel');
			$file = FCPATH."uploads/".$_FILES['file']['name'];
               $isUploaded = copy($_FILES['file']['tmp_name'], $file);
			   if($isUploaded)
			   {
                $objPHPExcel = PHPExcel_IOFactory::load("$file");
 

                $playerDetails = array();
                foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                    $dataArr = array();
                    $worksheetTitle     = $worksheet->getTitle();
                    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
                    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    
                    for ($row = 2; $row <= $highestRow; ++ $row) {
                    for ($col = 0; $col < $highestColumnIndex; ++ $col) {
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        $val = $cell->getValue();
                        //_print_r($val);
                        
                        $dataArr[$row][$col] = $val;
                        //_print_r($dataArr[$row]);
                        
                    }
                }
                
                
                if($dataArr)
                {
                    foreach($dataArr as $val)
                    {
                        $customerDetails = $this->customerModel->getCustomerByUnitNumber($val[0]);
                        if($customerDetails)
                        {
                            $this->customerModel->update_customer($customerDetails['customerId'],array('customerStatus'=>'delete'));
                        }
                        $length = 3;
                        $unitNumber =sprintf("%'.0".$length."d",$val[0]);
                        $password =password_hash(1234567, PASSWORD_DEFAULT, ['cost' => 11]); 
                        $array = array('unitNumber'=>$val[0],'surveyLot'=>$val[1],'customerName'=>$val[2]?$val[2]:'','ownerName'=>$val[2]?$val[2]:'','IC_no'=>$val[3]?$val[3]:'','customerMobile'=>$val[4]?$val[4]:'','customerAlternateMobile'=>$val[5]?$val[5]:'','customerStatus'=>'active','customerPassword'=>$password);
                        
                        $this->customerModel->create_customer($array);
                    }
                    
                    $json_data = array(
						"status"=>'success',
						"message"            =>'Record Inserted Successfully.'
                        );
                }
                }
                
               }
            }else
            {
                $json_data = array(
						"status"=>'error',
						"message"            =>'Please select file.'
                        );
            }
            
            echo json_encode($json_data);
        
        }
    }
    
    public function settings()
    {
        if($this->session->userdata('ad')['userRole']=='admin')
		{
			$data['title'] ='Admin Settings';
            $data['handoverCalenderSettings'] =array();
            $data['defectCalenderSettings'] = array();
            
             $calenderSettings = $this->adminModel->getCalenderSetting();
             //$data['emailList'] = $this->adminModel->getDepartmentEmail();
            if(!empty($calenderSettings))
            {
                foreach($calenderSettings as $val)
                {
                    if($val['calenderType']=='handover')
                    $data['handoverCalenderSettings'] = $val;
                    elseif($val['calenderType']=='defect')
                    $data['defectCalenderSettings'] = $val;
                    
                }
            }
			$this->adminTemplate($data['title'],'settings',$data);
		}
		else
		{
			redirect('admin/logout');
		}
    }
    
    public function calenderSetting()
    {
        $data['calenderType'] = $this->input->post('calenderType');
            $data['calenderSettingId'] = $this->input->post('calenderSettingId');
             $data['calenderSettings'] = $this->adminModel->getCalenderSettingById($data['calenderSettingId']);
        $this->load->view('admin/handoverCalenderSetting',$data);
    }
    
    public function updateSettings()
    {
         $json_data = array(
						"status"=>'fail',
						"message"            => "Error try again.");
        if($_SERVER['REQUEST_METHOD']=='POST')
        {
            $service = $this->input->post('service');
            $manpower = $this->input->post('manpower');
            $startTime = $this->input->post('startTime');
            $endTime = $this->input->post('endTime');
            $days = $this->input->post('days');
            $calenderType = $this->input->post('calenderType');
            $calenderSettingId = $this->input->post('calenderSettingId');
            //_print_r($days);
            $array = array('service'=>$service,'manpower'=>$manpower,'startTime'=>$startTime,'endTime'=>$endTime,'days'=>implode(',',$days),'calenderType'=>$calenderType);
            if($calenderSettingId)
            {
                $insertId = $this->adminModel->update_CalenderSettings($calenderSettingId,$array);
                
            }else{
            $insertId = $this->adminModel->insert_CalenderSettings($array);
            }
            if($insertId)
            {
                $json_data = array(
						"status"=>'success',
						"message"            =>'Record Updated Successfully.'
                        );
            }else
            {
                $json_data = array(
						"status"=>'fail',
						"message"            => "Error try again.");
            }
            
        }
        echo json_encode($json_data);
    }
    
    public function addEmail()
    {
        $data['emailId'] = $this->input->post('emailId');
        if($data['emailId'])
        {
            $data['emailDetails'] = $this->adminModel->getDepartmentEmailById($data['emailId']);
        }
        $this->load->view('admin/addEmail',$data);
    }
    
    public function addDepartmentEmail()
    {
        $json_data = array(
						"status"=>'fail',
						"message"            => "Error try again.");
        $contactName = $this->input->post('contactName');
        $categories = $this->input->post('categories');
        $email = $this->input->post('email');
        $departmentEmailId = $this->input->post('departmentEmailId');
        if($this->checkDepartmentEmail($email,$departmentEmailId))
        {
             $json_data = array(
						"status"=>'fail',
						"message"            => "Email is alreday exist.");
        }else{
        $array = array('contactName'=>$contactName,'categories'=>$categories,'email'=>$email);
        if($departmentEmailId){
            $insertId = $this->adminModel->updateDepartmentEmail($departmentEmailId,$array);
            $json_data = array(
						"status"=>'success',
						"message"            =>'Record Updated Successfully.'
                        );
        }else{
        $insertId = $this->adminModel->addDepartmentEmail($array);
        $json_data = array(
						"status"=>'success',
						"message"            =>'Record Inserted Successfully.'
                        );
        }
        
             
        }
            echo json_encode($json_data);
        
        
    }
    
    public function checkDepartmentEmail($email,$id='')
    {
        if($email)
        {
            $details = $this->adminModel->checkDepartmentEmail($email,$id);
            return $details;
        }
    }
    
    public function ajaxemallist()
    {
        $columns = array(
                        0=>'contactNumber',
                        1=>'categories',
                        2=>'email',
                        3=>'departmentEmailId'
                        );
        $customer=array();
        $requestData= $this->input->post();
        $totalRecord = $totalFilteredRecord = $this->adminModel->getDepartmentEmail('','','','','','count');
        if( !empty($requestData['search']['value']) ) {
            
            $data = $this->adminModel->getDepartmentEmail($requestData['search']['value'],$requestData['start'] ,$requestData['length'], $columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir']);
            $totalRecord = $totalFilteredRecord = $this->adminModel->getDepartmentEmail($requestData['search']['value'],'','','','','count');
            $i=0;
			if($data ){
				foreach($data as $key=>$record){
                    
                    $customer[$i][] =$record['contactName'];
					$customer[$i][] =$record['categories'];
					$customer[$i][] =$record['email'];
                    $customer[$i][] =$record['departmentEmailId'];
                    $i++;
                }
            }
        }else
        {
            $data = $this->adminModel->getDepartmentEmail('',$requestData['start'] ,$requestData['length'], $columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir']);
            $i=0;
			if($data ){
				foreach($data as $key=>$record){
                    
                    $customer[$i][] =$record['contactName'];
					$customer[$i][] =$record['categories'];
					$customer[$i][] =$record['email'];
                    $customer[$i][] =$record['departmentEmailId'];
                 $i++;   
                }
            }
            
        }
        
        if($customer){
			$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $totalRecord ,
                "recordsFiltered" => $totalFilteredRecord,
                "data"            => $customer
            );
		}else{
			$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $totalRecord ,
                "recordsFiltered" => 0,
                "data"            => array()
            );

		}
		echo json_encode($json_data);
    }
    
    public function removeEmail()
    {
        $emailId = $this->input->post('emailId');
        $this->adminModel->deleteEmail($emailId);
        
    }
    
    public function viewTenant()
    {
       if($this->session->userdata('ad')['userRole']=='admin')
		{
            $data['title'] = 'Tenant Details';
            $customerId = $this->uri->segment(3);
            $data['customerDetails'] = $this->customerModel->getCustomerDetailsById($customerId);
            $data['appointmentList'] = $this->customerModel->getCustomerAppointmentList($customerId);
            $this->adminTemplate($data['title'],'viewTenant',$data);
        }else
		{
			redirect('admin/logout');
		}
    }
    
    public function editAppointment()
	{
		if($this->session->userdata('ad')['userRole'] == "admin"){
			
			$appointmentId = $this->uri->segment(3);
			
			 
			 $appointmentDetails = $this->customerModel->_getAppointmentById($appointmentId);
             $appointmentDetails['commentDetails'] = $this->customerModel->_getAppointmentCommentsDetails($appointmentId);
             $appointmentDetails['parentCommentDetails'] = $this->customerModel->getParentAppointmentDetails($appointmentId);
			 
             $data['emailList'] = $this->adminModel->getDepartmentEmail('','','','','','','Customer service PIC');
				//if($appointmentDetails['appointmentStatus']=='open')
				$data['appointmentDetails'] = $appointmentDetails;
                
                //_print_r($appointmentDetails);
			 
			$data['title'] = 'Handover';
			$this->adminTemplate($data['title'],'handover',$data);
		}
		else
		{
			redirect('admin/logout');
		}
	}
    
    public function submitHandoverRequest()
    {
        if($this->session->userdata('ad')['userRole'] == "admin"){
           $appointmentId = $this->uri->segment(3);
           $appointmentDetails = $this->customerModel->_getAppointmentById($appointmentId);
             $appointmentDetails['commentDetails'] = $this->customerModel->_getAppointmentCommentsDetails($appointmentId);
             $appointmentDetails['parentCommentDetails'] = $this->customerModel->getParentAppointmentDetails($appointmentId);
			 
             $data['emailList'] = $this->adminModel->getDepartmentEmail('','','','','','','Customer service PIC');
				//if($appointmentDetails['appointmentStatus']=='open')
				$data['appointmentDetails'] = $appointmentDetails;
            if($_SERVER['REQUEST_METHOD']=='POST')
            {
                
                $data['appointmentDetails']['appointmentDate'] = $appointmentDate = $this->input->post('appointmentDate');
                $data['appointmentDetails']['appointmentTime'] = $appointmentTime = $this->input->post('appointmentTime');
                $contractorId = $this->input->post('contractorId');
                $appointmentStatus = $this->input->post('appointmentStatus');
                $appointmentComment = $this->input->post('appointmentComment');
                $appointmentReason = $this->input->post('appointmentReason');
                $visiblefortenant = $this->input->post('visiblefortenant');
                $calllog = $this->input->post('calllog');
                //_print_r($this->input->post());
                //_print_r($_FILES);
                //echo $_FILES['commentfile']['size'][0];exit;
                $this->form_validation->set_rules('appointmentDate','Date','trim|required');
				$this->form_validation->set_rules('appointmentTime','Time','trim|required');
                 if(!empty($_FILES['commentfile']['size'][0]>0))
                {
                    $this->form_validation->set_rules('appointmentComment','Comment','trim|required');
                }
                if(!empty($_FILES['reasonfile']['size'][0]>0))
                {
                    $this->form_validation->set_rules('appointmentReason','Reason','trim|required');
                }
                if($this->form_validation->run()===true){
                if($appointmentId){
                    
                    $appointmentDetails = $this->customerModel->_getAppointmentById($appointmentId);
                    //_print_r($appointmentDetails);exit;
                    
                $array = array('appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appointmentDate))),'appointmentTime'=>$appointmentTime,'appointmentStatus'=>$appointmentStatus,'contractorId'=>$contractorId);
                if($appointmentStatus=='')
                    {
                        $array['appointmentStatus'] = $appointmentDetails['appointmentStatus'];
                        
                    }elseif($appointmentStatus!='')
                    {
                        $array['statusUpdatedOn'] = date('Y-m-d H:i:s');
                    }

                    if($appointmentDetails['appointmentType']=='defect'){
                        $this->customerModel->updateHandoverAppoinmentByParentId($appointmentDetails['parentId'],$array);
                    }else
                    {
                        $this->customerModel->updateHandoverAppoinment($appointmentId,$array);
                    }
                
                if($contractorId!=''&&$appointmentDetails['contractorId']!=$contractorId)
                { 
                   $contractorDetails = $this->adminModel->getDepartmentEmailById($contractorId);
                   $subject = html_entity_decode(stripslashes('IBRACO:ASSIGNED CONTRACTOR'));
                   $link['contractorName'] = $contractorDetails['contactName'];
                    $link['appoinmentDate'] = $appointmentDate;
                    $link['appoinmentTime'] = $appointmentTime;
                    $link['appointmentType'] = $appointmentDetails['appointmentType'];
                    $link['defect'] = $appointmentDetails['defectCategory'].($appointmentDetails['defectLevel']?'. '.$appointmentDetails['defectLevel']:'').($appointmentDetails['defectLocation']?'. '.$appointmentDetails['defectLocation']:'').($appointmentDetails['defectIssue']?'. '.$appointmentDetails['defectIssue']:'');
                    $customerDetails = $this->customerModel->getCustomerDetailsById($appointmentDetails['customerId']);
                    $link['ownerName'] = $customerDetails['customerName'];
                    $link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
                    $message =   $this->load->view('email/contractorEmail',$link,true);
                    $to = $contractorDetails['email'];
                    sendEmail($to,$subject,$message);
                    $logText = $contractorDetails['contactName'].' has been assigned for this appointment.';
                    $logArray = array('logText'=>$logText,'appointmentId'=>$appointmentId,'action'=>'assigned','tanentId'=>$appointmentDetails['customerId']);
                    $this->customerModel->insertLog($logArray);
                }
                if($appointmentStatus!=''&&$appointmentDetails['appointmentStatus']!=$appointmentStatus)
                {
                    $subject = html_entity_decode(stripslashes('IBRACO:UPDATE APPOINTMENT'));
                   
                    $link['appoinmentDate'] = $appointmentDate;
                    $link['appoinmentTime'] = $appointmentTime;
                    $link['appointmentType'] = $appointmentDetails['appointmentType'];
                    $link['addedOn'] = $appointmentDetails['addedOn'];
                    $link['defectCategory'] = $appointmentDetails['defectCategory'];
                    $link['statusUpdatedOn'] = $appointmentDetails['statusUpdatedOn'];
                    $link['attended_by'] = $appointmentDetails['contractorName'];
                    $customerDetails = $this->customerModel->getCustomerDetailsById($appointmentDetails['customerId']);
                    $link['customerName'] = $customerDetails['customerName'];
                    $link['ownerName'] = $customerDetails['ownerName'];
                    $link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
                    $link['appointmentStatus'] = $appointmentStatus;
                    if($appointmentStatus=='accept')
                    {
                        $appointmentStatus = 'accepted';
                        $message =   $this->load->view('email/statusConfirm',$link,true);
                        $to = $customerDetails['customerEmail'];
                        $csTeam = csTeam();
                    $ICSBTeam = ICSBTeam();
                    $PIC = PICTeam();

                    $cc  = array_merge($csTeam,$ICSBTeam,$PIC);//_print_r($cc);
                        sendEmail($to,$subject,$message,$cc);//exit;
                    }if($appointmentStatus=='rejected')
                    {
                        $link['appointmentDate'] = $appointmentDate;
                        $link['appointmentTime'] = $appointmentTime;
                        $link['reason'] = isset($appointmentReason)?$appointmentReason:'';
                        $message =   $this->load->view('email/statusRejected',$link,true);
                        $to = $customerDetails['customerEmail'];
                        $csTeam = csTeam();
                    $ICSBTeam = ICSBTeam();
                    $PIC = PICTeam();

                    $cc  = array_merge($csTeam,$ICSBTeam,$PIC);
                        sendEmail($to,$subject,$message,$cc);
                    }elseif($appointmentStatus=='noshow')
                    {
                        $link['description'] = 'We regret to inform you that we will close this appointment request as you have failed to show up for the appointment. Please re-book your appointment accordingly should you would like to log any defect/handover report.';
                        $message =   $this->load->view('email/statusChange',$link,true);
                        $to = $customerDetails['customerEmail'];
                        $csTeam = csTeam();
                    $ICSBTeam = ICSBTeam();
                    $PIC = PICTeam();

                    $cc  = array_merge($csTeam,$ICSBTeam,$PIC);
                        sendEmail($to,$subject,$message,$cc);
                    }if($appointmentStatus=='resolved')
                    {
                        $appointmentId1 = $this->encryption->encrypt($appointmentId);
                    $appointmentId1 =str_replace(array('+', '/', '='), array('-', '_', '~'), $appointmentId1);
                        $link['appointmentDate'] = $appointmentDate;
                        $link['appointmentTime'] = $appointmentTime;
                        $link['statusUpdatedOn'] = date('Y-m-d H:i:s');
                        $link['accept'] = base_url('customer/editDefectAppointment/'.$appointmentId.'/jointinspection');
                        $link['reject'] = base_url('customer/appointmentAction/'.$appointmentId1.'/closed');
                        $link['description'] = '<p>Kindly be informed that should no action is taken after 14 days from the date of service ticket resolved, we shall assume that you are satisfied with the rectification work and the Service Ticket will be closed automatically.</p>';
                        $message =   $this->load->view('email/reminderEmail',$link,true);
                        $to = $customerDetails['customerEmail'];
                        $csTeam = csTeam();
                    $ICSBTeam = ICSBTeam();
                    $PIC = PICTeam();

                    $cc  = array_merge($csTeam,$ICSBTeam,$PIC);
                        sendEmail($to,$subject,$message,$cc);
                    }
                    
                    
                    
                    $logText = 'Management has '.$appointmentStatus.' the appointment.';
                    if($appointmentStatus=='noshow')
                    {
                        $logText = $customerDetails['unitNumber'].' no show up.';
                    }
                    $logArray = array('logText'=>$logText,'appointmentId'=>$appointmentId,'action'=>$appointmentStatus,'tanentId'=>$appointmentDetails['customerId']);
                    $this->customerModel->insertLog($logArray);
                    
                    //save notification
                    
                    $notificationText = 'Management has '.$appointmentStatus.' the appointment.';
                    if($appointmentStatus=='noshow')
                    {
                        $notificationText = $customerDetails['unitNumber'].' no show up.';
                    }
                    $notificationArray= array('appointmentId'=>$appointmentId,'notificationType'=>'tenant','notificationStatus'=>'unread','tenantId'=>$appointmentDetails['customerId'],'notificationText'=>$notificationText);
                    $this->customerModel->saveNotification($notificationArray);
                }
                if($appointmentComment||$appointmentReason){
                $commentArray = array('appointmentId'=>$appointmentId,'appointmentComment'=>$appointmentComment,'appointmentReason'=>$appointmentReason,'visiblefortenant'=>isset($visiblefortenant)?1:0,'calllog'=>isset($calllog)?1:0,'appointmentCommentStatus'=>'active');
                
                $commentId = $this->customerModel->addAppointmentComment($commentArray);
                }
                
                if(!empty($_FILES['commentfile']))
                {
                    foreach($_FILES['commentfile']['size'] as $key=>$commentfile)
                    {
                        
                        if($_FILES['commentfile']['size'][$key]>0)
                        {
                            $fileArray = array('name'=>$_FILES['commentfile']['name'][$key],'type'=>$_FILES['commentfile']['type'][$key],'tmp_name'=>$_FILES['commentfile']['tmp_name'][$key],'error'=>$_FILES['commentfile']['error'][$key],'size'=>$_FILES['commentfile']['size'][$key]);
                            $fileName = $this->uploadFile($fileArray);
                            
                            if($fileName)
                            {
                                $file = array('appointmentId'=>$appointmentId,'commentId'=>$commentId,'image'=>$fileName,'imageType'=>'comment');
                                $this->customerModel->appointmentFile($file);
                            }
                        }
                    }
                }
                
                if(!empty($_FILES['reasonfile']))
                {
                    foreach($_FILES['reasonfile']['size'] as $key=>$commentfile)
                    {
                        if($_FILES['reasonfile']['size'][$key]>0)
                        {
                            $fileArray = array('name'=>$_FILES['reasonfile']['name'][$key],'type'=>$_FILES['reasonfile']['type'][$key],'tmp_name'=>$_FILES['reasonfile']['tmp_name'][$key],'error'=>$_FILES['reasonfile']['error'][$key],'size'=>$_FILES['reasonfile']['size'][$key]);
                            $fileName = $this->uploadFile($fileArray);
                            if($fileName)
                            {
                                $file = array('appointmentId'=>$appointmentId,'commentId'=>$commentId,'image'=>$fileName,'imageType'=>'reason');
                                $this->customerModel->appointmentFile($file);
                            }
                        }
                    }
                }
                _showMessage('Record update successfully','success');
                redirect('admin/editAppointment/'.$appointmentId);
                }
                
                }
                $data['title'] = 'Handover';
			$this->adminTemplate($data['title'],'handover',$data);
                
            }
        }
        else
		{
			redirect('admin/logout');
		}
    }
    
    public function rejectAppointment()
	{
		$appointmentId = $this->uri->segment(3);
		
		
		$array = array('appointmentStatus'=>'rejected');
		$this->customerModel->updateHandoverAppoinment($appointmentId,$array);
		
	}
    
	public function appointmentAction()
    {
         $id = $this->uri->segment(3);
        $appointmentId=str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
		$appointmentId = $this->encryption->decrypt($appointmentId);
         $action = $this->uri->segment(4);
         $appointmentDetails = $this->customerModel->_getAppointmentById($appointmentId);
         
         if(!empty($appointmentId)){
         if($appointmentDetails['appointmentStatus']=='open')
         {
            $array = array('appointmentStatus'=>$action);
            $this->customerModel->updateHandoverAppoinment($appointmentId,$array);
            if($action=='accept')
                    {
                        $appointmentStatus = 'accepted';
                    }else
                    {
                        $appointmentStatus = $action;
                    }
                    $subject = html_entity_decode(stripslashes('IBRACO:UPDATE APPOINTMENT'));
                   
                    $link['appoinmentDate'] = $appointmentDetails['appointmentDate'];
                    $link['appoinmentTime'] = $appointmentDetails['appointmentTime'];
                    $link['appointmentType'] = $appointmentDetails['appointmentType'];
                    $link['defect'] = $appointmentDetails['defectCategory'].($appointmentDetails['defectLevel']?'. '.$appointmentDetails['defectLevel']:'').($appointmentDetails['defectLocation']?'. '.$appointmentDetails['defectLocation']:'').($appointmentDetails['defectIssue']?'. '.$appointmentDetails['defectIssue']:'');
                    $customerDetails = $this->customerModel->getCustomerDetailsById($appointmentDetails['customerId']);
                    $link['customerName'] = $customerDetails['customerName'];
                    $link['ownerName'] = $customerDetails['ownerName'];
                    $link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
                    $link['appointmentStatus'] = $appointmentStatus;
                    $message =   $this->load->view('email/statusChange',$link,true);
                    $to = $customerDetails['customerEmail'];
                    $csTeam = csTeam();
                    $ICSBTeam = ICSBTeam();
                    $PIC = PICTeam();

                    $cc  = array_merge($csTeam,$ICSBTeam,$PIC);
                    sendEmail($to,$subject,$message,$cc);
                    $logText = 'Management has '.$action.' appointment.';
                    $logArray = array('logText'=>$logText,'appointmentId'=>$appointmentId,'action'=>$action,'tanentId'=>$appointmentDetails['customerId']);
                    $this->customerModel->insertLog($logArray);
                    if($this->input->post('notificationId')){
                    $notiArray = array('notificationStatus'=>'read');
                    $this->adminModel->updateNotification($this->input->post('notificationId'),$notiArray);
            }
            
            _showMessage('You have status updated successfully.','success');
         }elseif(($appointmentDetails['appointmentStatus']=='resolved'||$appointmentDetails['appointmentStatus']=='completed')){
            
            if($action=='jointinspection')
            {
                $array = array('parentId'=>$appointmentId,'customerId'=>$appointmentDetails['customerId'],'appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appointmentDetails['appointmentDate']))),'appointmentTime'=>$appointmentDetails['appointmentTime'],'appointmentNotes'=>$appointmentDetails['appointmentNotes'],'appointmentType'=>$appointmentDetails['appointmentType'],'appointmentStatus'=>'open');
                $array['ticketNo'] = $appointmentDetails['ticketNo'].'A';
                $this->customerModel->addHandoverAppoinment($array);
                $logText = $customerDetails['unitNumber'].' requested for a joint inspection.';
                $logArray = array('logText'=>$logText,'appointmentId'=>$appointmentId,'action'=>'jointinspection','tanentId'=>$appointmentDetails['customerId']);
					$this->customerModel->insertLog($logArray);
                    
                    $subject = html_entity_decode(stripslashes('IBRACO:UPDATE APPOINTMENT'));
                   
                    $link['appoinmentDate'] = $appointmentDate;
                    $link['appoinmentTime'] = $appointmentTime;
					$link['defect'] = $appointmentDetails['defectCategory'].'. '.$appointmentDetails['defectLevel'].'. '.$appointmentDetails['defectLocation'].'. '.$appointmentDetails['defectIssue'];
                    $link['appointmentType'] = $appointmentDetails['appointmentType'];
                    $customerDetails = $this->customerModel->getCustomerDetailsById($appointmentDetails['customerId']);
                    $link['customerName'] = $customerDetails['customerName'];
                    $link['ownerName'] = $customerDetails['ownerName'];
                    $link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
                    $link['appointmentStatus'] = $appointmentStatus;
                    $message =   $this->load->view('email/statusChange',$link,true);
                    $to = $this->optionModel->getOptionByName('adminEmail');
                    $csTeam = csTeam();
                    $ICSBTeam = ICSBTeam();
                    $PIC = PICTeam();

                    $cc  = array_merge($csTeam,$ICSBTeam,$PIC);
                    sendEmail($to,$subject,$message,$cc);
                
            }elseif($action=='closed')
            {
                $array= array('appointmentStatus'=>'closed','statusUpdatedOn'=>date('Y-m-d H:i:s'));
                $this->customerModel->updateHandoverAppoinment($appointmentId,$array);
                $customerDetails = $this->customerModel->getCustomerDetailsById($appointmentDetails['customerId']);
                $logText = $customerDetails['unitNumber'].' has closed the appointment';
                $logArray = array('logText'=>$logText,'appointmentId'=>$appointmentId,'action'=>'closed','tanentId'=>$appointmentDetails['customerId']);
					$this->customerModel->insertLog($logArray);
                    $subject = html_entity_decode(stripslashes('IBRACO:UPDATE APPOINTMENT'));
                   
                    $link['appoinmentDate'] = $appointmentDetails['appointmentDate'];
                    $link['appoinmentTime'] = $appointmentDetails['appointmentTime'];
                    $link['appointmentType'] = $appointmentDetails['appointmentType'];
                    
                    $link['customerName'] = $customerDetails['customerName'];
                    $link['ownerName'] = $customerDetails['ownerName'];
                    $link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
                    $link['appointmentStatus'] = 'closed';
                    $message =   $this->load->view('email/statusChange',$link,true);
                    $to = $customerDetails['customerEmail'];
                    $csTeam = csTeam();
                    $ICSBTeam = ICSBTeam();
                    $PIC = PICTeam();

                    $cc  = array_merge($csTeam,$ICSBTeam,$PIC);
                    sendEmail($to,$subject,$message,$cc);
                    _showMessage('You have status updated successfully.','success');
            }
            
            
            
            }else
         {
             _showMessage('Invalid action.','error');
         }
         }else
         {
            _showMessage('Record not found.','error');
         }
         
         $this->load->view('message');
    }
    
    public function appointmentList()
    {
        $columns = array(
                
                0 =>'ticketNo',
                1 =>'customerName',
                2 =>'appointmentTime',
                3=>'unitNumber',
                4=>'appointmentNotes',
                5=>'contactName',
                6=>'appointmentStatus',
                7=>'appointmentId'
                         
                );
        
        $customer = array();
		$requestData= $this->input->post();
        $totalRecord = $totalFilteredRecord = $this->adminModel->getAppointmentList('','','','','','count',$requestData['status'],$requestData['date'],$requestData['cat'],$requestData['issue']);
        
        if( !empty($requestData['search']['value']) ) {
         
         $data = $this->adminModel->getAppointmentList($requestData['search']['value'] ,$requestData['start'] ,$requestData['length'], $columns[$requestData['order'][0]['column']], $requestData['order'][0]['dir'],'',$requestData['status'],$requestData['date'],$requestData['cat'],$requestData['issue']);
			$i=0;
			$totalFilteredRecord = $this->adminModel->getAppointmentList($requestData['search']['value'],'','','','','count',$requestData['status'],$requestData['date'],$requestData['cat'],$requestData['issue']);   
        }else
        {
            $data = $this->adminModel->getAppointmentList('',$requestData['start'] ,$requestData['length'], $columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir'],'',$requestData['status'],$requestData['date'],$requestData['cat'],$requestData['issue']);
        }
        
        $i=0;
			if($data ){
				foreach($data as $key=>$record){

					$customer[$i][] =$record['ticketNo'];
                    $customer[$i][] = ($record['unitNumber']);
					$customer[$i][] =$record['customerName'];
					$customer[$i][] =$record['appointmentDate'].'<br>'.$record['appointmentTime'];
                    $customer[$i][] =$record['creationDate'];
				
					
                    $customer[$i][] = $record['appointmentType']=='handover'?'Handover':'Defect'.($record['defectCategory']?'. '.$record['defectCategory']:'').($record['defectLevel']?'. '.$record['defectLevel']:'').($record['defectLocation']?'. '.$record['defectLocation']:'').($record['defectIssue']?'. '.$record['defectIssue']:'');
                    $customer[$i][] = $record['contactName'];
				
					if($record['appointmentStatus']=='accept')
					$customer[$i][] = '<label class="badge badge-success">Pending</label>';
					elseif($record['appointmentStatus']=='cancelled')
					$customer[$i][] = '<label class="badge badge-warning">Cancelled</label>';
					elseif($record['appointmentStatus']=='rejected')
					$customer[$i][] = '<label class="badge badge-danger">Rejected</label>';
                    elseif($record['appointmentStatus']=='open')
					$customer[$i][] = '<label class="badge badge-primary">Open</label>';
                    elseif($record['appointmentStatus']=='closed')
                    $customer[$i][]='<label class="badge label-dark">Close</label>';
                    elseif($record['appointmentStatus']=='resolved')
                    $customer[$i][]='<label class="badge label-resolved">Resolved</label>';
                    elseif($record['appointmentStatus']=='completed')
                    $customer[$i][]='<label class="badge label-resolved">Completed</label>';
                    elseif($record['appointmentStatus']=='noshow')
                    $customer[$i][]='<label class="badge label-dark">No Show</label>';
                    elseif($record['appointmentStatus']=='forceclose')
                    $customer[$i][]='<label class="badge label-dark">Force Close</label>';
                    
					else
                    $customer[$i][] = '';
					
					$customer[$i][] =$record['appointmentId'];
                    $customer[$i][] = $record['appointmentType'];
					
					$i++;

				}
			}
            if($customer){
			$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $totalRecord ,
                "recordsFiltered" => $totalFilteredRecord,
                "data"            => $customer
            );
		}else{
			$json_data = array(
                "draw"            => intval( $_REQUEST['draw'] ),
                "recordsTotal"    => $totalRecord ,
                "recordsFiltered" => 0,
                "data"            => array()
            );

		}
		echo json_encode($json_data);

    }
	
	
	
	public function viewCustomerPayment()
	{
		if($this->session->userdata('ad')['userRole']=='admin')
		{
			$teamId = $this->uri->segment(3);
			$data['title'] ='Customers Payment Details';
			$data['paymentDetails'] = $this->customerModel->getPaymentDetais($teamId);
			$this->adminTemplate($data['title'],'viewPayment',$data);
		}
		else
		{
			redirect('admin/logout');
		}
	}
	
	public function report()
	{
		if($this->session->userdata('ad')['userRole']=='admin')
		{
			$teamId = $this->uri->segment(3);
			$data['title'] ='Report';
			$data['tournamentList'] = $this->tournamentModel->getTournamentList();
			$this->adminTemplate($data['title'],'report',$data);
		}
		else
		{
			redirect('admin/logout');
		}
	}
	
	
	
	
	
	public function manageaccount()
	{
		if($this->session->userdata('ad')['userRole']=='admin')
		{
			
			$data['title'] ='Manage Account';
			$data['customerDetails'] = $this->adminModel->_getAdmin_by_admin_Id($this->session->userdata('ad')['userId']);
			
			if($_SERVER['REQUEST_METHOD']=='POST')
			{
				$this->form_validation->set_rules('admin_fullname','Name','trim|required');
				$this->form_validation->set_rules('admin_email','Email','trim|required');
				$this->form_validation->set_rules('admin_company_mobilenumber','Mobile Number','trim|required');
				$this->form_validation->set_rules('admin_company_address','Address','trim|required');
				$this->form_validation->set_rules('admin_company_name','Company Name','trim|required');
				
				if($this->form_validation->run()===true)
				{
					$data['adminDetails']['admin_fullname'] = $this->input->post('admin_fullname');
					$data['adminDetails']['admin_email'] = $this->input->post('admin_email');
					$data['adminDetails']['admin_company_mobilenumber'] = $this->input->post('admin_company_mobilenumber');
					$data['adminDetails']['admin_company_address'] = $this->input->post('admin_company_address');
					$data['adminDetails']['admin_company_name'] = $this->input->post('admin_company_name');
					
					$this->adminModel->update_admin($this->session->userdata('ad')['userId'],$data['adminDetails']);
					
					_showMessage('Record updated successfully.','success');
					redirect('admin/manageaccount');
				}
			}
			
			$this->adminTemplate($data['title'],'editaccount',$data);
		}
		else
		{
			redirect('admin/logout');
		}
	}
	
	public function changepassword()
	{
		
		if($this->session->userdata('ad')['userRole']=='admin')
		{
			
			$data['title'] ='Change Password';
			
			
			if($_SERVER['REQUEST_METHOD']=='POST')
			{
				$oldPass = $this->input->post('oldpassword');
					$newPass = $this->input->post('newpassword');
					$retypePass = $this->input->post('retypepassword');
					$this->form_validation->set_rules('oldpassword', 'Old Password', 'trim|required|callback_verifyPassword');
					$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required');
					$this->form_validation->set_rules('retypepassword', 'Retype Password', 'trim|required|matches[newpassword]');
					if ($this->form_validation->run() === TRUE)
					{
						
						$this->adminModel->changePassword($newPass,$this->session->userdata('ad')['userId']);
						_showMessage('Password changed.','success');
						redirect('admin/profile');
					}
			}
			
			$this->adminTemplate($data['title'],'changepassword',$data);
		}
		else
		{
			redirect('admin/logout');
		}
	}
	
	public function verifyPassword()
	{
		$oldPass = $this->input->post('oldpassword');
		$user_id = $this->session->userdata('ad')['userId'];
		$userData= $this->adminModel->checkUserdata($user_id,$oldPass);
		if(!$userData)
		{
			$this->form_validation->set_message('verifyPassword','Old Password does not match.');
			return false;
		
		}
		else
		{
			return true;
		
		}
		
	}
	
	public function notification()
	{
		if($this->session->userdata('ad')['userRole']=='admin')
		{
			$data['title'] = 'Notifications';
            $data['notificationList'] = $this->adminModel->getAdminNotificationList();
            //_print_r( $data['notificationList']);exit;
			$this->adminTemplate($data['title'],'notification',$data);
			
		}
		else
		{
			redirect('admin/logout');
		}
	}
	
	
	public function logout()
	{
		$this->session->unset_userdata('ad');
		redirect('admin/login');
	}
	
    public function getappointmentEvent()
    {
        if($this->session->userdata('ad')['userRole']=='admin')
		{
            $date = date('Y-m');
			$data['appointmentDetails'] = $this->adminModel->getappointmentByDate($date);
			echo json_encode($data['appointmentDetails']);
		}
		
    }
    
    public function getAppointmentDetails()
    {
        $appointmentDate = $this->input->post('appointmentDate');
        $data['appointmentList'] = $this->customerModel->_getAppointmentByDate($appointmentDate);
        //$appointmentDetails['commentDetails'] = $this->customerModel->_getAppointmentByDate($appointmentDate);
       // _print_r($data['appointmentList']);
        $this->load->view('admin/viewAppointment',$data);
    }
    
    public function handover()
    {
        $data['title'] = 'Handover';
         $date = $this->input->get('date');
       
       if($date)
       {
             $data['appointmentDetails']['appointmentDate'] = date('m/d/Y',strtotime($date));
       }
        $data['unitList'] = $this->adminModel->getAllUnitNumber();
        $data['emailList'] = $this->adminModel->getDepartmentEmail('','','','','','','Customer service PIC');
        $this->adminTemplate($data['title'],'adminHandover',$data);
    }
    
    public function addHandoverRequest()
    {
        if($this->session->userdata('ad')['userRole'] == "admin"){
            if($_SERVER['REQUEST_METHOD']=='POST')
                {
                    $data['appointmentDetails']['appointmentDate'] = $appointmentDate = $this->input->post('appointmentDate');
                    $data['appointmentDetails']['appointmentTime'] = $appointmentTime = $this->input->post('appointmentTime');
                    $data['appointmentDetails']['contractorId'] = $contractorId = $this->input->post('contractorId');
                    $data['appointmentDetails']['unitNumber']  = $customerId = $this->input->post('unitNumber');
                    $data['appointmentDetails']['notes'] = $appointmentNotes = $this->input->post('notes');
                    
                    $this->form_validation->set_rules('unitNumber','Unit Number','trim|required');
                    $this->form_validation->set_rules('appointmentDate','Date','trim|required|callback__checkAppointmentDate');
                    $this->form_validation->set_rules('appointmentTime','Time','trim|required|callback__checkAppointmentTime');
                    $this->form_validation->set_rules('contractorId','Contractor','trim|required');
                    //$this->form_validation->set_rules('notes','Comments','trim|required');
                    
                    if($this->form_validation->run()===TRUE)
                    {
                        $array = array('customerId'=>$customerId,'appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appointmentDate))),'appointmentTime'=>$appointmentTime,'appointmentStatus'=>'accept','contractorId'=>$contractorId,'appointmentNotes'=>$appointmentNotes);
                        $ticketNo = _getAppointmentTicketNo('handover');
                        $array['ticketNo']=$ticketNo;
                        $insertId = $this->customerModel->addHandoverAppoinment($array);
                        if($insertId)
                        {
                            $appointmentDetails = $this->customerModel->_getAppointmentById($insertId);
                                $contractorDetails = $this->adminModel->getDepartmentEmailById($contractorId);
                                $subject = html_entity_decode(stripslashes('IBRACO:ASSIGNED CONTRACTOR'));
                                $link['contractorName'] = $contractorDetails['contactName'];
                                 $link['appoinmentDate'] = $appointmentDate;
                                 $link['appoinmentTime'] = $appointmentTime;
                                 $link['appointmentType'] = $appointmentDetails['appointmentType'];
                                $link['defect'] = $appointmentDetails['defectCategory'].($appointmentDetails['defectLevel']?'. '.$appointmentDetails['defectLevel']:'').($appointmentDetails['defectLocation']?'. '.$appointmentDetails['defectLocation']:'').($appointmentDetails['defectIssue']?'. '.$appointmentDetails['defectIssue']:'');
                                 $customerDetails = $this->customerModel->getCustomerDetailsById($customerId);
                                 $link['ownerName'] = $customerDetails['ownerName'];
                                 $link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
                                 $message =   $this->load->view('email/contractorEmail',$link,true);
                                 $to = $contractorDetails['email'];
                                 $csTeam = csTeam();
                    $ICSBTeam = ICSBTeam();
                    $PIC = PICTeam();

                    $cc  = array_merge($csTeam,$ICSBTeam,$PIC);
                                 sendEmail($to,$subject,$message,$cc);
                                 //save notification
                                $notificationText = 'New Handover Appointment request by '.$customerDetails['unitNumber'];
                                $notificationArray= array('appointmentId'=>$insertId,'notificationType'=>'admin','notificationStatus'=>'unread','tenantId'=>$customerId,'notificationText'=>$notificationText);
                                $this->customerModel->saveNotification($notificationArray);
                                 $logText = $customerDetails['unitNumber'].' created an appointment.';
                                 $logArray = array('logText'=>$logText,'appointmentId'=>$insertId,'action'=>'assigned','tanentId'=>$customerId);
                                 $this->customerModel->insertLog($logArray);
                                 
                                 _showMessage('Appointment added successfully.','error');
                                 redirect('admin/calendarView');
                        }
                    }else
                    {
                        $data['title'] = 'Handover';
                        $data['unitList'] = $this->adminModel->getAllUnitNumber();
                        $data['emailList'] = $this->adminModel->getDepartmentEmail('','','','','','','Customer service PIC');
                        $this->adminTemplate($data['title'],'adminHandover',$data);
                    }
                    
                }
        }
        else
		{
			redirect('admin/logout');
		}
        
    }
    
    public function _checkAppointmentTime()
    {
        $appointmentTime = $this->input->post('appointmentTime');
        $appointmentDate = $this->input->post('appointmentDate');
        $customerId = $this->input->post('unitNumber');
        $calenderSettings = $this->adminModel->getCalenderSettings('handover');
        if($this->customerModel->checkAppointmentBycustomerId($customerId))
        {
            $this->form_validation->set_message('_checkAppointmentTime','You cannt request more than one handover request.');
            return false;
        }else
        {
            return true;
        }
    }
    
    public function _checkAppointmentDate()
    {
       
        $appointmentDate = $this->input->post('appointmentDate');
        $calenderSettings = $this->adminModel->getCalenderSettings('handover');
        if($this->customerModel->checkPublicHolidays($appointmentDate,'handover'))
        {
            $this->form_validation->set_message('_checkAppointmentDate','The selected date is non-working day. Please choose another date.');
            return false;
        }else
        {
            return true;
        }
    }
    
    public function defect()
    {
        $data['title'] = 'Defect';
         $date = $this->input->get('date');
       
       if($date)
       {
             $data['appointmentDetails']['appointmentDate'] = date('m/d/Y',strtotime($date));
       }
        $data['unitList'] = $this->adminModel->getAllUnitNumber();
        $data['emailList'] = $this->adminModel->getDepartmentEmail('','','','','','','Customer service PIC');
        $this->adminTemplate($data['title'],'adminDefect',$data);
    }
    
    public function submitDefectRequest()
    {
       
        $json_data = array(
						"status"=>'fail',
						"type"=>'',
						"message"            => "Somthing went wrong. Please try again.");
        $unitNumber = $this->input->post('unitNumber');
        $contractorId = $this->input->post('contractorId');
		$appoinmentDate = $this->input->post('appoinmentDate');
		$appoinmentTime = $this->input->post('appoinmentTime');
		$notes = $this->input->post('notes');
		$appointmentId = $this->input->post('appointmentId');
		$category = $this->input->post('category');
		$level = $this->input->post('level');
		$location = $this->input->post('location');
		$issue = $this->input->post('issue');
		if($this->customerModel->checkPublicHolidays($appoinmentDate,'defect'))
		{
			$json_data = array(
						"status"=>'fail',
						"type"=>'',
						"message" => "The selected date is non-working day. Please choose another date.");
		}else {
		if(isset($category)){
			foreach($category as $key=>$cat)
			{
				$defectValue[] = array('category'=>$cat,'level'=>isset($level[$key])?$level[$key]:'','location'=>isset($location[$key])?$location[$key]:'','issue'=>isset($issue[$key])?$issue[$key]:'');
			}
		}
        if(count($defectValue)==1)
        {
            foreach($defectValue as $val)
			{
            $array = array('contractorId'=>$contractorId,'customerId'=>$unitNumber,'appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appoinmentDate))),'appointmentTime'=>$appoinmentTime,'appointmentNotes'=>$notes,'appointmentType'=>'defect','defectCategory'=>$val['category'],'defectLevel'=>$val['level'],'defectLocation'=>$val['location'],'defectIssue'=>$val['issue'],'appointmentStatus'=>'open');
            $link['defect'] = $val['category'].'. '.$val['level'].'. '.$val['location'].'. '.$val['issue'];
            }
        }else
        {
            $array = array('contractorId'=>$contractorId,'customerId'=>$unitNumber,'appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appoinmentDate))),'appointmentTime'=>$appoinmentTime,'appointmentNotes'=>$notes,'appointmentType'=>'defect','appointmentStatus'=>'open');
        }
		
				if($appointmentId==''){
					$ticketNo = _getAppointmentTicketNo('defect');
					$array['ticketNo']=$ticketNo;
				}
				
		$insertId = $this->customerModel->addHandoverAppoinment($array);
		
		if(isset($defectValue)&&count($defectValue)>1)
		{
			$defectTicketNo = $ticketNo.'01';
			foreach($defectValue as $val)
			{
				
				$array = array('customerId'=>$unitNumber,'parentId'=>$insertId,'ticketNo'=>$defectTicketNo,'appointmentDate'=>date('Y-m-d',strtotime(str_replace('/','-',$appoinmentDate))),'appointmentTime'=>$appoinmentTime,'appointmentNotes'=>$notes,'appointmentType'=>'defect','defectCategory'=>$val['category'],'defectLevel'=>$val['level'],'defectLocation'=>$val['location'],'defectIssue'=>$val['issue'],'appointmentStatus'=>'open');
				$defectTicketNo =  _getAppointmentTicketNo('',$defectTicketNo);
				$insert_id = $this->customerModel->addHandoverAppoinment($array);
                if($insert_id){
                $subject = html_entity_decode(stripslashes('IBRACO:NEW DEFECT APPOINTMENT'));
                    
                    $appointmentId = $this->encryption->encrypt($insert_id);
                    $appointmentId =str_replace(array('+', '/', '='), array('-', '_', '~'), $appointmentId);
                    
                    $link['accept'] = base_url('admin/editDefectAppointment/'.$insert_id.'/accept');
                    $link['reject'] = base_url('admin/rejectAppointment/'.$appointmentId.'/rejected');
                    $link['appoinmentDate'] = $appoinmentDate;
                    $link['appoinmentTime'] = $appoinmentTime;
                    $link['appointmentType'] = 'defect';
                    $link['defect'] = $val['category'].'. '.$val['level'].'. '.$val['location'].'. '.$val['issue'];
                    
                    $customerDetails = $this->customerModel->getCustomerDetailsById($unitNumber);
                    $link['ownerName'] = $customerDetails['ownerName'];
                    $link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
                    $message =   $this->load->view('email/Accept _ rejectemail',$link,true);
                    $to = $this->optionModel->getOptionByName('adminEmail');
                    $csTeam = csTeam();
                    $ICSBTeam = ICSBTeam();
                    $PIC = PICTeam();

                    $cc  = array_merge($csTeam,$ICSBTeam,$PIC);
                    sendEmail($to,$subject,$message,$cc);
                    
                    //save log
                    $logText = $customerDetails['unitNumber'].' created an appointment.';
                    $logArray = array('logText'=>$logText,'appointmentId'=>$insert_id,'action'=>'open','tanentId'=>$unitNumber);
                    $this->customerModel->insertLog($logArray);
                    
                    //save notification
                    $notificationText = 'New Defect Appointment request by '.$customerDetails['unitNumber'];
                    $notificationArray= array('appointmentId'=>$insert_id,'notificationType'=>'admin','notificationStatus'=>'unread','tenantId'=>$unitNumber,'notificationText'=>$notificationText);
                    $this->customerModel->saveNotification($notificationArray);
                }
			}
			$this->customerModel->updateHandoverAppoinment($insertId,array('multiDefect'=>1));
            
                $json_data = array(
								"status"=>'success',
								"type"=>'',
								"message"            => "Record inserted successfully.");
					_showMessage('Record inserted successfully.','success');	
		}
		if($insertId&&count($defectValue)==1)
				{
					
					$subject = html_entity_decode(stripslashes('IBRACO:NEW DEFECT APPOINTMENT'));
					
					$appointmentId = $this->encryption->encrypt($insertId);
					$appointmentId =str_replace(array('+', '/', '='), array('-', '_', '~'), $appointmentId);
					
					$link['accept'] = base_url('admin/editDefectAppointment/'.$insertId.'/accept');
					$link['reject'] = base_url('admin/rejectAppointment/'.$appointmentId.'/rejected');
					$link['appoinmentDate'] = $appoinmentDate;
					$link['appoinmentTime'] = $appoinmentTime;
					$link['appointmentType'] = 'defect';
					$customerDetails = $this->customerModel->getCustomerDetailsById($unitNumber);
                    $link['ownerName'] = $customerDetails['ownerName'];
                    $link['customerName'] = $customerDetails['customerName'];
					$link['unitNumber'] = isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'';
					$message =   $this->load->view('email/Accept _ rejectemail',$link,true);
					$to = $this->optionModel->getOptionByName('adminEmail');
                    $csTeam = csTeam();
                    $ICSBTeam = ICSBTeam();
                    $PIC = PICTeam();

                    $cc  = array_merge($csTeam,$ICSBTeam,$PIC);
					sendEmail($to,$subject,$message,$cc);
					
					//save log
					$logText = $customerDetails['unitNumber'].' created an appointment.';
					$logArray = array('logText'=>$logText,'appointmentId'=>$insertId,'action'=>'open','tanentId'=>$unitNumber);
					$this->customerModel->insertLog($logArray);
					
					//save notification
					$notificationText = 'New Defect Appointment request by '.$customerDetails['unitNumber'];
					$notificationArray= array('appointmentId'=>$insertId,'notificationType'=>'admin','notificationStatus'=>'unread','tenantId'=>$unitNumber,'notificationText'=>$notificationText);
					$this->customerModel->saveNotification($notificationArray);
					$json_data = array(
								"status"=>'success',
								"type"=>'',
								"message"            => "Record inserted successfully.");
					_showMessage('Record inserted successfully.','success');		
				}
        }
				
				echo json_encode($json_data);
    }
    
    public function editDefectAppointment()
	{
		if($this->session->userdata('ad')['userRole'] == "admin"){
            $this->session->unset_userdata('last_page');
			
			$appointmentId = $this->uri->segment(3);
			
			 
			 $appointmentDetails = $this->customerModel->_getAppointmentById($appointmentId);
             $appointmentDetails['commentDetails'] = $this->customerModel->_getAppointmentCommentsDetails($appointmentId);
             $appointmentDetails['parentCommentDetails'] = $this->customerModel->getParentAppointmentDetails($appointmentId);
			 
             $data['emailList'] = $this->adminModel->getDepartmentEmail('','','','','','','Customer service PIC');
				//if($appointmentDetails['appointmentStatus']=='open')
				$data['appointmentDetails'] = $appointmentDetails;
                
                //_print_r($appointmentDetails);
			 
			$data['title'] = 'Handover';
			$this->adminTemplate($data['title'],'handover',$data);
		}
		else
		{
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
			redirect('admin/logout');
		}
	}
    public function excelLoad()
    {
        $status = $this->input->get('status');
        $date = $this->input->get('date');
        $appointmentList = $this->adminModel->getAppointmentList('','','','','','',$status,$date);
        //_print_r($appointmentList);exit;
        $this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Appointments');
	$link_style_array = [
  		'font'  => [
   		 'color' => ['rgb' => '1A2226'],
   		 'underline' => 'single'
 		 ]
	];
    
    $this->excel->getActiveSheet()->getStyle('A2:M2')->getFont()->setBold(true)->setSize(12);
// Set it!
 	$this->excel->getActiveSheet()->getStyle("A2")->applyFromArray($link_style_array);
	$this->excel->getActiveSheet()->setCellValue('A2', 'Issue ID');
	$this->excel->getActiveSheet()->setCellValue('B2', 'Teanant Name');
	$this->excel->getActiveSheet()->setCellValue('C2', 'Appointment Time');
    $this->excel->getActiveSheet()->setCellValue('D2', 'Appointment Date');
	$this->excel->getActiveSheet()->setCellValue('E2', 'Unit/Block Number');
	$this->excel->getActiveSheet()->setCellValue('F2', 'Service Request');
	$this->excel->getActiveSheet()->setCellValue('G2', 'Service Provided By');
    $this->excel->getActiveSheet()->setCellValue('H2', 'Defect Categories');
    $this->excel->getActiveSheet()->setCellValue('I2', 'Level');
    $this->excel->getActiveSheet()->setCellValue('J2', 'Location');
    $this->excel->getActiveSheet()->setCellValue('K2', 'Issue');
	$this->excel->getActiveSheet()->setCellValue('L2', 'Status');
	$this->excel->getActiveSheet()->setCellValue('M2', 'Resolved Date');
	$this->excel->getActiveSheet()->getStyle('A2:M2')->getFont()->setBold(true)->setSize(12);
	foreach(range('A2','M2') as $columnID) {
		$this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}
    if($appointmentList)
	{
		$i=3;
		foreach($appointmentList as $val)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $val['ticketNo']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $val['customerName']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $val['appointmentTime']);
            $this->excel->getActiveSheet()->setCellValue('D'.$i, $val['appointmentDate']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $val['unitNumber']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $val['appointmentType']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $val['contactName']);
            $this->excel->getActiveSheet()->setCellValue('H'.$i, $val['defectCategory']);
            $this->excel->getActiveSheet()->setCellValue('I'.$i, $val['defectLevel']);
            $this->excel->getActiveSheet()->setCellValue('J'.$i, $val['defectLocation']);
            $this->excel->getActiveSheet()->setCellValue('K'.$i, $val['defectIssue']);
			$this->excel->getActiveSheet()->setCellValue('L'.$i, ($val['appointmentStatus']=='accept')?'accepted':$val['appointmentStatus']);
			$this->excel->getActiveSheet()->setCellValue('M'.$i, ($val['appointmentStatus']=='resolved')?$val['statusUpdatedOn']:'');

			$i++;
		}
	}
	
	$filename='IbracoAccount'.time().'.xls';
	header('Content-Type: application/vnd.ms-excel'); //mime type
	header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
	//header('Cache-Control: max-age=0'); //no cache\


             
    //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
    //if you want to save it as .XLSX Excel 2007 format
    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    ob_end_clean();
    
    //force user to download the Excel file without writing it to server's HD
    $objWriter->save('php://output');
    
    }
    
    public function sendEmail()
    {
        $customerId = $this->input->post('id');
        $customerDetails = $this->customerModel->getCustomerDetailsById($customerId);
        if($customerDetails&&$customerDetails['customerEmail'])
        {
            $subject = 'Welcome to TT3 Plaza - Sign Up Email';
            $link['unitNumber'] = $customerDetails['unitNumber'];
            $link['IC_no'] = ($customerDetails['IC_no']);
            $message =   $this->load->view('email/SignUp',$link,true);
            $to = $customerDetails['customerEmail'];
            if(sendEmail($to,$subject,$message))
            {
                $password =password_hash($customerDetails['IC_no'], PASSWORD_DEFAULT, ['cost' => 11]);
                $this->customerModel->update_customer($customerDetails['customerId'],array('customerPassword'=>$password,'emailSent'=>1));
            }
        }
    }
    
    public function exportTenant()
    {
        $customerList = $this->customerModel->getAllCustomersList();
        
         $this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('TenantList');
	$link_style_array = [
  		'font'  => [
   		 'color' => ['rgb' => '1A2226'],
   		 'underline' => 'single'
 		 ]
	];
        $this->excel->getActiveSheet()->getStyle('A2:M2')->getFont()->setBold(true)->setSize(12);
// Set it!
 	$this->excel->getActiveSheet()->getStyle("A2")->applyFromArray($link_style_array);
	$this->excel->getActiveSheet()->setCellValue('A2', 'Unit Number');
	$this->excel->getActiveSheet()->setCellValue('B2', 'Teanant Name');
	$this->excel->getActiveSheet()->setCellValue('C2', 'I/C Number');
    $this->excel->getActiveSheet()->setCellValue('D2', 'Primary Number');
	$this->excel->getActiveSheet()->setCellValue('E2', 'Emergency Contact');
	$this->excel->getActiveSheet()->setCellValue('F2', 'Emergency Contact Number');
	$this->excel->getActiveSheet()->setCellValue('G2', 'Email');
	$this->excel->getActiveSheet()->setCellValue('H2', 'Authorized Person');
    $this->excel->getActiveSheet()->setCellValue('I2','Authorized person Contact No');
    $this->excel->getActiveSheet()->setCellValue('J2','Vacant Possession Letter Date');
    $this->excel->getActiveSheet()->setCellValue('K2','Occupation Permit Date');
    $this->excel->getActiveSheet()->setCellValue('L2','Handover Date');
    $this->excel->getActiveSheet()->setCellValue('M2','Defect Liability Expiry Date');
    
	$this->excel->getActiveSheet()->getStyle('A2:M2')->getFont()->setBold(true)->setSize(12);
	foreach(range('A2','M2') as $columnID) {
		$this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	}
    if($customerList)
	{
		$i=3;
		foreach($customerList as $val)
		{
			$this->excel->getActiveSheet()->setCellValue('A'.$i, $val['unitNumber']);
			$this->excel->getActiveSheet()->setCellValue('B'.$i, $val['customerName']);
			$this->excel->getActiveSheet()->setCellValue('C'.$i, $val['IC_no']);
            $this->excel->getActiveSheet()->setCellValue('D'.$i, $val['customerMobile']);
			$this->excel->getActiveSheet()->setCellValue('E'.$i, $val['customerAlternatecontactName']);
			$this->excel->getActiveSheet()->setCellValue('F'.$i, $val['customerAlternateMobile']);
			$this->excel->getActiveSheet()->setCellValue('G'.$i, $val['customerEmail']);
			$this->excel->getActiveSheet()->setCellValue('H'.$i, $val['authorized_person']);
            $this->excel->getActiveSheet()->setCellValue('I'.$i, $val['authorized_person_contact_no']);
            $this->excel->getActiveSheet()->setCellValue('J'.$i, $val['vacant_possession_letter_date']);
            $this->excel->getActiveSheet()->setCellValue('K'.$i, $val['occupation_permit_date']);
            $this->excel->getActiveSheet()->setCellValue('L'.$i, $val['handover_date']);
            $this->excel->getActiveSheet()->setCellValue('M'.$i, $val['defect_liability_expiry_date']);
			$i++;
		}
	}
	
	$filename='IbracoTenant'.time().'.xls';
	header('Content-Type: application/vnd.ms-excel'); //mime type
	header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
	//header('Cache-Control: max-age=0'); //no cache\


             
    //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
    //if you want to save it as .XLSX Excel 2007 format
    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
    ob_end_clean();
    
    //force user to download the Excel file without writing it to server's HD
    $objWriter->save('php://output');
    }
    
    public function deleteCommentImage()
    {
        $url = $this->input->post('url');
        $imageId = explode('?',$url);
        $id = explode('=', $imageId[1]);
        $json = array('error'=>'',
                          'status'=>'fail',
                          'message'=>'');
        if($this->adminModel->deleteCommentImage($id[1]))
        {
            $json = array('error'=>'',
                          'status'=>'success',
                          'message'=>'Image deleted successfully.');
        }
        
        echo json_encode($json);
    }
    
	public function adminTemplate($title='',$template,$data)
	{
		$data['title'] = $title;
        $data['notificationCount'] = $this->adminModel->getNotificationCount('count');
		$this->load->view('admin/common/header',$data);
		$this->load->view('admin/common/sidebar',$data);
		$this->load->view('admin/'.$template,$data);
		$this->load->view('admin/common/footer',$data);
	}
    
    private function uploadFile($file)
{

if ( ! empty($_FILES))
		{
			$filename = array();
			$this->config->load('upload');
			$config = $this->config->item('appointment');
 			$config['upload_path'] = $config['upload_path'];
			if(!is_dir($config['upload_path'])) //create the folder if it's not already exists
   				 {
      				mkdir($config['upload_path'],0755,TRUE);
   				 } 
			$this->load->library('upload');
			$files           = $file;
			
			$errors = 0;

			// codeigniter upload just support one file
			// to upload. so we need a litte trick
			
				$_FILES['file']['name'] = mktime(date("h"),date("i"),date("s"),date("m"),date("d"),date("y"))."_".$files['name'] ;
				$_FILES['file']['type'] = $files['type'];
				$_FILES['file']['tmp_name'] = $files['tmp_name'];
				$_FILES['file']['error'] = $files['error'];
				$_FILES['file']['size'] = $files['size'];
				// we have to initialize before upload
				$this->upload->initialize($config);

				if (! $this->upload->do_upload("file")) {
					$errors++;
				}else{
					$finfo=$this->upload->data();
					$filename = ($finfo['file_name']);
				}
			
			if(isset($filename) && !empty($filename)){	
				return $filename;
			}

			if ($errors > 0) {
				return  FALSE;
			}

		}	
	}
    
    

}
?>