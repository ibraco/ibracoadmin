

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                
                <div class="row">
                   <?php echo $this->session->flashdata('msg');?>
                    <div class="col-xl-6 col-xxl-12 col-sm-12">
                        <div class="card">
                            <img class="bg-image" src="<?php echo base_url('lib/images/');?>reset-password-real-estate-agent-handing-the-house-key-to-pe.png">
                            <img class="rectangle bg-overlay" src="<?php echo base_url('lib/images/');?>reset-password-rectangle.png">
                            <img class="profile-oval" src="<?php echo base_url('lib/images/');?>reset-password-oval2x.png">
                            <img class="profile-shape" src="<?php echo base_url('lib/images/');?>profile-shape32x.png">
                            
                        </div>
                    </div>
                    
                    
                    
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-xxl-12 col-xl-6">
                        <div class="card">
                            <div class="card-body">
                                <?php echo form_open('customer/resetpassword')?>
                                
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-label">Old Password</label>
                                    <div class="col-sm-4">
                                        <input type="password" class="form-control" name="oldpassword" value="">
                                        <span class="form-text text-danger" ><?php echo form_error('oldpassword');?></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-label">New Password</label>
                                    <div class="col-sm-4">
                                        <input type="password" class="form-control" name="newpassword" value="">
                                        <span class="form-text text-danger" ><?php echo form_error('newpassword');?></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-label">Confirm New Password</label>
                                    <div class="col-sm-4">
                                        <input type="password" name="cpassword" class="form-control" value=""></label>
                                        <span class="form-text text-danger" ><?php echo form_error('cpassword');?></span>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group div-btn">
                                                <button id="submitCustomer" data-value="" type="submit" class="btn btn-success btn-ft sett-btn">Submit</button>
                                            </div>
                                
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
                
                
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        