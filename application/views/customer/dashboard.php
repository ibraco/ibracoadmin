
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                
                <div class="row">
                    
                    <div class="col-xl-6 col-xxl-12 col-sm-12">
                        <div class="card">
                            <img class="bg-image" src="<?php echo base_url('lib/images/');?>reset-password-real-estate-agent-handing-the-house-key-to-pe.png">
                            <img class="rectangle bg-overlay" src="<?php echo base_url('lib/images/');?>reset-password-rectangle.png">
                            <div class="group4 button-appointment">
                              <div class="letsscheduleanapp">Lets schedule an appointment for:</div>
                              <a href="<?php echo base_url('customer/handover');?>">
                                    <div class="group">
                                          <img class="rectangle1" src="<?php echo base_url('lib/images/');?>home-rectangle2@2x.png">
                                          <div class="handover">Handover</div>
                                    </div>
                              </a>
                              <a href="<?php echo base_url('customer/defect');?>">
                                    <div class="groupcopy">
                                          <img class="rectangle1" src="<?php echo base_url('lib/images/');?>home-rectangle2@2x.png">
                                          <div class="defect">Defect</div>
                                    </div>
                              </a>
                              
                            </div>
                            <div class="dashboard bg1"></div>
                    <div class="myappointment"><span><i class="mdi mdi-clock "></i></span>My Appointment</div>
                        </div>
                    </div>
                    
                    
                    
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-xl-8 view-tenant">
                        <div id="accordion-three" class="accordion">
                                <?php if(!empty($appointmentList)){
                                        foreach($appointmentList as $appo){
                                                
                                        $appointmentId = $this->encryption->encrypt($appo['appointmentId']);
                                        $appointmentId =str_replace(array('+', '/', '='), array('-', '_', '~'), $appointmentId);?>
                                        <div>
                                                <div class="card notification <?php echo $appo['appointmentStatus'];?>-stage">
                                 
                                                <div class="box">
                                          <div class="box-header with-border">
                                            <h3 class="box-title noti-title"><?php echo ($appo['appointmentType']=='handover')?'Handover':'Defect'.($appo['defectCategory']?'. '.$appo['defectCategory']:'').($appo['defectLevel']?'. '.$appo['defectLevel']:'').($appo['defectIssue']?'. '.$appo['defectIssue']:'')?></h3>
                                            <div class="noti-num">#<?php echo $appo['ticketNo'];?></div>
                                                <div class="noti-time ">
                                                      <span><i class="mdi mdi-clock " style=""></i></span><?php echo date('j M Y',strtotime($appo['appointmentDate'])).' '.$appo['appointmentTime'];?>
                                                </div>
                                                
                                                
                                               <?php if($appo['appointmentStatus']=='open') {?>
                                               
                                               <div class="box-tools pull-right">
                                                <?php if($appo['appointmentType']=='handover'){?>
                                              <a href="<?php echo base_url('customer/editAppointment/'.$appointmentId);?>" class="edit-icon"><i class="mdi mdi-pencil-circle noti-icon"></i></a>
                                              <?php }elseif($appo['appointmentType']=='defect') {?>
                                              <a href="<?php echo base_url('customer/editDefectAppointment/'.$appointmentId);?>" class="edit-icon"><i class="mdi mdi-pencil-circle noti-icon"></i></a>
                                              <?php }?>
                                              <button type="button" class="btn btn-box-tool collapsed"  data-toggle="collapse" data-target="#collapseTwo<?php echo $appo['appointmentId'];?>" aria-expanded="false" aria-controls="collapseTwo5">
                                                <i class="fa fa-minus"></i></button>
                                              <!--<a href="<?php echo base_url('customer/deleteAppointment/'.$appointmentId);?>" class="edit-icon removeAppointment" onclick="return false;" data-id="" data-value="<?php echo $this->security->get_csrf_hash();?>"><i class="mdi mdi-close-circle noti-icon"></i></a>-->
                                              
                                            </div>
                                               <?php }elseif($appo['appointmentStatus']=='resolved'||$appo['appointmentStatus']=='completed'){?>
                                               
                                               <div class="box-tools pull-right">
                                              <?php if($appo['appointmentType']=='handover'){?>
                                              <a href="<?php echo base_url('customer/editAppointment/'.$appointmentId);?>" class="edit-icon"><i class="mdi mdi-pencil-circle noti-icon"></i></a>
                                              <?php }elseif($appo['appointmentType']=='defect') {?>
                                              <a href="<?php echo base_url('customer/editDefectAppointment/'.$appointmentId);?>" class="edit-icon"><i class="mdi mdi-pencil-circle noti-icon"></i></a>
                                              <?php }?>
                                              <button type="button" class="btn btn-box-tool collapsed"  data-toggle="collapse" data-target="#collapseTwo<?php echo $appo['appointmentId'];?>" aria-expanded="false" aria-controls="collapseTwo5">
                                                <i class="fa fa-minus"></i></button>
                                              
                                            </div>
                                               
                                               <?php }
                                               
                                               else{?>
                                            <div class="box-tools pull-right">
                                              <button type="button" class="btn btn-box-tool collapsed"  data-toggle="collapse" data-target="#collapseTwo<?php echo $appo['appointmentId'];?>" aria-expanded="false" aria-controls="collapseTwo5">
                                                <i class="fa fa-minus"></i></button>
                                              
                                            </div>
                                            <?php }?>
                                            <div class="noti-notes"><?php echo $appo['appointmentNotes']?$appo['appointmentNotes']:'&nbsp;';?></div>
                                            <div><i class="mdi mdi-account user-account"><?php echo ($appo['contractorName'])?$appo['contractorName']:'Unassigned';?></i></div>
                                            <div class="pull-right noti-dates"><?php echo date('d M Y',strtotime($appo['addedOn']));?></div>
                                          </div>
                                          
                                    </div>
                                    
                              </div>
                              <div id="collapseTwo<?php echo $appo['appointmentId'];?>" class="collapse" data-parent="#accordion-three">
                                          <div class="box-body">
                                            &nbsp;
                                          </div>
                                          <!-- /.box-body -->
                                          <div class="box-footer">
                                                <?php if(isset($appo['commentDetails'])&&!empty($appo['commentDetails'])) {?>
                                            <div class="timeline_content">
                            
                                                <ul class="timeline comment-timeline">
                                                    <?php 
                                                            foreach($appo['commentDetails'] as $commentDetails){
                                                               
                                                                if(isset($commentDetails['appointmentCommentId'])&&$commentDetails['visiblefortenant']>0){?>
                                                    <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget comment-list">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['appointmentComment'];?></p>
                                                                    <div id="gallery<?php echo $commentDetails['appointmentCommentId']?>" class="gallery1">
                                                                        
                                                                    </div>
                                                                    <?php
                                                                    $imageArray=array();
                                                                    if(isset($commentDetails['commentImage'])) {
                                                                    foreach($commentDetails['commentImage'] as $image){
                                                                        
                                                                        $imageArray[] = (base_url('uploads/appointment/'.$image['image']));
                                                                        ?>
                                                                        
                                                                    <?php }
                                                                    //_print_r($imageArray);
                                                                    $html ='';
                                                                     if( isset($imageArray)){
                                                                        
                                                                        $i=0;
                                                                        foreach($imageArray as $img){
                                                                         $html .="'".$img."'";
                                                                        if(isset($imageArray[$i+1]))
                                                                         $html .=',';
                                                                        $i++;
                                                                        }
                                                                        }?>
                                                                   
                                                                    <script>
                                                                        //var images = [];
                                                                        var images = [<?php echo $html;?>];
                                                                        $(function() {
                                                                       
                                                                                       $('#gallery<?php echo $commentDetails['appointmentCommentId']?>').imagesGrid({
                                                                                               images:[<?php echo $html;?>]
                                                                                               });
                                                                                       
                                                                       
                                                                                   });</script>
                                                                    <?php }?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php }elseif(isset($commentDetails['logId'])){ ?>
                                                     <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget comment-list">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['logText'];?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                     <?php } }?>
                                                    
                                                </ul>
                                        </div>
                                            <?php }
                                            
                                            if(isset($appo['parentAppointment'])&&!empty($appo['parentAppointment'])) {
                                                foreach($appo['parentAppointment'] as $parent){
                                                ?>
                                            
                                            
                                            <div class="card notification timeline_content" id="parent-accordion">
                                 
                                    <div class="box">
                                          <div class="box-header with-border">
                                            <h3 class="box-title noti-title">Handover</h3>
                                            <div class="noti-num">#<?php echo $parent['ticketNo']?></div>
                                                <div class="noti-time ">
                                                      <span><i class="mdi mdi-clock " style=""></i></span><?php echo $parent['appointmentTime']?>                                               </div>
                                                
                                                
                                                                                           <div class="box-tools pull-right">
                                              <button type="button" class="btn btn-box-tool collapsed" data-toggle="collapse" aria-expanded="false" data-target="#collapseTwo<?php echo $parent['appointmentId'];?>" aria-controls="collapseTwo5">
                                                <i class="fa fa-minus"></i></button>
                                              
                                            </div>
                                        <div class="noti-notes"><?php echo $parent['appointmentNotes'];?></div>
                                          </div>
                                          
                                    </div>
                                    
                              </div>
                                            <div id="collapseTwo<?php echo $parent['appointmentId'];?>" class="collapse" data-parent="#parent-accordion">
                                          <div class="box-body">
                                            &nbsp;
                                          </div>
                                          <!-- /.box-body -->
                                          <div class="box-footer">
                                                <?php if(isset($parent['commentDetails'])&&!empty($parent['commentDetails'])) {?>
                                            <div class="timeline_content">
                            
                                                <ul class="timeline comment-timeline">
                                                    <?php 
                                                            foreach($parent['commentDetails'] as $commentDetails){
                                                                if(isset($commentDetails['appointmentCommentId'])&&$commentDetails['visiblefortenant']>0) {?>
                                                    <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget comment-list">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo $appo['appointmentTime'];?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['appointmentComment'];?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php }
                                                    elseif(isset($commentDetails['logId'])){ ?>
                                                     <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget comment-list">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['logText'];?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                     <?php } }?>
                                                    
                                                </ul>
                                        </div>
                                            <?php }?>
                                          </div>
                                            </div>
                                            <?php } }?>
                                            
                                          </div>
                                          <!-- /.box-footer-->
                                          </div>
                                        </div>
                              <?php } }?>
                              <!--<div class="card notification open-stage">
                                 
                                    <div class="box">
                                          <div class="box-header with-border">
                                            <h3 class="box-title">Title</h3>
                                  
                                            <div class="box-tools pull-right">
                                              <button type="button" class="btn btn-box-tool collapsed"  data-toggle="collapse" data-target="#collapseTwo5" aria-expanded="false" aria-controls="collapseTwo5">
                                                <i class="fa fa-minus"></i></button>
                                              
                                            </div>
                                          </div>
                                          <div id="collapseTwo5" class="collapse" data-parent="#accordion-three">
                                          <div class="box-body">
                                            Start creating your amazing application!
                                          </div>
                                          
                                          <div class="box-footer">
                                            Footer
                                          </div>
                                          
                                          </div>
                                    </div>
                                    
                              </div>-->
                        </div>
                    </div>
                </div>
                
                
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        <div class="footer color-div">
                <div class="col-md-12">
                        <ul class="color-tone">
                                <li class="tone-li"><div class="open-tone"></div><span>Open</span></li>
                                <li class="tone-li"><div class="cancelled-tone"></div> <span>   Cancelled</span></li>
                                <li class="tone-li"><div class="pending-tone"></div><span>   Pending</span></li>
                                <li class="tone-li"><div class="rejected-tone"></div><span>   Rejected</span></li>
                                <li class="tone-li"><div class="resolved-tone"></div><span>   Resolved</span></li>
                                <li class="tone-li"><div class="closed-tone"></div><span>   Closed</span></li>
                                <li class="tone-li"><div class="noshow-tone"></div><span>   No show</span></li>
                                
                        </ul>
                </div>
        </div>
        