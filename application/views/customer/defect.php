      <div id="load_popup_modal1_show_id" class="modal fade" tabindex="-1"></div>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                
                <div class="row">
                    <div class="msg" style="width: 100%"></div>
                    <div class="col-xl-6 col-xxl-12 col-sm-12">
                        <div class="card">
                         <div class="card-body">
                            <div class="row mt-5">
                <div class="col-md-8 set-sm-fit mb-4">
                    <form action="#">
                        <div class="preference-title">
                            <h4>Defect Preferences</h4>
                        </div>
                        <!-- preferences Wrap -->
                        <div class="preferences">
                            <!-- Styled radio btn 1 -->
                            <div class="preference-radio">
                                <p>Choose a date?</p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-4 sm-box">
                                        <div class="input-group date insertInfo bitmap " data-provide="datepicker">
                                        <input type="text" class="form-control" id="appoinmentDate" value="<?php echo isset($appointmentDetails['appointmentDate'])?date('d/m/Y',strtotime($appointmentDetails['appointmentDate'])):''?>">
                                        <div class="input-group-addon close-button">
                                          <i class="mdi mdi-calendar" style="font-size: 16px ;margin-left:-6px"></i>
                                        </div>
                                      </div>
                                        <p id="appoinmentDateError" class="form-text text-danger"></p>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <!--// Styled radio btn 1 -->
                            <!-- Styled radio btn 2 -->
                            <div class="preference-radio mt-4">
                                <p>Available Time</p>
                                <div class="row">
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                                <div class="btn-group radio-group a800am900am1" >
                                                   <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='9:00 AM - 10:00 AM')?'active':''?>">
                                                     <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='9:00 AM - 10:00 AM')?'checked':''?>  autocomplete="off" value="9:00 AM - 10:00 AM" > 9:00 AM - 10:00 AM
                                                   </label>
                                                </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                           <div class="input-group">
                                           <div class="btn-group radio-group a800am900am1" >
                                             <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='10:00 AM - 11:00 AM')?'active':''?>">
                                               <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='10:00 AM - 11:00 AM')?'checked':''?>  autocomplete="off" value="10:00 AM - 11:00 AM" > 10:00 AM - 11:00 AM
                                             </label>
                                           </div>
                                           </div>
                                           </div>
                                    </div>
                                    
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group radio-group a800am900am1" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='1:15 PM - 2:15 PM')?'active':''?>">
                                              <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='1:15 PM - 2:15 PM')?'checked':''?>  autocomplete="off" value="1:15 PM - 2:15 PM" > 1:15 PM - 2:15 PM
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                           <div class="input-group">
                                           <div class="btn-group radio-group a800am900am1" >
                                             <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='2:15 PM - 3:15 PM')?'active':''?>">
                                               <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='2:15 PM - 3:15 PM')?'checked':''?>  autocomplete="off" value="2:15 PM - 3:15 PM" > 2:15 PM - 3:15 PM
                                             </label>
                                           </div>
                                           </div>
                                           </div>
                                    </div>
                                    
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group radio-group a800am900am1" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='3:15 PM - 4:15 PM')?'active':''?>">
                                              <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='3:15 PM - 4:15 PM')?'checked':''?>  autocomplete="off" value="3:15 PM - 4:15 PM" > 3:15 PM - 4:15 PM
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                            </div>
                            <div class="row">
                              <p>Defect List</p>
                                <div class="preference-radio mt-4">
                                    
                                        <div id="defectList">
                                                
                                        </div>
                                        <p id="categoryError" class="form-text text-danger"></p>
                                </div>
                            </div>
                            <div class="preference-radio mt-4">
                                
                                <button type="button" id="" href="javascript:void(0)" class=" button  btn-defect-list" data-toggle="modal" data-value="<?php echo $this->security->get_csrf_hash();?>"> Add Defect</button>
                            </div>
                            
                            <!--// Styled Check box -->
                            <div class="preferences">
                                <!-- Comment box -->
                                <p>Do you have any notes? <span class="optional-fade">(optional)</span></p>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                          <div class="form-group">                                          
                                        <textarea id="notes" class="form-control optinal-textarea col-md-10 col-sm-12" style="text-align: right;height: 85px;"><?php echo (isset($appointmentDetails['appointmentNotes']))?$appointmentDetails['appointmentNotes']:''?></textarea>
                                         <div style="position: relative;overflow: hidden;top: -37px;left: -23px;width: 75px;height: 44px;" class="file btn btn-lg">
                                                <i class="mdi mdi-file-outline"></i>
                                                <input type="file" id="commentfile" name="commentfile" style="position: absolute;opacity: 0;top: 4px;cursor: pointer;">
						</div>
                                          </div>
                                        <p id="notesError" class="form-text text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            <!--// Personal Details -->
                        </div>
                        <!--// preferences Wrap -->
                    </form>
                </div>

                <!-- Booking Summary -->
                <div class="col-md-4 set-sm-fit">
                    <div style="height: auto;"></div><div data-toggle="affix" class="">
                        <!-- data-toggle="affix" -->
                        <div class="preference-title">
                            <h4>Booking Summary</h4>
                        </div>
                        <div class="fesilities">
                            <ul>
                                <li><i class="fa fa-paint-brush" aria-hidden="true"></i>
                                    <p>Defect</p>
                                </li>
                                <li id="handOver-date"><i class="fa fa-calendar" aria-hidden="true"></i>
                                    <p><?php echo isset($appointmentDetails['appointmentDate'])?date('D d M Y',strtotime(str_replace('/','-',$appointmentDetails['appointmentDate']))):'-'?></p>
                                </li>
                                <li id="handOver-time"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p><?php echo isset($appointmentDetails['appointmentTime'])?$appointmentDetails['appointmentTime']:'-'?></p>
                                </li>
                                <li><i class="fa fa-refresh" aria-hidden="true"></i>
                                    <p>1 hour</p>
                                </li>
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <p><?php echo isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:'-'?></p>
                                </li>
                            </ul>
                            <input type="hidden" id="appointmentId" value="<?php echo isset($appointmentDetails['appointmentId'])?$appointmentDetails['appointmentId']:'';?>">
                            <button type="button" id="" href="javascript:void(0)" class=" button  submit-defect" data-toggle="modal" data-value="<?php echo $this->security->get_csrf_hash();?>"> Submit</button>
                        </div>
                    </div>

                </div>
                <!--// Booking Summary -->
            </div>
                        </div>
                         </div>
                    </div>
                    
                    
                    
                </div>
                
                
                
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        