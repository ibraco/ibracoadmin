<!DOCTYPE html>
<!-- To get the source code upgrade to a Pro plan and Export code https://launchpad.animaapp.com/pricing - Sample Exported Code: https://medium.com/sketch-app-sources/how-to-export-sketch-to-html-65c427b3aa3a -->
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"><link rel="shortcut icon" type="image/png" href="https://animaapp.s3.amazonaws.com/home/favicon.png"><meta charset="UTF-8"><meta name="viewport" content="width=1024, maximum-scale=1.0">

<link href="<?php echo base_url('lib/admin/css/custom.css');?>" rel="stylesheet">
<link href="<?php echo base_url('lib/css/handover.css');?>" rel="stylesheet">
<link href="<?php echo base_url('lib/admin/login/bootstrap/css/bootstrap.css');?>" rel="stylesheet">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
<link href="<?php echo base_url('lib/datepicker/css/datepicker.css');?>" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" rel="stylesheet">
<link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
<style>
 .radio-group label {
   overflow: hidden;
} .radio-group input {
    /* This is on purpose for accessibility. Using display: hidden is evil.
    This makes things keyboard friendly right out tha box! */
   height: 1px;
   width: 1px;
   position: absolute;
   top: -20px;
} .radio-group .not-active  {
   color: #3276b1;
   background-color: #fff;
}
</style>
</head>
<body style="margin: 0;
 background: rgba(255, 255, 255, 1.0);">
 <input type="hidden" id="anPageName" name="page" value="handover">
 <div class="handover">
  <div style="width: 1024px; height: 100%; position:relative; margin:auto;">
   <div class="bg"></div>
   <form class="">
   <div class="rectangle"></div>
       <div class="input-group date insertInfo bitmap" data-provide="datepicker">
  <input type="text" class="form-control">
  <div class="input-group-addon close-button">
    <span class="glyphicon glyphicon-calendar"></span>
  </div>
</div>
   
   <div class="chooseadate">Choose a date?</div>
   <div class="doyouhaveanynote">Do you have any Notes? (optional)</div>
   <div class="group">
       <div class="group42">
       <!--<div class="a800am900am1">8:00 AM -&nbsp;&nbsp;9:00 AM</div>-->
       <div class="form-group">
       <div class="input-group">
       <div class="btn-group radio-group a800am900am1" >
                                                      <label class="btn btn-primary  day-btn ">
                                                        <input type="radio" name="appoinmentTime"  autocomplete="off" value="mon" > 8:30 AM -&nbsp;&nbsp;10:30 AM
                                                      </label>
       </div>
       </div>
       </div>
       </div>
       <div class="group4copy">
       <!--<div class="rectangle2"></div>
       <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>-->
       <div class="form-group">
       <div class="input-group">
       <div class="btn-group radio-group a800am900am1" >
                                                      <label class="btn btn-primary  day-btn ">
                                                        <input type="radio" name="appoinmentTime"  autocomplete="off" value="mon1" > 1:30 AM -&nbsp;&nbsp;3:30 AM
                                                      </label>
       </div>
       </div>
       </div>
       </div>
   </div>
   
   
   
   <!--<div class="group">
    <div class="group42">
   <div class="rectangle2"></div>
   <div class="a800am900am1">8:00 AM -&nbsp;&nbsp;9:00 AM</div>
   </div><div class="group4copy"><div class="rectangle2"></div>
   <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
   </div>
   </div>
   <div class="group">
    <div class="group42">
   <div class="rectangle2"></div>
   <div class="a800am900am1">8:00 AM -&nbsp;&nbsp;9:00 AM</div>
   </div><div class="group4copy"><div class="rectangle2"></div>
   <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
   </div>
   </div>-->
   <!--<div class="groupcopy">
    <div class="group4copy8">
     <div class="rectangle2"></div>
     <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
    </div><div class="group4copy81"><div class="rectangle2"></div>
    <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
    </div><div class="group4copy82"><div class="rectangle2"></div>
    <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div></div>
    <div class="group4copy83"><div class="rectangle2"></div>
    <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
    </div><div class="group4copy84"><div class="rectangle2"></div>
    <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
    </div><div class="group4copy85"><div class="rectangle2"></div>
    <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
    </div><div class="group4copy86"><div class="rectangle2"></div>
    <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
    </div><div class="group4copy87"><div class="rectangle2"></div>
    <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
    </div><div class="group4copy88"><div class="rectangle2"></div>
    <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
    </div><div class="group4copy89"><div class="rectangle2"></div>
    <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
    </div>
   </div>-->
   <!--<div class="group4"><img class="rectangle2" src="<?php echo base_url('lib/images/');?>defect-rectangle2x.png">
   <div class="a830am1030am">8:30 AM -&nbsp;&nbsp;10:30 AM</div>
   </div>-->
   <div class="rectanglecopy2">
    
   </div>
   <textarea class="bitmap1" ></textarea>
   <div class="availabletime">Available time</div><div class="handover1">Handover</div>
   <div class="tues31nov2018">Tues. 31 Nov 2018</div><div class="a800am900am">8:00 AM - 9:00 AM</div>
   <div class="a1hours">1 hours</div>
   <div class="b1010continewb">
   <span class="span1">B-10-10 Continew </span>
   <span class="span2">,<br></span>
   <span class="span3">brickfield</span>
   <span class="span4">,</span>
   <span class="span5">KL</span>
   </div>
   <img class="bitmap2" src="<?php echo base_url('lib/images/');?>defect-bitmap%252012x.png" >
   <img class="rectangle1" src="<?php echo base_url('lib/images/');?>handover-rectangle%25201.png" ><img class="rectanglecopy" src="<?php echo base_url('lib/images/');?>defect-copy-2-rectangle-copy2x.png" >
   <a href="javascript:void(0)">
    <img class="rectanglecopy3" src="<?php echo base_url('lib/images/');?>defect-copy-2-rectangle-copy-32x.png" ></a>
   <div class="handoverpreferences">Handover Preferences</div><div class="bookingsummary">Booking Summary</div>
   <div class="submit"><!--<input type="submit" class="" value="Submit">-->Submit</div>
   </form>
   <div class="bookyourhandovera">Book Your Handover appointment</div>
   <div class="group41">
    <div class="rectangle2"></div>
   <a href="<?php echo base_url('customer/dashboard');?>">
   <img class="bitmap3" src="<?php echo base_url('lib/images/');?>edit-profile-bitmap2x.png"></a>
   <a href="<?php echo base_url('customer/profile');?>">
    <div class="group2">
     <div class="label">Profile</div>
    <div class="icon">
    <img class="shape" src="<?php echo base_url('lib/images/');?>defect-shape2x.png">
    <img class="shape1" src="<?php echo base_url('lib/images/');?>defect-copy-2-shape%252012x.png">
    </div>
    </div>
   </a>
   <div class="group1">
    <a href="<?php echo base_url('customer/logout');?>">
     <div class="group3">
      <div class="logout">Logout</div>
     </div>
    </a>
    <img class="shape" src="<?php echo base_url('lib/images/');?>defect-copy-shape%252022x.png">
   </div>
   </div>
   <div class="fillinalltheappo">Fill in all the appointment detail</div>
  </div>
 </div>
 <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
 <script src="<?php echo base_url('lib/admin/login/jquery/jquery-3.2.1.min.js');?>"></script>
 
 <script src="<?php echo base_url('lib/datepicker/js/bootstrap-datepicker.js');?>"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
 
 <script >
  var today = new Date();
  var MAXDATE = today.getDate()+2;
  
  today.setDate(MAXDATE);
  
  $('.date').datepicker({
  format: 'dd/mm/yyyy',
  todayHighlight: true,
  autoclose: true,
   startDate:today,
            
});
  // $('.date').datepicker("setDate", "+3");

$('.close-button').unbind();

$('.close-button').click(function() {
  if ($('.datepicker').is(":visible")) {
    $('.date').datepicker('hide');
  } else {
    $('.date').datepicker('show');
  }
});
  $(function() {
    // Input radio-group visual controls
    $('.radio-group label').on('click', function(){
   //  alert('ss');
     $('.radio-group label').removeClass('active');
        $(this).addClass('active');
        
    });
});
 </script>
 </body>
</html>