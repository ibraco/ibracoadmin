<!DOCTYPE html>
<!-- To get the source code upgrade to a Pro plan and Export code https://launchpad.animaapp.com/pricing - Sample Exported Code: https://medium.com/sketch-app-sources/how-to-export-sketch-to-html-65c427b3aa3a -->
<html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="UTF-8"><meta name="viewport" content="width=1024, maximum-scale=1.0">
<link href="<?php echo base_url('lib/admin/css/style.css');?>" rel="stylesheet">
<link href="<?php echo base_url('lib/css/profile.css');?>" rel="stylesheet">
<style>
.alert.alert-success.alert-dismissible.fade.in {
    opacity: unset;
}
</style>
<script>

  var baseurl = "<?php print base_url(); ?>";
  var csrfCode = "<?php echo $this->security->get_csrf_hash();?>";
  var token = "<?php echo $this->security->get_csrf_token_name(); ?>";
  var actionUrl = "<?php echo site_url("customer/") ?>";
</script>
</head>
<body style="margin: 0;
 background: rgba(255, 255, 255, 1.0);">
    <div class="profile">
        <div style="width: 1024px; height: 100%; position:relative; margin:auto;">
            <div class="group4">
                <div class="rectangle1"></div>
            <a href="<?php echo base_url('customer/dashboard');?>">
                <img class="bitmap" src="<?php echo base_url('lib/images');?>/edit-profile-bitmap2x.png" ></a>
            <a href="<?php echo base_url('customer/profile');?>">
                <div class="group2"><div class="label">Profile</div>
                <div class="icon">
                <img class="shape2" src="<?php echo base_url('lib/images');?>/defect-shape2x.png">
                <img class="shape1" src="<?php echo base_url('lib/images');?>/defect-copy-2-shape%252012x.png">
                </div>
                </div>
            </a>
            <div class="group">
                <a href="<?php echo base_url('customer/logout');?>">
                    <div class="group3">
                        <div class="logout">Logout</div>
                    </div>
                </a>
                <img class="shape1" src="<?php echo base_url('lib/images');?>/defect-copy-shape%252022x.png" >
            </div>
            </div>
            <img class="realestateagenthandingthehousekeytopeht6ze" src="<?php echo base_url('lib/images');?>/reset-password-real-estate-agent-handing-the-house-key-to-pe.png">
            <img class="rectangle" src="<?php echo base_url('lib/images');?>/reset-password-rectangle.png">
            <div class="bg"></div>
            <?php echo validation_errors('<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><b>Error! </b>', '</div>');
							echo $this->session->flashdata('msg');?>
            <div class="msg" style="position: relative;top: 370px"></div>
            <div class="name">Name</div>
            <div class="nickleong"><?php echo $customerDetails['customerName'];?></div>
            <div class="unitblocknumber">Unit/Block number</div>
            <div class="icnumber">I/C number</div>
            <div class="primarycontactnumb">Primary contact number</div>
            <div class="b1010"><?php echo $customerDetails['unitNumber'];?> </div>
            <div class="a860923102837"><?php echo $customerDetails['IC_no'];?></div>
            <div class="a01272727377"><?php echo $customerDetails['customerCountryCode'].$customerDetails['customerMobile'];?></div>
            <div class="emergencycontact">Emergency contact</div>
            <div class="dickyzhang"><?php echo $customerDetails['customerAlternatecontactName'];?></div>
            <div class="emergencycontactnu">Emergency contact Number</div>
            <div class="a0124848996"><?php echo $customerDetails['customerAlternateMobile'];?></div>
            <div class="email">Email</div>
            <div class="nickgmailcom"><?php echo $customerDetails['customerEmail'];?></div>
            <a id="resetpassword-link" href="javascript:void(0)">
                <div class="resetpassword">Reset Password</div>
            </a>
            <div class="group7" id="resetpassword-form">
             <?php echo form_open();?>
                <div class="group6">
                    <img class="rectanglecopy3" src="<?php echo base_url('lib/images');?>/reset-password-rectangle-copy-32x.png">
                    <a href="javascript:void(0)" id="resetpassword-btn" class="savesubmit">Save &amp; Submit</a>
                </div>
                <div class="newpassword">New Password</div>
                <div class="oldpassword">Old Password</div>
                <div class="confirmnewpassword">Confirm New Password</div>
                <div class="rectangle1 resetpassword-field"><input id="newpassword" type="text" name="newpassword"><span class="form-text text-danger" id="newpassword_error"></span></div>
                <div class="rectanglecopy2 resetpassword-field"><input id="oldpassword" type="text" name="oldpassword"><span class="form-text text-danger" id="oldpassword_error"></span></div>
                <div class="rectanglecopy resetpassword-field"><input id="cpassword" type="text" name="cpassword"><span class="form-text text-danger" id="cpassword_error"></span></div>
                <?php echo form_close();?>
            </div>
            <a href="<?php echo base_url('customer/editprofile');?>">
                <div class="updateprofile">Update Profile</div>
            </a>
            <img class="oval" src="<?php echo base_url('lib/images');?>/reset-password-oval2x.png">
            <div class="namecopy7">Name</div>
            <div class="namecopy8">Name</div>
            <div class="namecopy9">Name</div>
            <img class="shape" src="<?php echo base_url('lib/images');?>/profile-shape%252032x.png">
        </div>
    </div>
    <script src="http://localhost/lbraco/lib/admin/assets/plugins/common/common.min.js"></script>
    <script src="http://localhost/lbraco/lib/admin/assets/plugins/jqueryui/js/jquery-ui.min.js"></script>
    <script src="http://localhost/lbraco/lib/admin/assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
    <script src="http://localhost/lbraco/lib/admin/js/plugins-init/datatables.init.js"></script>
    <script src="<?php echo base_url('lib/admin/js/custom.js');?>" type="text/javascript"></script>
    </body>
</html>