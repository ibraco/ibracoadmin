        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                
                <div class="row">
                    <div class="msg" style="width: 100%"></div>
                    <div class="col-xl-6 col-xxl-12 col-sm-12">
                        <div class="card">
                                <?php echo $this->session->flashdata('msg');?>
                         <div class="card-body">
                            <div class="row mt-5">
                                <?php
                                $attribute = array('class'=>'');
                                if($this->uri->segment(3))
                                $url = base_url('customer/updateHandover/'.$this->uri->segment(3));
                                else
                                $url = base_url('customer/submitHandover/');
                                
                                echo form_open_multipart($url);?>
                <div class="col-md-8 set-sm-fit mb-4">
                    
                        <div class="preference-title">
                            <h4>Handover Preferences</h4>
                        </div>
                        <!-- preferences Wrap -->
                        <div class="preferences">
                            <!-- Styled radio btn 1 -->
                            <div class="preference-radio">
                                <p>Choose a date?</p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-4 sm-box">
                                        <div class="input-group date insertInfo bitmap " data-provide="datepicker">
                                        <input type="text" class="form-control" name="appointmentDate" id="appoinmentDate" value="<?php echo isset($appointmentDetails['appointmentDate'])?date('d/m/Y',strtotime($appointmentDetails['appointmentDate'])):''?>">
                                        <div class="input-group-addon close-button">
                                          <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                      </div>
                                        <p id="appoinmentDateError" class="form-text text-danger"></p>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <!--// Styled radio btn 1 -->
                            <!-- Styled radio btn 2 -->
                            <div class="preference-radio mt-4">
                                <p>Available Time</p>
                                <div class="row">
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group radio-group a800am900am1" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='8:30 AM - 10:30 AM')?'active':''?>">
                                              <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='8:30 AM - 10:30 AM')?'checked':''?>  autocomplete="off" value="8:30 AM - 10:30 AM" > 8:30 AM - 10:30 AM
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                           <div class="input-group">
                                           <div class="btn-group radio-group a800am900am1" >
                                             <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='1:30 PM - 3:30 PM')?'active':''?>">
                                               <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='1:30 PM - 3:30 PM')?'checked':''?>  autocomplete="off" value="1:30 PM - 3:30 PM" > 1:30 PM - 3:30 PM
                                             </label>
                                           </div>
                                           </div>
                                           </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            
                            
                            <div class="preference-radio mt-4">
                                <!-- Comment box -->
                                <?php if((isset($appointmentDetails['contractorId']))&&$appointmentDetails['contractorId']){?>
                                <p>Contractor PIC</p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        
                                                <?php if($emailList){
                                                        foreach($emailList as $email){?>
                                                <label><?php echo (isset($appointmentDetails['contractorId']))&&$appointmentDetails['contractorId']==$email['departmentEmailId']?$email['contactName']:''?></label>
                                                <?php }}?>
                                        
                                        <p id="notesError" class="form-text text-danger"></p>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            <!--// Styled Check box -->
                            <div class="preference-radio mt-4">
                                <!-- Comment box -->
                                <?php if((isset($appointmentDetails['appointmentNotes']))&&$appointmentDetails['appointmentNotes']!=''){?>
                                <p>Notes</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label id="notes" class="optinal-textarea" ><?php echo (isset($appointmentDetails['appointmentNotes']))?$appointmentDetails['appointmentNotes']:''?></label>
                                        <p id="notesError" class="form-text text-danger"></p>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            
                            
                            <div class="preference-radio mt-4">
                                <!-- Comment box -->
                                <p>Select 1 action (Update status)</p>
                                <?php  if($appointmentDetails['appointmentStatus']=='open'){?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group  a800am900am1 appointmentStatus" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='cancelled')?'cancelled active':''?>" data-id="cancelled">
                                              <input <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='cancelled')?'checked':''?> type="radio" name="appointmentStatus"  autocomplete="off" value="cancelled" > Cancel Appointment
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                        
                                        <p id="notesError" class="form-text text-danger"></p>
                                    </div>
                                </div>
                                <?php } else if($appointmentDetails['appointmentStatus']=='resolved'||$appointmentDetails['appointmentStatus']=='completed') {?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-4 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group  a800am900am1 appointmentStatus" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='noshow')?'closed active':''?>" data-id="closed" >
                                              <input type="radio" <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='closed')?'checked':''?> name="appointmentStatus"  autocomplete="off" value="closed" > Close case
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                        <div class="col-md-4 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group  a800am900am1 appointmentStatus" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='jointinspection')?'jointinspection active':''?>" data-id="jointinspection" >
                                              <input type="radio" <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='jointinspection')?'checked':''?> name="appointmentStatus"  autocomplete="off" value="jointinspection" > Joint Inspection
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                        <p id="notesError" class="form-text text-danger"></p>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            
                            <!--// Personal Details -->
                        </div>
                        <!--// preferences Wrap -->
                    
                </div>

                <!-- Booking Summary -->
                <div class="col-md-4 set-sm-fit">
                    <div style="height: auto;"></div><div data-toggle="affix" class="">
                        <!-- data-toggle="affix" -->
                        <div class="preference-title">
                            <h4>Booking Summary</h4>
                        </div>
                        <div class="fesilities">
                            <ul>
                                <li><i class="fa fa-paint-brush" aria-hidden="true"></i>
                                    <p>Handover</p>
                                </li>
                                <li id="handOver-date"><i class="fa fa-calendar" aria-hidden="true"></i>
                                    <p><?php echo isset($appointmentDetails['appointmentDate'])?date('D d M Y',strtotime(str_replace('/','-',$appointmentDetails['appointmentDate']))):'-'?></p>
                                </li>
                                <li id="handOver-time"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p><?php echo isset($appointmentDetails['appointmentTime'])?$appointmentDetails['appointmentTime']:'-'?></p>
                                </li>
                                <li><i class="fa fa-refresh" aria-hidden="true"></i>
                                    <p>2 hours</p>
                                </li>
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <p><?php echo $appointmentDetails['customerDetails']['unitNumber'];?></p>
                                </li>
                            </ul>
                            <input type="hidden" id="appointmentId" value="<?php echo isset($appointmentDetails['appointmentId'])?$appointmentDetails['appointmentId']:'';?>">
                            <button type="submit" id="" href="javascript:void(0)" class=" button  submit-handover1" data-toggle="modal" data-value="<?php echo $this->security->get_csrf_hash();?>"> Update</button>
                        </div>
                    </div>

                </div>
                <!--// Booking Summary -->
                
                
            <?php echo form_close();?>
                            </div>
                        </div>
                         </div>
                    </div>
                    
                    
                    
                </div>
                
                
                
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        