<!DOCTYPE html>
<!-- To get the source code upgrade to a Pro plan and Export code https://launchpad.animaapp.com/pricing - Sample Exported Code: https://medium.com/sketch-app-sources/how-to-export-sketch-to-html-65c427b3aa3a -->
<html><head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="UTF-8">
<!--<meta name="viewport" content="width=1024, maximum-scale=1.0">-->

<link href="<?php echo base_url('lib/admin/css/style.css');?>" rel="stylesheet">
<link href="<?php echo base_url('lib/css/profile.css');?>" rel="stylesheet">
</head>
<body style="margin: 0;
 background: rgba(255, 255, 255, 1.0);">
    <div class="profile">
        <div style="width: 1024px; height: 100%; position:relative; margin:auto;">
            <div class="group4">
                <div class="rectangle1"></div>
            <a href="<?php echo base_url('customer/dashboard');?>">
                <img class="bitmap" src="<?php echo base_url('lib/images');?>/edit-profile-bitmap2x.png" ></a>
            <a href="<?php echo base_url('customer/profile');?>">
                <div class="group2"><div class="label">Profile</div>
                <div class="icon">
                <img class="shape2" src="<?php echo base_url('lib/images');?>/defect-shape2x.png">
                <img class="shape1" src="<?php echo base_url('lib/images');?>/defect-copy-2-shape%252012x.png">
                </div>
                </div>
            </a>
            <div class="group">
                <a href="<?php echo base_url('customer/logout');?>">
                    <div class="group3">
                        <div class="logout">Logout</div>
                    </div>
                </a>
                <img class="shape1" src="<?php echo base_url('lib/images');?>/defect-copy-shape%252022x.png" >
            </div>
            </div>
            <img class="realestateagenthandingthehousekeytopeht6ze" src="<?php echo base_url('lib/images');?>/reset-password-real-estate-agent-handing-the-house-key-to-pe.png">
            <img class="rectangle" src="<?php echo base_url('lib/images');?>/reset-password-rectangle.png">
            <div class="bg"></div>
            <?php //echo validation_errors('<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><b>Error! </b>', '</div>');
							echo $this->session->flashdata('msg');?>
            <div class="name">Name</div>
            <div class="nickleong"><?php echo $customerDetails['customerName'];?></div>
            <div class="unitblocknumber">Unit/Block number</div>
            <div class="icnumber">I/C number</div>
            <div class="primarycontactnumb">Primary contact number</div>
            <div class="b1010"><?php echo $customerDetails['unitNumber'];?> </div>
            <div class="a860923102837"><?php echo $customerDetails['IC_no'];?></div>
            <?php echo form_open()?>
            <div class="a01272727377">
                <input class="profile-field" name="phoneNumber" type="text" value="<?php echo $customerDetails['customerCountryCode'].$customerDetails['customerMobile'];?>">
            <span class="form-text text-danger"><?php echo form_error('phoneNumber');?></span></div>
            <div class="emergencycontact">Emergency contact</div>
            <div class="dickyzhang"><input class="profile-field" name="customerAlternatecontactName" type="text" value="<?php echo $customerDetails['customerAlternatecontactName'];?>">
            <span class="form-text text-danger"><?php echo form_error('customerAlternatecontactName');?></span></div>
            <div class="emergencycontactnu">Emergency contact Number</div>
            <div class="a0124848996"><input class="profile-field" name="customerAlternateMobile" type="text" value="<?php echo $customerDetails['customerAlternateMobile'];?>" >
            <span class="form-text text-danger"><?php echo form_error('customerAlternateMobile');?></span></div>
            <div class="email">Email</div>
            <div class="nickgmailcom"><input class="profile-field" name="customerEmail" type="text" value="<?php echo $customerDetails['customerEmail'];?>">
            <span class="form-text text-danger"><?php echo form_error('customerEmail');?></span></div>
            <div >
                <div class="resetpassword btn-saveProfile">
                    <div class="btn-saveProfile-text">
                        <input type="submit" value="Save">
                    </div>
            </div>
            </div>
            <?php echo form_close();?>
           
            <a href="<?php echo base_url('customer/editprofile');?>">
                <div class="updateprofile saveProfile"> <div class="cancel">Cancel</div></div>
            </a>
            
        </div>
    </div>
    </body>
</html>