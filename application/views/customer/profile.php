

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                
                <div class="row">
                   <?php echo $this->session->flashdata('msg');?>
                    <div class="col-xl-6 col-xxl-12 col-sm-12">
                        <div class="card">
                            <img class="bg-image" src="<?php echo base_url('lib/images/');?>reset-password-real-estate-agent-handing-the-house-key-to-pe.png">
                            <img class="rectangle bg-overlay" src="<?php echo base_url('lib/images/');?>reset-password-rectangle.png">
                            <img class="profile-oval" src="<?php echo base_url('lib/images/');?>reset-password-oval2x.png">
                            <img class="profile-shape" src="<?php echo base_url('lib/images/');?>profile-shape32x.png">
                            
                        </div>
                    </div>
                    
                    
                    
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-xxl-12 col-xl-6">
                        <div class="card">
                            <div class="card-body">
                                <?php echo form_open('customer/editprofile')?>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-label">Name</label>
                                    <div class="col-sm-4">
                                        <label><?php echo $customerDetails['customerName'];?></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-label">Unit/Block number</label>
                                    <div class="col-sm-4">
                                        <label><?php echo $customerDetails['unitNumber'];?></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-label">I/C Number</label>
                                    <div class="col-sm-4">
                                        <label><?php echo $customerDetails['IC_no'];?></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-label">Primary contact number</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="phoneNumber" value="<?php echo $customerDetails['customerCountryCode'].$customerDetails['customerMobile'];?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-label">Emergency contact</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="customerAlternatecontactName" value="<?php echo $customerDetails['customerAlternatecontactName'];?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-label">Emergency contact Number</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="customerAlternateMobile" class="form-control" value="<?php echo $customerDetails['customerAlternateMobile'];?>"></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label text-label">Email</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="customerEmail" class="form-control" value="<?php echo $customerDetails['customerEmail'];?>">
                                    </div>
                                </div>
                                
                                <div class="form-group div-btn">
                                                <button id="submitCustomer" data-value="" type="submit" class="btn btn-success btn-ft sett-btn">Submit</button>
                                            </div>
                                
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
                
                
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        