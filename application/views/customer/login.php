<!DOCTYPE html>
<html lang="en">
<head>
	<title>Ibraco|Tenant Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url('lib/admin/assets/images/favicon.ico');?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/admin/login/bootstrap/css/bootstrap.min.css');?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/admin/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css');?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/admin/login/fonts/iconic/css/material-design-iconic-font.min.css');?>">
<!--===============================================================================================-->

	
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/admin/login/css/util.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('lib/admin/login/css/main.css');?>">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100 background" >
			<div class="wrap-login100 p-l-55 p-r-55 p-t-20 p-b-20">
				<?php $attribute = array('id'=>'form','class'=>' login100-form validate-form');
							echo form_open('customer/login',$attribute)?>
							<?php echo validation_errors('<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><b>Error! </b>', '</div>');
							echo $this->session->flashdata('msg');?>
					<span class="login100-form-title p-b-49">
						<img src="<?php echo base_url()?>lib/admin/login/images/TT3 Plaza-01.png" style="max-width:200px;height: auto;">
					</span>

					<div class="wrap-input100 validate-input m-b-23" data-validate = "Unit number is required">
						<span class="label-input100">Unit Number</span>
						<input class="input100" type="text" name="unitNumber" placeholder="Type your unit Number">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="password" placeholder="Type your password">
						<span class="focus-input100" data-symbol="&#xf190;"></span>
					</div>
					
					<div class="text-right p-t-8 p-b-31">
						<a href="<?php echo base_url('customer/forgotpassword');?>">
							Forgot password?
						</a>
					</div>
					
					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Login
							</button>
						</div>
					</div>

					
				<?php echo form_close();?>
			</div>
			<div class="col-md-12" style="text-align: center;"><img src="<?php echo base_url('lib/admin/login/images/logo.png');?>" style="width: 100px;height: auto;"></div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url('lib/admin/login/jquery/jquery-3.2.1.min.js');?>"></script>
<!--===============================================================================================-->
	<!--<script src="vendor/animsition/js/animsition.min.js"></script>-->
<!--===============================================================================================-->
<!--	<script src="vendor/bootstrap/js/popper.js"></script>-->
	<script src="<?php echo base_url('lib/admin/login/bootstrap/js/bootstrap.min.js');?>"></script>
<!--===============================================================================================-->
	<!--<script src="vendor/select2/select2.min.js"></script>-->
<!--===============================================================================================-->
	<!--<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>-->
<!--===============================================================================================-->
	<!--<script src="vendor/countdowntime/countdowntime.js"></script>-->
<!--===============================================================================================-->
	<script src="<?php echo base_url('lib/admin/login/js/main.js');?>"></script>

</body>
</html>