
<div id="load_popup_modal_contant" class="" role="dialog">
                                        <div class="modal-dialog" role="document" style="max-width: 680px">

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <!--<h5 class="modal-title">Upload File</h5>-->
                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <?php $attribute = array('id'=>'form','class'=>'mt-5 mb-5');
                                                    echo form_open_multipart('admin/uploadTenantData',$attribute)?>
                                                    <?php echo validation_errors('<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><b>Error! </b>', '</div>');
                                                    echo $this->session->flashdata('msg');?>
                                                <div id="msg"></div>
                                                <input type="hidden" id="customerId" value="<?php echo isset($customerDetails['customerId'])?$customerDetails['customerId']:'';?>">
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Tenant Name:</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="customerName" placeholder="Tenant Name" value="<?php echo isset($customerDetails['customerName'])?$customerDetails['customerName']:''?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Unit Number:</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            <input type="text" readonly="readonly" class="form-control" id="unitNumber" placeholder="Unit Number" value="<?php echo isset($customerDetails['unitNumber'])?$customerDetails['unitNumber']:''?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">I/C Number:</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            
                                                             <input type="text" class="form-control" id="IC_no" placeholder="IC_no" value="<?php echo isset($customerDetails['IC_no'])?$customerDetails['IC_no']:''?>">    
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Owner Name:</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="ownerName" placeholder="Owner Name" value="<?php echo isset($customerDetails['ownerName'])?$customerDetails['ownerName']:''?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Primary Number:</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            
                                                             <input type="text" class="form-control" id="customerMobile" placeholder="Primary Number" value="<?php echo isset($customerDetails['customerMobile'])?$customerDetails['customerCountryCode'].$customerDetails['customerMobile']:''?>">    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Emergency Contact:</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            
                                                             <input type="text" class="form-control" id="customerAlternatecontactName" placeholder="Emergency Contact" value="<?php echo isset($customerDetails['customerAlternatecontactName'])?$customerDetails['customerAlternatecontactName']:''?>">    
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Emergency Contact Number:</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            
                                                             <input type="text" class="form-control" id="customerAlternateMobile" placeholder="Emergency Contact Number" value="<?php echo isset($customerDetails['customerAlternateMobile'])?$customerDetails['customerAlternateMobile']:''?>">    
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Email:</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="customerEmail" placeholder="Email" value="<?php echo isset($customerDetails['customerEmail'])?$customerDetails['customerEmail']:''?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Authorized Person :</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="authorized_person" placeholder="Authorized Person" value="<?php echo isset($customerDetails['authorized_person'])?$customerDetails['authorized_person']:''?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Authorized person Contact No :</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="authorized_person_contact_no" placeholder="Authorized person Contact No" value="<?php echo isset($customerDetails['authorized_person_contact_no'])?$customerDetails['authorized_person_contact_no']:''?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Vacant Possession Letter Date :</label>
                                                    <div class="col-sm-6">
                                                        
                                                        <div class="input-group vacant_date date  insertInfo bitmap " data-provide="datepicker" data-format="dd/mm/yyyy">
                                        <input type="text" class="form-control" id="vacant_possession_letter_date" placeholder="Vacant Possession Letter Date" value="<?php echo (isset($customerDetails['vacant_possession_letter_date'])&&$customerDetails['vacant_possession_letter_date']!='0000-00-00')?date('d/m/Y',strtotime($customerDetails['vacant_possession_letter_date'])):''?>">
                                        <div class="input-group-addon close-button">
                                          <i class="mdi mdi-calendar" style="font-size: 16px ;margin-left:-6px"></i>
                                        </div>
                                      </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Occupation Permit Date :</label>
                                                    <div class="col-sm-6">
                                                        
                                                        <div class="input-group date insertInfo bitmap " data-provide="datepicker">
                                        <input type="text" class="form-control" id="occupation_permit_date" placeholder="Occupation Permit Date" value="<?php echo (isset($customerDetails['occupation_permit_date'])&&$customerDetails['occupation_permit_date']!='0000-00-00')?date('d/m/Y',strtotime($customerDetails['occupation_permit_date'])):''?>">
                                        <div class="input-group-addon close-button">
                                          <i class="mdi mdi-calendar" style="font-size: 16px ;margin-left:-6px"></i>
                                        </div>
                                      </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Handover Date :</label>
                                                    <div class="col-sm-6">
                                                        
                                        <div class="input-group date insertInfo bitmap " data-provide="datepicker">
                                        <input type="text" class="form-control" id="handover_date" placeholder="Handover Date" value="<?php echo (isset($customerDetails['handover_date'])&&$customerDetails['handover_date']!='0000-00-00')?date('d/m/Y',strtotime($customerDetails['handover_date'])):''?>">
                                        <div class="input-group-addon close-button">
                                          <i class="mdi mdi-calendar" style="font-size: 16px ;margin-left:-6px"></i>
                                        </div>
                                      </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Defect Liability Expiry Date :</label>
                                                    <div class="col-sm-6">
                                                        <!--<div class="input-group date">
                                                            <input type="text" class="form-control date" id="defect_liability_expiry_date" placeholder="Defect Liability Expiry Date" value="<?php echo isset($customerDetails['customerEmail'])?date('d/m/Y',strtotime($customerDetails['customerEmail'])):''?>">
                                                        </div>-->
                                                        <div class="input-group  insertInfo bitmap " >
                                        <input type="text" readonly="readonly" class="form-control " id="defect_liability_expiry_date" placeholder="Defect Liability Expiry Date" value="<?php echo (isset($customerDetails['defect_liability_expiry_date'])&&$customerDetails['defect_liability_expiry_date']!='0000-00-00')?date('d/m/Y',strtotime($customerDetails['defect_liability_expiry_date'])):''?>">
                                        
                                      </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group div-btn">
                                                <button id="submitCustomer" data-value="<?php echo $this->security->get_csrf_hash();?>" type="button" class="btn btn-success btn-ft sett-btn">Submit</button>
                                            </div>
                                                </div>
                                            </div>
                                        </div>
</div>
<style>
    .datepicker{z-index:1151 !important;}
</style>

<script src="<?php echo base_url('lib/admin/js/action.js');?>"></script>
<script>
 $(function() {
$('.vacant_date').datepicker({
   
   format: 'dd/mm/yyyy',
  todayHighlight: true,
  autoclose: true
  }).on('changeDate',function(date){
    //$('#handOver-date').html(date);
    var nxtDate = new Date(date.date);
    console.log(nxtDate.setMonth(nxtDate .getMonth() + 13));
    nxtDate.setDate(nxtDate .getDate() + 15);
    var defect_liability_expiry_date = (nxtDate.getDate()+'/'+nxtDate.getMonth()+'/'+nxtDate.getFullYear());
    $('#defect_liability_expiry_date').val(defect_liability_expiry_date);
    });
$('.close-button').unbind();
$('.date').datepicker({
   
   format: 'dd/mm/yyyy',
  todayHighlight: true,
  autoclose: true
  })

$('.close-button').click(function() {
  if ($('.datepicker').is(":visible")) {
    
    $('.vacant_date').datepicker('hide');
  } else {
    
    $('.vacant_date').datepicker('show');
  }
});
 });
</script>