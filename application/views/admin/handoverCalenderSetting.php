<div id="load_popup_modal_contant" class="" role="dialog">
                                        <div class="modal-dialog" role="document" style="max-width: 680px">

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <!--<h5 class="modal-title">Upload File</h5>-->
                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <?php $attribute = array('id'=>'form','class'=>'mt-5 mb-5');
                                                    echo form_open_multipart('admin/uploadTenantData',$attribute)?>
                                                    <?php echo validation_errors('<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><b>Error! </b>', '</div>');
                                                    echo $this->session->flashdata('msg');?>
                                                <div id="msg"></div>
                                                <div class="row">
                                                <input type="hidden" id="calenderType" value="<?php echo $calenderType;?>">
                                                <input type="hidden" id="calenderSettingId" value="<?php echo $calenderSettingId;?>">
                                                    <div class="form-group">
                                                        <label class="text-label">Services available on :</label>
                                                        <div class="row mb-4 col-md-10">
                                                        <div class="col-sm-6 col-md-4 col-xl">
                                                            <div class="form-radio">
                                                                <input id="radio22" checked="" class="radio-outlined" name="radiodemo2" type="radio" value="weekly_hours">
                                                                <label for="radio22" class="radio-red">Regular weekly hours</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-4 col-xl">
                                                            <div class="form-radio">
                                                                <input id="radio23" <?php echo @$calenderSettings['service']=='specific_date'?'checked':''?> class="radio-outlined" name="radiodemo2" type="radio" value="specific_date">
                                                                <label for="radio23" class="radio-red">Only Specific date</label>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="width: 100%">
                                                        <label class="text-label col-md-3">Manpower :</label>
                                                        <div class="col-sm-6 col-md-2">
                                                        <select id="manpower" name="manpower" class="form-control">
                                                            <option value="1" <?php echo (@$calenderSettings['manpower']==1)?'selected':''?>>1</option>
                                                            <option value="2" <?php echo (@$calenderSettings['manpower']==2)?'selected':''?>>2</option>
                                                            <option value="3" <?php echo (@$calenderSettings['manpower']==3)?'selected':''?>>3</option>
                                                            <option value="4" <?php echo (@$calenderSettings['manpower']==4)?'selected':''?>>4</option>
                                                            <option value="5" <?php echo (@$calenderSettings['manpower']==5)?'selected':''?>>5</option>
                                                            <option value="6" <?php echo (@$calenderSettings['manpower']==6)?'selected':''?>>6</option>
                                                            <option value="7" <?php echo (@$calenderSettings['manpower']==7)?'selected':''?>>7</option>
                                                            <option value="8" <?php echo (@$calenderSettings['manpower']==8)?'selected':''?>>8</option>
                                                            <option value="9" <?php echo (@$calenderSettings['manpower']==9)?'selected':''?>>9</option>
                                                            
                                                        </select>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                
                                                <div class="form-group  align-items-center">
                                            <!--Radio buttons-->
                                            <?php $days = array();
                                            if(isset($calenderSettings['days']))
                                            {
                                              $days = explode(',',$calenderSettings['days']);
                                              
                                            }?>
                                                    <div class="btn-group" data-toggle="buttons">
                                                      <label class="btn btn-primary day-btn <?php echo in_array(1,$days)?'active':''?>">
                                                        <input type="checkbox" <?php echo in_array(1,$days)?'checked':''?> autocomplete="off" value="1" > Mon
                                                      </label>
                                                      <label class="btn btn-primary day-btn <?php echo in_array(2,$days)?'active':''?>">
                                                        <input type="checkbox" <?php echo in_array(2,$days)?'checked':''?> autocomplete="off" value="2"> Tue
                                                      </label>
                                                      <label class="btn btn-primary day-btn <?php echo in_array(3,$days)?'active':''?>">
                                                        <input type="checkbox" <?php echo in_array(3,$days)?'checked':''?> autocomplete="off" value="3"> Wed
                                                      </label>
                                                      <label class="btn btn-primary day-btn <?php echo in_array(4,$days)?'active':''?>">
                                                        <input type="checkbox" <?php echo in_array(4,$days)?'checked':''?> autocomplete="off" value="4"> Thu
                                                      </label>
                                                      <label class="btn btn-primary day-btn <?php echo in_array(5,$days)?'active':''?>">
                                                        <input type="checkbox" <?php echo in_array(5,$days)?'checked':''?> autocomplete="off" value="5"> Fri
                                                      </label>
                                                      <label class="btn btn-primary day-btn <?php echo in_array(6,$days)?'active':''?>">
                                                        <input type="checkbox" <?php echo in_array(6,$days)?'checked':''?> autocomplete="off" value="6"> Sat
                                                      </label>
                                                      <label class="btn btn-primary day-btn <?php echo in_array(0,$days)?'active':''?>">
                                                        <input type="checkbox" <?php echo in_array(0,$days)?'checked':''?> autocomplete="off" value="0"> Sun
                                                      </label>
                                                    </div>


<!--Radio buttons-->
                                        </div>
                                                <div class="form-group">
                                                  <div>
                                                        <label class="text-label">Lunch Break :</label>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6">
                                                            <label class="text-label col-md-3">Start Time:</label>
                                                            <div class="col-md-6">
                                                            <div class="input-group clockpicker" data-placement="bottom" data-align="top" data-autoclose="true">
                                                            <input id="startTime" type="text" class="form-control" value="<?php echo isset($calenderSettings['startTime'])?$calenderSettings['startTime']:''?>">
                                                            <!--<span class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-clock-o"></i>
                                                                </span>
                                                            </span>-->
                                                        </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6">
                                                            <label class="text-label col-md-3">End Time:</label>
                                                            <div class="col-md-6">
                                                            <div class="input-group clockpicker" data-placement="bottom" data-align="top" data-autoclose="true">
                                                            <input id="endTime" type="text" class="form-control" value="<?php echo isset($calenderSettings['endTime'])?$calenderSettings['endTime']:''?>">
                                                            <!--<span class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-clock-o"></i>
                                                                </span>
                                                            </span>-->
                                                        </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                <div class="form-group div-btn col-md-12">
                                                <button id="handoverCalenderSetting" type="button" class="btn btn-success btn-ft sett-btn">Submit</button>
                                            </div>
                                                </div>
                                                <?php echo form_close();?>
                                                </div>
                                                <!--<div class="modal-footer">
                                                    <button type="button" class="btn btn-light btn-form" data-dismiss="modal">Close</button>
                                                    
                                                </div>-->
                                            </div>
                                        </div>
                                    </div>
<link href="<?php echo base_url('lib/admin/assets/plugins/clockpicker/css/bootstrap-clockpicker.min.css')?>" rel="stylesheet">
  <script src="<?php echo base_url('lib/admin/assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js')?>"></script>
    <script src="<?php echo base_url('lib/admin/js/plugins-init/clock-picker-init.js')?>"></script>
    <script>
        var success1 = '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
var success2 = '</div>';
var fail1 = ' <div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
var fail2 = '</div>'; 
        $('#handoverCalenderSetting').on('click',function(){
          
             var calenderSettingId = $('#calenderSettingId').val();
            var calenderType = $('#calenderType').val();
            var service = $('input[type=radio]:checked').val();
            var manpower = $('#manpower').val();
            var val = [];
            $(':checkbox:checked').each(function(i){
              val[i] = $(this).val();
            });
            var startTime = $('#startTime').val();
            var endTime = $('#endTime').val();
            var err=false;
            console.log(val);
            if(val.length==0)
            {
                $('#msg').html(fail1+'Please select day.'+fail2);
            }else if(startTime=='')
            {
               $('#msg').html(fail1+'Please select start time.'+fail2);
            }else if(endTime=='')
            {
               $('#msg').html(fail1+'Please select end time.'+fail2);
            }else
            {
                $.ajax({
                    
                    url:'updateSettings',
                    type:'POST',
                    data:{'service':service,'manpower':manpower,'days':val,'startTime':startTime,'endTime':endTime,'calenderType':calenderType,'calenderSettingId':calenderSettingId,'form-no':'<?php echo $this->security->get_csrf_hash();?>'},
                    success:function(response)
                    {
                        var data = JSON.parse(response);
                        if(data.status == 'success'){
                            var message = success1+data.message+success2;
                            $("#msg").append(message);
                            if(calenderSettingId==''){
                            $('#startTime').val('');
                            $('#endTime').val('');
                            }
                            window.setTimeout(function(){location.reload()},1000)
                        }else{
                        var message = fail1+data.message+fail2;
                        $("#msg").append(message);
                    }
                    }
                    });
            }
            });
    </script>