<div id="load_popup_modal1_show_id" class="modal fade" tabindex="-1"></div>
<div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4 style="color: red">Calendar Event</h4>
                        
                    </div>
                    <!--<div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Calender</a>
                            </li>
                            <li class="breadcrumb-item active">Event</li>
                        </ol>
                    </div>-->
                </div>
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="default-tab">
                                    <ul class="nav nav-tabs mb-4" role="tablist">
                                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#home">Calendar</a>
                                        </li>
                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile">Email</a>
                                        </li>
                                        
                                    </ul>
                                    <div class="tab-content tab-content-default">
                                        <div class="tab-pane fade active show" id="home" role="tabpanel">
                                            
                                            <div id="accordion-two" class="accordion">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1"><i class="fa" aria-hidden="true"></i> Calendar Handover</h5>
                                                </div>
                                                <div id="collapseOne1" class="collapse show" data-parent="#accordion-two" style="">
                                                    <div class="card-body pt-0">
                                                        <?php if(!empty($handoverCalenderSettings)){?>
                                                        
                                                            <div class="form-group row align-items-center">
                                                                <label class="col-sm-3 col-form-label text-label">Services available on</label>
                                                                <div class="col-sm-9">:<?php echo $handoverCalenderSettings['service']?></div>
                                                            </div>
                                                            <div class="form-group row align-items-center">
                                                                <label class="col-sm-3 col-form-label text-label">For Service request</label>
                                                                <div class="col-sm-9">:<?php echo 'Handover'?></div>
                                                            </div>
                                                            <div class="form-group row align-items-center">
                                                                <label class="col-sm-3 col-form-label text-label">Weekly working time</label>
                                                                <div class="col-sm-9">:<?php echo $handoverCalenderSettings['days']?></div>
                                                            </div>
                                                            <div class="form-group row align-items-center">
                                                                <label class="col-sm-3 col-form-label text-label">Lunch Break</label>
                                                                <div class="col-sm-9">:Start Time-<?php echo $handoverCalenderSettings['startTime']?>, End Time-<?php echo $handoverCalenderSettings['endTime']?></div>
                                                            </div>
                                                            <div class="">
                                                                        <a id="" href="javascript:void(0)" class="btn button addAdminSetting " data-toggle="modal" data-info="handover" data-id="<?php echo $handoverCalenderSettings['calenderSettingId']?>" data-value="<?php echo $this->security->get_csrf_hash();?>" > Edit Settings</a>
                                                            </div>
                                                        <?php }else {?>
                                                        
                                                        <a id="" href="javascript:void(0)" class="btn button addAdminSetting" data-toggle="modal" data-id='' data-info="handover" data-value="<?php echo $this->security->get_csrf_hash();?>" >+ Add Calendar Setting</a>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5 class="mb-0" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2"><i class="fa" aria-hidden="true"></i> Calendar Defect</h5>
                                                </div>
                                                <div id="collapseTwo2" class="collapse " data-parent="#accordion-two" style="">
                                                    <div class="card-body pt-0">
                                                    <?php if(!empty($defectCalenderSettings)){?>
                                                    
                                                            <div class="form-group row align-items-center">
                                                                <label class="col-sm-3 col-form-label text-label">Services available on</label>
                                                                <div class="col-sm-9">:<?php echo $defectCalenderSettings['service']?></div>
                                                            </div>
                                                            <div class="form-group row align-items-center">
                                                                <label class="col-sm-3 col-form-label text-label">For Service request</label>
                                                                <div class="col-sm-9">:<?php echo 'Defect'?></div>
                                                            </div>
                                                            <div class="form-group row align-items-center">
                                                                <label class="col-sm-3 col-form-label text-label">Weekly working time</label>
                                                                <div class="col-sm-9">:<?php echo $defectCalenderSettings['days']?></div>
                                                            </div>
                                                            <div class="form-group row align-items-center">
                                                                <label class="col-sm-3 col-form-label text-label">Lunch Break</label>
                                                                <div class="col-sm-9">:Start Time-<?php echo $defectCalenderSettings['startTime']?>, End Time-<?php echo $defectCalenderSettings['endTime']?></div>
                                                            </div>
                                                            <div class="">
                                                                        <a id="" href="javascript:void(0)" class="btn button addAdminSetting" data-toggle="modal" data-id="<?php echo $defectCalenderSettings['calenderSettingId']?>" data-info="defect" data-value="<?php echo $this->security->get_csrf_hash();?>" > Edit Settings</a>
                                                            </div>
                                                        <?php }else {?>
                                                        
                                                        <a id="" href="javascript:void(0)" class="btn button addAdminSetting" data-toggle="modal" data-id='' data-info="defect" data-value="<?php echo $this->security->get_csrf_hash();?>" >+ Add Calendar Setting</a>
                                                        <?php }?></div>
                                                </div>
                                            </div>
                                    
                                </div>
                                        </div>
                                        <div class="tab-pane fade" id="profile">
                                            <div>
                                                <a id="" href="javascript:void(0)" class="btn button addEmail" data-toggle="modal" data-id='' data-value="<?php echo $this->security->get_csrf_hash();?>" >+ Add Email</a>
                                                <div class="">
                                    <table id="emailTable" class="table table-bordered verticle-middle" >
                                        <thead>
                                            <tr>
                                                <th scope="col">Contact Name</th>
                                                <th scope="col">Categories</th>
                                                <th scope="col">Email</th>
                                                
                                                <th scope="col">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
