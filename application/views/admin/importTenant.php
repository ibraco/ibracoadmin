<div id="load_popup_modal_contant" class="" role="dialog">
                                        <div class="modal-dialog" role="document">

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Upload File</h5>
                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <?php $attribute = array('id'=>'form','class'=>'mt-5 mb-5');
                                                    echo form_open_multipart('admin/uploadTenantData',$attribute)?>
                                                    <?php echo validation_errors('<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><b>Error! </b>', '</div>');
                                                    echo $this->session->flashdata('msg');?>
                                                <div class="msg"></div>
                                                <div class="form-group row align-items-center">
                                            <label class="col-sm-3 col-form-label text-label">Upload File</label>
                                            <div class="col-sm-9">
                                                <div class="input-group">
                                                    <input type="file" class="form-control" id="pic" accept=".xlsx, .xls, .csv" >
                                                </div>
                                            </div>
                                        </div>
                                                <div class="form-group">
                                        <button id="uploadTenantData" type="button" class="btn button">Submit</button>
                                        <button id="waitTenantData" style="display: none" type="button" class="btn button">Please Wait....</button>
                                    </div>
                                                <?php echo form_close();?>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-light btn-form" data-dismiss="modal">Close</button>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

<script>
    var success1 = '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
var success2 = '</div>';
var fail1 = ' <div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
var fail2 = '</div>';
    $(function () {
    $('#uploadTenantData').on('click',function(){
        var file_data = $('#pic').prop('files')[0];
        var form_data = new FormData();  // Create a FormData object
        form_data.append('file', file_data);  // Append all element in FormData  object
        form_data.append('form-no',"<?php echo $this->security->get_csrf_hash();?>")
        $('#uploadTenantData').css('display','none');
        $('#waitTenantData').css('display','block');
        $.ajax({
                url         : 'uploadTenantData',     // point to server-side PHP script 
                dataType    : 'text',           // what to expect back from the PHP script, if anything
                cache       : false,
                contentType : false,
                processData : false,
                data        : form_data,                         
                type        : 'post',
                success     : function(output){
                            // display response from the PHP script, if any
                            
                            var data = JSON.parse(output);
                            console.log(data);
                            if(data.status=='success')
                            {
                                $('.msg').html(success1+data.message+success2);
                            }else if(data.status=='error')
                            {
                                $('.msg').html(fail1+data.message+fail2);
                            }
                                    $('#uploadTenantData').css('display','block');
        $('#waitTenantData').css('display','none');
        var uTable = $('#example-ajax').dataTable();
      uTable.fnStandingRedraw();
                }
                
                
         });

         $('#pic').val(''); 
   
    
    });
    /*
@function: Called to reload the table on active/inactive the record. 

*/
$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
    //redraw to account for filtering and sorting
    // concept here is that (for client side) there is a row got inserted at the end (for an add)
    // or when a record was modified it could be in the middle of the table
    // that is probably not supposed to be there - due to filtering / sorting
    // so we need to re process filtering and sorting
    // BUT - if it is server side - then this should be handled by the server - so skip this step
    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        //iDisplayStart has been reset to zero - so lets change it back
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }
      
    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};
    });
</script>