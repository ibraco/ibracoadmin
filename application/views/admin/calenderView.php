<!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4 style="color: red">Hello, <span>welcome</span></h4>
                    </div>
                    <!--<div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Calender</a>
                            </li>
                            <li class="breadcrumb-item active">Event</li>
                        </ol>
                    </div>-->
                </div>
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                    <!-- BEGIN MODAL -->
                    <div class="modal fade none-border" id="event-modal">
                        <div class="modal-dialog" style="max-width: 799px;">
                            <div class="modal-content">
                                <div class="modal-header">
                                        <h5 class="modal-title">&nbsp;</h5>
                                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                    <div class="calendar group4 button-appointment">
                              <div class=" letsscheduleanapp">Schedule an appointment for:</div>
                              <a id="handover" href="handover">
                                    <div class="group">
                                          <img class="rectangle1" src="<?php echo base_url('lib/images/home-rectangle2@2x.png')?>">
                                          <div class="handover">Handover</div>
                                    </div>
                              </a>
                              <a id="defect" href="javascript:void(0)">
                                    <div class="groupcopy">
                                          <img class="rectangle1" src="<?php echo base_url('lib/images/home-rectangle2@2x.png');?>">
                                          <div class="defect">Defect</div>
                                    </div>
                              </a>
                              
                            </div>
                                </div>
                                <div class="modal-body"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect"
                                        data-dismiss="modal">Close</button>
                                    <!--<button type="button" class="btn btn-success save-event waves-effect waves-light">Create
                                        event</button>-->
                                        
                                   <!-- <button type="button" class="btn btn-danger delete-event waves-effect waves-light"
                                        data-dismiss="modal">Delete</button>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Add Category -->
                    <div class="modal fade none-border" id="add-category">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title"><strong>Add a category</strong></h4>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="control-label">Category Name</label>
                                                <input class="form-control form-white" placeholder="Enter name"
                                                    type="text" name="category-name">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Choose Category Color</label>
                                                <select class="form-control form-white"
                                                    data-placeholder="Choose a color..." name="category-color">
                                                    <option value="success">Success</option>
                                                    <option value="danger">Danger</option>
                                                    <option value="info">Info</option>
                                                    <option value="pink">Pink</option>
                                                    <option value="primary">Primary</option>
                                                    <option value="warning">Warning</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default waves-effect"
                                        data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-danger waves-effect waves-light save-category"
                                        data-dismiss="modal">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        <div class="footer color-div">
                <div class="col-md-12">
                        <ul class="color-tone">
                                <li class="tone-li"><div class="open-tone"></div><span>Open</span></li>
                                <li class="tone-li"><div class="cancelled-tone"></div> <span>   Cancelled</span></li>
                                <li class="tone-li"><div class="pending-tone"></div><span>   Pending</span></li>
                                <li class="tone-li"><div class="rejected-tone"></div><span>   Rejected</span></li>
                                <li class="tone-li"><div class="resolved-tone"></div><span>   Resolved</span></li>
                                <li class="tone-li"><div class="closed-tone"></div><span>   Closed</span></li>
                                <li class="tone-li"><div class="noshow-tone"></div><span>   No show</span></li>
                                
                        </ul>
                </div>
        </div>
        <?php
        foreach($publicHolidays as $val)
        {
                $holidays[]= $val['holidayDate'];
        }
        ;?>
        <script>
                var days=<?php echo isset($days)?$days:[];?>;
                var publicHolidays = <?php echo json_encode($holidays);?>;
        </script>
        