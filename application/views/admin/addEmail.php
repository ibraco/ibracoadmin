<div id="load_popup_modal_contant" class="" role="dialog">
                                        <div class="modal-dialog" role="document" style="max-width: 680px">

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <!--<h5 class="modal-title">Upload File</h5>-->
                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <?php $attribute = array('id'=>'form','class'=>'mt-5 mb-5');
                                                    echo form_open_multipart('admin/uploadTenantData',$attribute)?>
                                                    <?php echo validation_errors('<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><b>Error! </b>', '</div>');
                                                    echo $this->session->flashdata('msg');?>
                                                <div id="msg"></div>
                                                <input type="hidden" id="departmentEmailId" value="<?php echo isset($emailDetails['departmentEmailId'])?$emailDetails['departmentEmailId']:'';?>">
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Contact Name:</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="contactName" placeholder="Contact Name" value="<?php echo isset($emailDetails['contactName'])?$emailDetails['contactName']:''?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Categories:</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            <select type="text" class="form-control" id="categories" placeholder="">
                                                                <option value="Cleaning company" <?php echo (@$emailDetails['categories']=='Cleaning company')?>>Cleaning company</option>
                                                                <option value="Contractor" <?php echo (@$emailDetails['categories']=='Contractor')?>>Contractor</option>
                                                                <option value="Main Contractor" <?php echo (@$emailDetails['categories']=='Main Contractor')?>>Main Contractor</option>
                                                                <option value="Site Contractor" <?php echo (@$emailDetails['categories']=='Site Contractor')?>>Site Contractor</option>
                                                                <option value="Customer service PIC" <?php echo (@$emailDetails['categories']=='Customer service PIC')?>>Customer service PIC</option>
                                                                
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row align-items-center">
                                                    <label class="col-sm-3 col-form-label text-label">Email:</label>
                                                    <div class="col-sm-6">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="email" placeholder="Email" value="<?php echo isset($emailDetails['email'])?$emailDetails['email']:''?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group div-btn">
                                                <button id="submitEmail" data-value="<?php echo $this->security->get_csrf_hash();?>" type="button" class="btn btn-success btn-ft sett-btn">Submit</button>
                                            </div>
                                                </div>
                                            </div>
                                        </div>
</div>

<script src="<?php echo base_url('lib/admin/js/action.js');?>"></script>