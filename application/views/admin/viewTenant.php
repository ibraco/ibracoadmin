<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<div id="load_popup_modal1_show_id" class="modal fade" tabindex="-1"></div>
<div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4 style="color: red">Tenant Profile</h4>
                        
                    </div>
                    <!--<div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Calender</a>
                            </li>
                            <li class="breadcrumb-item active">Event</li>
                        </ol>
                    </div>-->
                </div>
                <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12">
                                    <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Name</label>
                                        <div class="col-sm-6"><?php echo $customerDetails['customerName'];?></div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Unit Number</label>
                                        <div class="col-sm-6"><?php echo $customerDetails['unitNumber'];?></div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">I/C Number</label>
                                        <div class="col-sm-6"><?php echo $customerDetails['IC_no'];?></div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Primary Contact Number</label>
                                        <div class="col-sm-6"><?php echo $customerDetails['customerMobile'];?></div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Authorized Person</label>
                                        <div class="col-sm-6"><?php echo $customerDetails['authorized_person'];?></div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Authorized Person Contact No</label>
                                        <div class="col-sm-6"><?php echo $customerDetails['authorized_person_contact_no'];?></div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Vacant Possession Date</label>
                                        <div class="col-sm-6"><?php echo date('d F Y',strtotime($customerDetails['vacant_possession_letter_date']));?></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Emergancy Contact</label>
                                        <div class="col-sm-6"><?php echo $customerDetails['customerAlternatecontactName'];?></div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Emergancy Contact Number</label>
                                        <div class="col-sm-6"><?php echo $customerDetails['customerAlternateMobile'];?></div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Email</label>
                                        <div class="col-sm-6"><?php echo $customerDetails['customerEmail'];?></div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Occupation Permit Date</label>
                                        <div class="col-sm-6"><?php echo date('d F Y',strtotime($customerDetails['occupation_permit_date']));?></div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Handover Date</label>
                                        <div class="col-sm-6"><?php echo date('d F Y',strtotime($customerDetails['handover_date']));?></div>
                                    </div>
                                    <div class="form-group row align-items-center">
                                        <label class="col-sm-6 col-form-label text-label">Defect Liability Expiry Date</label>
                                        <div class="col-sm-6"><?php echo date('d F Y',strtotime($customerDetails['defect_liability_expiry_date']));?></div>
                                    </div>
                                </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dashboard bg1"></div>
                            <div class="tenant-profile myappointment"><span><i class="mdi mdi-clock "></i></span>Summary Ticket listing</div>
                            <div class="card-body">
                                <div class="row">
                    <div class="col-xl-8 view-tenant" >
                        <div id="accordion-three" class="accordion">
                                <?php if(!empty($appointmentList)){
                                    
                                    //_print_r($appointmentList);
                                        foreach($appointmentList as $appo){
                                                $appointmentId = $appo['appointmentId'];?>
                              <div>
                                                <div class="card notification <?php echo $appo['appointmentStatus'];?>-stage">
                                 
                                                <div class="box">
                                          <div class="box-header with-border">
                                            <h3 class="box-title noti-title"><?php echo ($appo['appointmentType']=='handover')?'Handover':'Defect'.($appo['defectCategory']?'. '.$appo['defectCategory']:'').($appo['defectLevel']?'. '.$appo['defectLevel']:'').($appo['defectIssue']?'. '.$appo['defectIssue']:'');?></h3>
                                            <div class="noti-num">#<?php echo $appo['ticketNo'];?></div>
                                                <div class="noti-time ">
                                                      <span><i class="mdi mdi-clock " style=""></i></span><?php echo date('j M Y',strtotime($appo['appointmentDate'])).' '.$appo['appointmentTime'];?>
                                                </div>
                                                
                                                
                                               
                                               
                                               <div class="box-tools pull-right">
                                              <?php if($appo['appointmentType']=='handover'){?>
                                              <a href="<?php echo base_url('admin/editAppointment/'.$appointmentId);?>" class="edit-icon"><i class="mdi mdi-pencil-circle noti-icon"></i></a>
                                              <?php }elseif($appo['appointmentType']=='defect') {?>
                                              <a href="<?php echo base_url('admin/editDefectAppointment/'.$appointmentId);?>" class="edit-icon"><i class="mdi mdi-pencil-circle noti-icon"></i></a>
                                              <?php }?>
                                              <button type="button" class="btn btn-box-tool collapsed"  data-toggle="collapse" data-target="#collapseTwo<?php echo $appo['appointmentId'];?>" aria-expanded="false" aria-controls="collapseTwo5">
                                                <i class="fa fa-minus"></i></button>
                                              <!--<a href="<?php echo base_url('customer/deleteAppointment/'.$appointmentId);?>" class="edit-icon removeAppointment" onclick="return false;" data-id="" data-value="<?php echo $this->security->get_csrf_hash();?>"><i class="mdi mdi-close-circle noti-icon"></i></a>-->
                                              
                                            </div>
                                              
                                            <div class="noti-notes"><?php echo $appo['appointmentNotes']?$appo['appointmentNotes']:'&nbsp;';?></div>
                                            <div><i class="mdi mdi-account user-account"><?php echo ($appo['contractorName'])?$appo['contractorName']:'Unassigned';?></i></div>
                                            <div class="pull-right noti-dates"><?php echo date('d M Y',strtotime($appo['addedOn']));?></div>
                                          </div>
                                          
                                    </div>
                                    
                              </div>
                              <div id="collapseTwo<?php echo $appo['appointmentId'];?>" class="collapse" data-parent="#accordion-three">
                                          <div class="box-body">
                                            &nbsp;
                                          </div>
                                          <!-- /.box-body -->
                                          <div class="box-footer">
                                                <?php if(isset($appo['commentDetails'])&&!empty($appo['commentDetails'])) {?>
                                            <div class="timeline_content">
                            
                                                <ul class="timeline comment-timeline">
                                                    <?php 
                                                            foreach($appo['commentDetails'] as $commentDetails){
                                                                         if(isset($commentDetails['appointmentCommentId'])){
                                                                                    //_print_r($commentDetails);
                                                                                    ?>
                                                    <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget comment-list">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['appointmentComment'];?></p>
                                                                    <p><?php echo $commentDetails['appointmentReason'];?></p>
                                                                    <div id="gallery<?php echo $commentDetails['appointmentCommentId']?>" class="gallery1">
                                                                        
                                                                    </div>
                                                                    <?php
                                                                    $imageArray=array();
                                                                    if(isset($commentDetails['commentImage'])) {
                                                                    foreach($commentDetails['commentImage'] as $image){
                                                                        
                                                                        $imageArray[] = (base_url('uploads/appointment/'.$image['image']));
                                                                        ?>
                                                                        
                                                                    <?php }
                                                                    //_print_r($imageArray);
                                                                    $html ='';
                                                                     if( isset($imageArray)){
                                                                        
                                                                        $i=0;
                                                                        foreach($imageArray as $img){
                                                                         $html .="'".$img."'";
                                                                        if(isset($imageArray[$i+1]))
                                                                         $html .=',';
                                                                        $i++;
                                                                        }
                                                                        }?>
                                                                   
                                                                    <script>
 //var images = [];
 var images = [<?php echo $html;?>];
 $(function() {

                $('#gallery<?php echo $commentDetails['appointmentCommentId']?>').imagesGrid({
                        images:[<?php echo $html;?>]
                        });
                

            });</script>
                                                                    <?php }?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php } elseif(isset($commentDetails['logId'])){ ?>
                                                     <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget comment-list">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['logText'];?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                     <?php } } ?>
                                                    
                                                </ul>
                                        </div>
                                            <?php }
                                            
                                            if(isset($appo['parentAppointment'])&&!empty($appo['parentAppointment'])) {
                                                foreach($appo['parentAppointment'] as $parent){
                                                ?>
                                            
                                            
                                            <div class="card notification timeline_content" id="parent-accordion">
                                 
                                    <div class="box">
                                          <div class="box-header with-border">
                                            <h3 class="box-title noti-title"><?php echo ($parent['appointmentType']=='handover')?'Handover':'Defect'.($parent['defectCategory']?'. '.$parent['defectCategory']:'').($parent['defectLevel']?'. '.$parent['defectLevel']:'').($parent['defectIssue']?'. '.$parent['defectIssue']:'');?></h3>
                                            <div class="noti-num">#<?php echo $parent['ticketNo']?></div>
                                                <div class="noti-time ">
                                                      <span><i class="mdi mdi-clock " style=""></i></span><?php echo $parent['appointmentTime']?>                                               </div>
                                                
                                                
                                                                                           <div class="box-tools pull-right">
                                              <button type="button" class="btn btn-box-tool collapsed" data-toggle="collapse" aria-expanded="false" data-target="#collapseTwo<?php echo $parent['appointmentId'];?>" aria-controls="collapseTwo5">
                                                <i class="fa fa-minus"></i></button>
                                              
                                            </div>
                                        <div class="noti-notes"><?php echo $parent['appointmentNotes'];?></div>
                                          </div>
                                          
                                    </div>
                                    
                              </div>
                                            <div id="collapseTwo<?php echo $parent['appointmentId'];?>" class="collapse" data-parent="#parent-accordion">
                                          <div class="box-body">
                                            &nbsp;
                                          </div>
                                          <!-- /.box-body -->
                                          <div class="box-footer">
                                                <?php if(isset($parent['commentDetails'])&&!empty($parent['commentDetails'])) {?>
                                            <div class="timeline_content">
                            
                                                <ul class="timeline comment-timeline">
                                                    <?php 
                                                            foreach($parent['commentDetails'] as $commentDetails){
                                                                        if(isset($commentDetails['appointmentCommentId'])){?>
                                                    <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget comment-list">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['appointmentComment'];?></p>
                                                                    <p><?php echo $commentDetails['appointmentReason'];?></p>
                                                                    <div id="gallery<?php echo $commentDetails['appointmentCommentId']?>" class="gallery1">
                                                                        
                                                                    </div>
                                                                    <?php
                                                                    $imageArray=array();
                                                                    if(isset($commentDetails['commentImage'])) {
                                                                    foreach($commentDetails['commentImage'] as $image){
                                                                        
                                                                        $imageArray[] = (base_url('uploads/appointment/'.$image['image']));
                                                                        ?>
                                                                        
                                                                    <?php }
                                                                    //_print_r($imageArray);
                                                                    $html ='';
                                                                     if( isset($imageArray)){
                                                                        
                                                                        $i=0;
                                                                        foreach($imageArray as $img){
                                                                         $html .="'".$img."'";
                                                                        if(isset($imageArray[$i+1]))
                                                                         $html .=',';
                                                                        $i++;
                                                                        }
                                                                        }?>
                                                                   
                                                                    <script>
                                                                        //var images = [];
                                                                        var images = [<?php echo $html;?>];
                                                                        $(function() {
                                                                       
                                                                                       $('#gallery<?php echo $commentDetails['appointmentCommentId']?>').imagesGrid({
                                                                                               images:[<?php echo $html;?>]
                                                                                               });
                                                                                       
                                                                       
                                                                                   });</script>
                                                                    <?php }?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php } elseif(isset($commentDetails['logId'])){ ?>
                                                     <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget comment-list">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['logText'];?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                     <?php } } ?>
                                                    
                                                </ul>
                                        </div>
                                            <?php }?>
                                          </div>
                                            </div>
                                            <?php } }?>
                                            
                                          </div>
                                          <!-- /.box-footer-->
                                          </div>
                                        </div>
                              <?php } }?>
                              
                        </div>
                    </div>
                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
 <div class="footer color-div">
                <div class="col-md-12">
                        <ul class="color-tone">
                                <li class="tone-li"><div class="open-tone"></div><span>Open</span></li>
                                <li class="tone-li"><div class="cancelled-tone"></div> <span>   Cancelled</span></li>
                                <li class="tone-li"><div class="pending-tone"></div><span>   Pending</span></li>
                                <li class="tone-li"><div class="rejected-tone"></div><span>   Rejected</span></li>
                                <li class="tone-li"><div class="resolved-tone"></div><span>   Resolved</span></li>
                                <li class="tone-li"><div class="closed-tone"></div><span>   Closed</span></li>
                                <li class="tone-li"><div class="noshow-tone"></div><span>   No show</span></li>
                                
                        </ul>
                </div>
        </div>
 