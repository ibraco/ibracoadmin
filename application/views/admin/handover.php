   <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
   <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                
                <div class="row">
                    <div class="msg" style="width: 100%"></div>
                    <div class="col-xl-12 col-xxl-12 col-sm-12">
                        <div class="card">
                                <?php echo $this->session->flashdata('msg');?>
                         <div class="card-body">
                            <div class="row mt-5">
                                <?php
                                $attribute = array('class'=>'edt-app');
                                if($this->uri->segment(3))
                                $url = base_url('admin/updateHandover/'.$this->uri->segment(3));
                                else
                                $url = base_url('admin/submitHandover/');
                                
                                echo form_open_multipart($url,$attribute);?>
                <div class="col-md-8 set-sm-fit mb-4">
                    
                        <div class="preference-title">
                            <h4><?php if($appointmentDetails['appointmentType']=='handover'){?>Handover<?php } elseif($appointmentDetails['appointmentType']=='defect'){ echo 'Defect';}?> Preferences</h4>
                        </div>
                        <!-- preferences Wrap -->
                        <div class="preferences">
                           <div class="preference-radio">
                              <?php if($appointmentDetails['defectCategory']){?>
                                <strong>Defect: <?php echo $appointmentDetails['defectCategory'].($appointmentDetails['defectLevel']?'. '.$appointmentDetails['defectLevel']:'').($appointmentDetails['defectLocation']?'. '.$appointmentDetails['defectLocation']:'').($appointmentDetails['defectIssue']?'. '.$appointmentDetails['defectIssue']:'');?></strong>
                                <p></p>
                              <?php }?>
                           </div>
                            <!-- Styled radio btn 1 -->
                            <div class="preference-radio">
                                <p>Choose a date?</p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-4 sm-box">
                                        <div class="input-group date insertInfo bitmap " data-provide="datepicker">
                                        <input type="text" class="form-control" name="appointmentDate" id="appoinmentDate" value="<?php echo isset($appointmentDetails['appointmentDate'])?date('d/m/Y',strtotime($appointmentDetails['appointmentDate'])):''?>">
                                        <div class="input-group-addon close-button">
                                          <i class="mdi mdi-calendar" style="font-size: 16px ;margin-left:-6px"></i>
                                        </div>
                                      </div>
                                        <span id="appoinmentDateError" class="form-text text-danger"><?php echo form_error('appointmentDate');?></span>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <!--// Styled radio btn 1 -->
                            <!-- Styled radio btn 2 -->
                            <div class="preference-radio mt-4">
                                <p>Available Time</p>
                                <?php if($appointmentDetails['appointmentType']=='handover'){?>
                                <div class="row">
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group radio-group a800am900am1" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='8:30 AM - 10:30 AM')?'active':''?>">
                                              <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='8:30 AM - 10:30 AM')?'checked':''?>  autocomplete="off" value="8:30 AM - 10:30 AM" > 8:30 AM - 10:30 AM
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                           <div class="input-group">
                                           <div class="btn-group radio-group a800am900am1" >
                                             <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='1:30 PM - 3:30 PM')?'active':''?>">
                                               <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='1:30 PM - 3:30 PM')?'checked':''?> autocomplete="off" value="1:30 PM - 3:30 PM" > 1:30 PM - 3:30 PM
                                             </label>
                                           </div>
                                           </div>
                                           </div>
                                    </div>
                                    
                                    
                                </div>
                                <?php } elseif($appointmentDetails['appointmentType']=='defect'){?>
                                
                                 <div class="row">
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                                <div class="btn-group radio-group a800am900am1" >
                                                   <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='9:00 AM - 10:00 AM')?'active':''?>">
                                                     <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='9:00 AM - 10:00 AM')?'checked':''?>  autocomplete="off" value="9:00 AM - 10:00 AM" > 9:00 AM - 10:00 AM
                                                   </label>
                                                </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                           <div class="input-group">
                                           <div class="btn-group radio-group a800am900am1" >
                                             <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='10:00 AM - 11:00 AM')?'active':''?>">
                                               <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='10:00 AM - 11:00 AM')?'checked':''?>  autocomplete="off" value="10:00 AM - 11:00 AM" > 10:00 AM - 11:00 AM
                                             </label>
                                           </div>
                                           </div>
                                           </div>
                                    </div>
                                    
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group radio-group a800am900am1" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='1:15 PM - 2:15 PM')?'active':''?>">
                                              <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='1:15 PM - 2:15 PM')?'checked':''?>  autocomplete="off" value="1:15 PM - 2:15 PM" > 1:15 PM - 2:15 PM
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                           <div class="input-group">
                                           <div class="btn-group radio-group a800am900am1" >
                                             <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='2:15 PM - 3:15 PM')?'active':''?>">
                                               <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='2:15 PM - 3:15 PM')?'checked':''?>  autocomplete="off" value="2:15 PM - 3:15 PM" > 2:15 PM - 3:15 PM
                                             </label>
                                           </div>
                                           </div>
                                           </div>
                                    </div>
                                    
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group radio-group a800am900am1" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='3:15 PM - 4:15 PM')?'active':''?>">
                                              <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='3:15 PM - 4:15 PM')?'checked':''?>  autocomplete="off" value="3:15 PM - 4:15 PM" > 3:15 PM - 4:15 PM
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                                
                                <?php }?>
                                <p id="appoinmentTimeError" class="form-text text-danger"><?php echo form_error('appointmentTime');?></p>
                            </div>
                            
                            
                            <div class="preference-radio mt-4">
                                <!-- Comment box -->
                                <p>Contractor PIC</p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <select class="form-control" name="contractorId">
                                          <!-- <option value="">-</option> -->
                                                <?php if($emailList){
                                                        foreach($emailList as $email){?>
                                                <option value="<?php echo $email['departmentEmailId']?>" <?php echo $appointmentDetails['contractorId']==$email['departmentEmailId']?'selected':''?>><?php echo $email['contactName']?></option>
                                                <?php }}?>
                                        </select>
                                        <p id="notesError" class="form-text text-danger"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--// Styled Check box -->
                            <div class="preference-radio mt-4">
                                <!-- Comment box -->
                                <p>Notes</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label id="notes" class="optinal-textarea" ><?php echo (isset($appointmentDetails['appointmentNotes']))?$appointmentDetails['appointmentNotes']:''?></label>
                                        <p id="notesError" class="form-text text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="preference-radio mt-4">
                                <!-- Comment box -->
                                <p>Select 1 action (Update status)</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group  a800am900am1 appointmentStatus" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='accept')?'accept active':''?>" data-id="accept">
                                              <input <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='accept')?'checked':''?> type="radio" name="appointmentStatus"  autocomplete="off" value="accept" > Accept
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                        <div class="col-md-3 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group  a800am900am1 appointmentStatus" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='rejected')?'reject active':''?>" data-id="reject">
                                              <input type="radio" <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='rejected')?'checked':''?> name="appointmentStatus"  autocomplete="off" value="rejected" > Reject
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                        <p id="notesError" class="form-text text-danger"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group  a800am900am1 appointmentStatus" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='noshow')?'noshow active':''?>" data-id="noshow" >
                                              <input type="radio" <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='noshow')?'checked':''?> name="appointmentStatus"  autocomplete="off" value="noshow" > No Show
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                        <div class="col-md-3 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                          <?php if($appointmentDetails['appointmentType']=='handover'){?>
                                         <div class="btn-group  a800am900am1 appointmentStatus" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='completed')?'resolve active':''?>" data-id="resolve" >
                                              <input type="radio" <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='completed')?'checked':''?> name="appointmentStatus"  autocomplete="off" value="completed" > Complete
                                            </label>
                                         </div>
                                         <?php } elseif($appointmentDetails['appointmentType']=='defect'){?>
                                         
                                         <div class="btn-group  a800am900am1 appointmentStatus" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='resolved')?'resolve active':''?>" data-id="resolve" >
                                              <input type="radio" <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='resolved')?'checked':''?> name="appointmentStatus"  autocomplete="off" value="resolved" > Resolve
                                            </label>
                                         </div>
                                         <?php }?>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                        <p id="notesError" class="form-text text-danger"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-3 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group  a800am900am1 appointmentStatus" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='close')?'checked':''?>" data-id="closed" >
                                              <input type="radio" <?php echo (isset($appointmentDetails['appointmentStatus'])&&$appointmentDetails['appointmentStatus']=='close')?'checked':''?> name="appointmentStatus"  autocomplete="off" value="closed" > Close
                                            </label>
                                         </div>
                                         <p id="appoinmentTimeError" class="form-text text-danger"></p>
                                         </div>
                                         </div>
                                    </div>
                                        
                                        <p id="notesError" class="form-text text-danger"></p>
                                    </div>
                                </div>
                            </div>
                            
                            <!--// Personal Details -->
                        </div>
                        <!--// preferences Wrap -->
                    
                </div>

                <!-- Booking Summary -->
                <div class="col-md-4 set-sm-fit">
                    <div style="height: auto;"></div><div data-toggle="affix" class="">
                        <!-- data-toggle="affix" -->
                        <div class="preference-title">
                            <h4>Booking Summary</h4>
                        </div>
                        <div class="fesilities">
                            <ul>
                                <li><i class="fa fa-paint-brush" aria-hidden="true"></i>
                                    <p><?php if($appointmentDetails['appointmentType']=='handover'){?>Handover<?php } elseif($appointmentDetails['appointmentType']=='defect'){ echo 'Defect';}?></p>
                                </li>
                                <li id="handOver-date"><i class="fa fa-calendar" aria-hidden="true"></i>
                                    <p><?php echo isset($appointmentDetails['appointmentDate'])?date('D d M Y',strtotime(str_replace('/','-',$appointmentDetails['appointmentDate']))):'-'?></p>
                                </li>
                                <li id="handOver-time"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p><?php echo isset($appointmentDetails['appointmentTime'])?$appointmentDetails['appointmentTime']:'-'?></p>
                                </li>
                                <li><i class="fa fa-refresh" aria-hidden="true"></i>
                                    <p><?php if($appointmentDetails['appointmentType']=='handover'){?>2<?php } elseif($appointmentDetails['appointmentType']=='defect'){ echo '1';}?> hours</p>
                                </li>
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <p><?php echo $appointmentDetails['customerDetails']['unitNumber'];?></p>
                                </li>
                            </ul>
                            <input type="hidden" id="appointmentId" value="<?php echo isset($appointmentDetails['appointmentId'])?$appointmentDetails['appointmentId']:'';?>">
                            <button type="submit" id="" href="javascript:void(0)" class=" button  submit-handover1" data-toggle="modal" data-value="<?php echo $this->security->get_csrf_hash();?>"> Submit</button>
                        </div>
                    </div>

                </div>
                <!--// Booking Summary -->
                
                <div class="col-md-12 set-sm-fit mb-4">
                        <div class="preference-title">
                            <h4>Timeline Log</h4>
                        </div>
                        <div class="preferences">
                                <div class="row">
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <p class="">Comment</p>
                                            <textarea name="appointmentComment" class="form-control" style="text-align: right"></textarea>
                                            <div style="position: relative;overflow: hidden;top: -37px;left: -14px;" class="file btn btn-lg">
                                                <i class="mdi mdi-file-outline"></i>
                                                <input type="file" multiple name="commentfile[]" style="position: absolute;opacity: 0;top: 4px;cursor: pointer;">
						</div>
                                        </div>
                                        <span id="appoinmentDateError" class="form-text text-danger"><?php echo form_error('appointmentComment');?></span>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <p class="">Reason</p>
                                            <textarea class="form-control" name="appointmentReason" style="text-align: right" ></textarea>
                                            <div style="position: relative;overflow: hidden;top: -37px;left: -14px;" class="file btn btn-lg">
                                                <i class="mdi mdi-file-outline"></i>
                                                <input type="file" multiple name="reasonfile[]" style="position: absolute;opacity: 0;top: 4px;cursor: pointer;">
						</div>
                                        </div>
                                        <span id="appoinmentDateError" class="form-text text-danger"><?php echo form_error('appointmentReason');?></span>
                                        </div>
                                        <div class="col-sm-6 col-md-4 col-xl">
                                        <div class="form-check mr-4 mb-4">
                                            <input id="checkbox19" name="visiblefortenant" class="form-check-input filled-in styled-checkbox" checked="" type="checkbox">
                                            <label for="checkbox19" class="tenantVisibal form-check-label check-red">visible for tenant</label>
                                        </div>
                                    </div>
                                        <div class="col-sm-6 col-md-4 col-xl">
                                        <div class="form-check mr-4 mb-4">
                                            <input id="checkbox20" name="calllog" class="form-check-input filled-in styled-checkbox" checked="" type="checkbox">
                                            <label for="checkbox20" class="tenantVisibal form-check-label check-red">Call log</label>
                                        </div>
                                    </div>
                                        
                                        <div class="timeline_content">
                            <!--<div class="timeline-timestamp">
                                <span class="badge badge-primary">18 May 2018</span>
                            </div>-->
                            <ul class="timeline comment-timeline">
                              
                                <?php if($appointmentDetails['commentDetails']) {
                                        
                                        
                                        foreach($appointmentDetails['commentDetails'] as $commentDetails){
                                                 if(isset($commentDetails['appointmentCommentId'])){?>
                                <li class="timeline-inverted">
                                    <div class="timeline-badge">
                                        <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                        <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="card story-widget">
                                            <div class="card-header  d-flex justify-content-between align-items-center">
                                                <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                
                                            </div>
                                            <div class="card-body">
                                                <p><?php echo $commentDetails['appointmentComment'];?></p>
                                                <p><?php echo $commentDetails['appointmentReason'];?></p>
                                                <div id="gallery<?php echo $commentDetails['appointmentCommentId']?>" class="gallery1">
                                                                        
                                                                    </div>
                                                                    <?php
                                                                    $imageArray=array();
                                                                    if(isset($commentDetails['commentImage'])) {
                                                                    foreach($commentDetails['commentImage'] as $image){
                                                                        
                                                                        $imageArray[] = (base_url('uploads/appointment/'.$image['image'].'?id='.$image['appointmentCommentImageId']));
                                                                        ?>
                                                                        
                                                                    <?php }
                                                                    //_print_r($imageArray);
                                                                    $html ='';
                                                                     if( isset($imageArray)){
                                                                        
                                                                        $i=0;
                                                                        foreach($imageArray as $img){
                                                                         $html .="'".$img."'";
                                                                        if(isset($imageArray[$i+1]))
                                                                         $html .=',';
                                                                        $i++;
                                                                        }
                                                                        }?>
                                                                   
                                                                    <script>
                                                                        //var images = [];
                                                                        var images = [<?php echo $html;?>];
                                                                        $(function() {
                                                                       
                                                                                       $('#gallery<?php echo $commentDetails['appointmentCommentId']?>').imagesGrid({
                                                                                               images:[<?php echo $html;?>]
                                                                                               });
                                                                                       
                                                                       
                                                                                   });</script>
                                                                    <?php }?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php }elseif(isset($commentDetails['logId'])){?>
                                                        
                                                        <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget ">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['logText'];?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                     
                                                    
                                <?php } } }?>
                                <?php if(isset($appointmentDetails['parentCommentDetails'])&&!empty($appointmentDetails['parentCommentDetails'])) {
                                        
                                        
                                        foreach($appointmentDetails['parentCommentDetails'] as $parent){
                                                 ?>
                                <li class="timeline-inverted">
                                    <div class="timeline-badge">
                                        <p class=""><strong><?php echo date('d',strtotime($parent['addedOn']));?></strong><p><?php echo date('M',strtotime($parent['addedOn']));?></p>
                                        <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="card story-widget">
                                            <div class="card-header  d-flex justify-content-between align-items-center">
                                                <h4 class="heading-primary mb-0"><?php echo ($parent['appointmentType']=='handover')?'Handover':'Defect'.($parent['defectCategory']?'. '.$parent['defectCategory']:'').($parent['defectLevel']?'. '.$parent['defectLevel']:'').($parent['defectIssue']?'. '.$parent['defectIssue']:'');?></h4>
                                                
                                            </div>
                                            <div class="noti-num">#<?php echo $parent['ticketNo']?></div>
                                            <div class="noti-time ">
                                                      <span><i class="mdi mdi-clock " style=""></i></span><?php echo $parent['appointmentTime']?></div>
                                            <div class="card-body">
                                                <p><?php echo $parent['appointmentNotes'];?></p>
                                                
                                                
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <?php if(isset($parent['commentDetails'])&&$parent['commentDetails']) { ?>
                                    
                                    <div class="timeline_content">
                            <!--<div class="timeline-timestamp">
                                <span class="badge badge-primary">18 May 2018</span>
                            </div>-->
                            <ul class="timeline comment-timeline">
                              
                             <?php foreach($parent['commentDetails'] as $commentDetails){
                                                 if(isset($commentDetails['appointmentCommentId'])){?>
                                <li class="timeline-inverted">
                                    <div class="timeline-badge">
                                        <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                        <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="card story-widget">
                                            <div class="card-header  d-flex justify-content-between align-items-center">
                                                <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                
                                            </div>
                                            <div class="card-body">
                                                <p><?php echo $commentDetails['appointmentComment'];?></p>
                                                <p><?php echo $commentDetails['appointmentReason'];?></p>
                                                <div id="gallery<?php echo $commentDetails['appointmentCommentId']?>" class="gallery1">
                                                                        
                                                                    </div>
                                                                    <?php
                                                                    $imageArray=array();
                                                                    if(isset($commentDetails['commentImage'])) {
                                                                    foreach($commentDetails['commentImage'] as $image){
                                                                        
                                                                        $imageArray[] = (base_url('uploads/appointment/'.$image['image'].'?id='.$image['appointmentCommentImageId']));
                                                                        ?>
                                                                        
                                                                    <?php }
                                                                    //_print_r($imageArray);
                                                                    $html ='';
                                                                     if( isset($imageArray)){
                                                                        
                                                                        $i=0;
                                                                        foreach($imageArray as $img){
                                                                         $html .="'".$img."'";
                                                                        if(isset($imageArray[$i+1]))
                                                                         $html .=',';
                                                                        $i++;
                                                                        }
                                                                        }?>
                                                                   
                                                                    <script>
                                                                        //var images = [];
                                                                        var images = [<?php echo $html;?>];
                                                                        $(function() {
                                                                       
                                                                                       $('#gallery<?php echo $commentDetails['appointmentCommentId']?>').imagesGrid({
                                                                                               images:[<?php echo $html;?>]
                                                                                               });
                                                                                       
                                                                       
                                                                                   });</script>
                                                                    <?php }?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php }elseif(isset($commentDetails['logId'])){?>
                                                        
                                                        <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget ">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['logText'];?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                     
                                                    
                                <?php } }
                                ?>
                            </ul>
                                    </div>
                                    <?php }?>
                                </li>
                                
                                                     
                                                    
                                <?php  } }?>
                                <?php if($appointmentDetails['filename']){?>
                                 
                                 <li class="timeline-inverted">
                                    <div class="timeline-badge">
                                        <p class=""><strong><?php echo date('d',strtotime($appointmentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($appointmentDetails['addedOn']));?></p>
                                        <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="card story-widget">
                                            <div class="card-header  d-flex justify-content-between align-items-center">
                                                <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($appointmentDetails['addedOn']));?></h4>
                                                
                                            </div>
                                            <div class="card-body">
                                                
                                                <div id="gallery<?php echo $appointmentDetails['appointmentId']?>" class="gallery1">
                                                                        
                                                                    </div>
                                                                    <?php
                                                                    $imageArray=array();
                                                                    if(isset($appointmentDetails['filename'])) {
                                                                   
                                                                        
                                                                        $img = (base_url('uploads/appointment/'.$appointmentDetails['filename']));
                                                                        ?>
                                                                        
                                                                    <?php 
                                                                    //_print_r($imageArray);
                                                                    $html ='';
                                                                     
                                                                        
                                                                        $i=0;
                                                                       
                                                                         $html .="'".$img."'";
                                                                        
                                                                        ?>
                                                                   
                                                                    <script>
                                                                        //var images = [];
                                                                        var images = [<?php echo $html;?>];
                                                                        $(function() {
                                                                       
                                                                                       $('#gallery<?php echo $appointmentDetails['appointmentId']?>').imagesGrid({
                                                                                               images:[<?php echo $html;?>]
                                                                                               });
                                                                                       
                                                                       
                                                                                   });</script>
                                                                    
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                              
                              <?php }?>
                            </ul>
                                        </div>
                                        
                                
                                </div>
                        </div>
                </div>
            <?php echo form_close();?>
                            </div>
                        </div>
                         </div>
                    </div>
                    
                    
                    
                </div>
                
                
                
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        