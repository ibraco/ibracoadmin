<!--**********************************
            Sidebar start
        ***********************************-->
        <div class="nk-sidebar">           
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label">Dashboard</li>
                    <li class="mega-menu mega-menu-lg">
                        <a class="" href="<?php echo base_url('admin/dashboard');?>" aria-expanded="false">
                            <i class="mdi mdi-view-dashboard"></i><span class="nav-text">Dashboard</span>
                        </a>
                        
                    </li>
                    <li class="mega-menu mega-menu-sm">
                        <a class="" href="<?php echo base_url('admin/calendarView');?>" aria-expanded="false">
                                <i class="mdi mdi-calendar"></i>
                                <span class="nav-text">Calender View</span>
                        </a>
                        
                    </li>
                    
                    <li><a class="" href="<?php echo base_url('admin/notification');?>" aria-expanded="false">
                    <i class="mdi mdi-bell"></i>
                    <span class="nav-text">Notification</span>
                    <span class="badge badge-danger nav-badge"><?php echo isset($notificationCount)?$notificationCount:0;?></span>
                    </a>
                        
                    </li>
                    <li><a class="" href="<?php echo base_url('admin/tenant');?>" aria-expanded="false">
                    <i class="mdi mdi-account-box"></i>
                    <span class="nav-text">Tenant Profile</span></a>
                        
                    </li>
                    <li><a class="" href="<?php echo base_url('admin/settings');?>" aria-expanded="false">
                    <i class="mdi mdi-settings"></i>
                    <span class="nav-text">Settings</span> </a>
                        
                    </li>
                    
                    

                </ul>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->