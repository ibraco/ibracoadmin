
        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <!--<p>Copyright &copy; Designed by <a href="https://themeforest.net/user/digitalheaps">Digitalheaps</a>, Developed by <a href="https://themeforest.net/user/quixlab">Quixlab</a> 2018</p>-->
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->

        
        
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="<?php echo base_url('lib/admin/assets/plugins/common/common.min.js');?>"></script>
    <script src="<?php echo base_url('lib/admin/js/custom.min.js');?>"></script>
    <script src="<?php echo base_url('lib/admin/js/settings.js');?>"></script>
    <script src="<?php echo base_url('lib/admin/js/gleek.js');?>"></script>
    <script src="<?php echo base_url('lib/admin/js/styleSwitcher.js');?>"></script>

    <script src="<?php echo base_url('lib/admin/assets/plugins/chart.js/Chart.bundle.min.js');?>"></script>
    <script src="<?php echo base_url('lib/admin/js/dashboard/dashboard-5.js');?>"></script>
     <script src="<?php echo base_url('lib/admin/assets/plugins/jqueryui/js/jquery-ui.min.js');?>"></script>
    <script src="<?php echo base_url('lib/admin/assets/plugins/moment/moment.min.js');?>"></script>
    <script src="<?php echo base_url('lib/admin/assets/plugins/fullcalendar/js/fullcalendar.min.js');?>"></script>
    <script src="<?php echo base_url('lib/admin/js/plugins-init/fullcalendar-init.js');?>"></script>
    <script src="<?php echo base_url('lib/admin/assets/plugins/datatables/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo base_url('lib/admin/js/plugins-init/datatables.init.js?version='.date('his'));?>"></script>
    <script src="<?php echo base_url('lib/admin/js/custom.js?version='.date('his'));?>"></script>
    
    <script src="<?php echo base_url('lib/datepicker/js/bootstrap-datepicker.js');?>"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
 

        <script src="<?php echo base_url('lib/src/images-grid.js');?>"></script>
        <link rel="stylesheet" href="<?php echo base_url('lib/src/images-grid.css');?>">
 
 <script >
  
  var today = new Date();
  var MAXDATE = today.getDate();
  
  today.setDate(MAXDATE);
  $(function() {
   
   
   
  $('.date').datepicker({
  format: 'dd/mm/yyyy',
  todayHighlight: true,
  autoclose: true,
   startDate:today
   
}).on('changeDate',function(date){
    //$('#handOver-date').html(date);
    
    $('#handOver-date p').html( moment(date.date).format('ddd DD MMM YYYY'));
    });
  // $('.date').datepicker("setDate", "+3");

  
  
$('.close-button').unbind();

$('.close-button').click(function() {
  if ($('.datepicker').is(":visible")) {
    $('.date').datepicker('hide');
    
  } else {
    $('.date').datepicker('show');
    
  }
});

  $(function() {
    // Input radio-group visual controls
    $('.radio-group label').on('click', function(){
   //  alert('ss');
     $('.radio-group label').removeClass('active');
     
     $('#handOver-time p').html($('input[type="radio"]:checked').val())
        $(this).addClass('active');
        
    });
    
    // Input radio-group visual controls
    $('.appointmentStatus label').on('click', function(){
     var value = $(this).attr('data-id');
     $('.appointmentStatus label').removeClass('active');
     
     
     //$('#handOver-time p').html($('input[type="radio"]:checked').val())
        $(this).addClass('active '+value);
        
    });
});
  
  $('#unitNumber').on('change',function(){
   
   var value = <?php echo isset($unitList)?json_encode($unitList):'NULL'?>;
   var id = $(this).val();
   $('#tenantName').html(value[id].customerName);
   $('#unitAddress').html(value[id].unitNumber);
   });
  });
   
  
 </script>
  
</body>

</html>