<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>IBRACO|ADMIN</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('lib/admin/assets/images/favicon.ico');?>">
    <!-- fullcalendar -->
    <link href="<?php echo base_url('lib/admin/assets/plugins/fullcalendar/css/fullcalendar.min.css');?>" rel="stylesheet">
    
    <link href="<?php echo base_url('lib/admin/assets/plugins/datatables/css/jquery.dataTables.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('lib/admin/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Custom Stylesheet -->
    <link href="<?php echo base_url('lib/admin/css/style.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('lib/admin/css/custom.css?version='.date('his'));?>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" rel="stylesheet">
<script src= "http://code.jquery.com/jquery-1.9.0.js"> </script>
<script src= "http://code.jquery.com/jquery-migrate-1.2.1.js"> </script>
    <script>

  var baseurl = "<?php print base_url(); ?>";
  var csrfCode = "<?php echo $this->security->get_csrf_hash();?>";
  var token = "<?php echo $this->security->get_csrf_token_name(); ?>";
  var actionUrl = "<?php echo site_url("admin/") ?>";
</script>
</head>

<body>
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo"><a href="<?php echo base_url('admin/dashboard');?>"><b><img src="<?php echo base_url('lib/admin/login/images/logo.png')?>" alt="" > </b></a>
            </div>
            <div class="nav-control">
                <div class="hamburger"><span class="line"></span>  <span class="line"></span>  <span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">    
            <div class="header-content">
                <div class="header-left">
                    <ul>
                        <li class="icons position-relative"><a href="javascript:void(0)"><i class="icon-magnifier f-s-16"></i></a>
                            <div class="drop-down animated bounceInDown">
                                <div class="dropdown-content-body">
                                    <div class="header-search" id="header-search">
                                        <form action="#">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Search">
                                                <div class="input-group-append"><span class="input-group-text"><i class="icon-magnifier"></i></span>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="header-right">
                    <ul>
                        
                        <li class="icons">
                            <a href="javascript:void(0)" class="log-user">
                                <img src="<?php echo base_url('lib/admin/assets/images/avatar5.png');?>" alt=""> <span><?php echo $this->session->userdata('ad')['userName']?></span>  <i class="fa fa-caret-down f-s-14" aria-hidden="true"></i>
                            </a>
                            <div class="drop-down dropdown-profile animated bounceInDown">
                                <div class="dropdown-content-body">
                                    <ul>
                                        
                                        <li><a href="<?php echo base_url('admin/logout');?>"><i class="icon-power"></i> <span>Logout</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--**********************************
            Header end
        ***********************************-->

        