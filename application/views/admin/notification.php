 <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-0">
                        <h4 style="color: red">Hello, <span>Welcome</span></h4>
                    </div>
                    <!--<div class="col p-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Layout</a>
                            </li>
                            <li class="breadcrumb-item active">Blank</li>
                        </ol>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php if($notificationList){
                                    foreach($notificationList as $noti){
                                        $appointmentId = $this->encryption->encrypt($noti['appointmentId']);
                                        $appointmentId =str_replace(array('+', '/', '='), array('-', '_', '~'), $appointmentId);?>
                                <div class="card notification read-stage">
                                 
                                    <div class="box">
                                          <div class="box-header with-border">
                                            <h3 class="box-title noti-title"><?php echo $noti['notificationText']?></h3>
                                            
                                                <div class="noti-time ">
                                                      <span><i class="mdi mdi-clock " style=""></i></span><?php echo date('d M',strtotime($noti['appointmentDate'])).' '.$noti['appointmentTime']?></div>
                                                  <div class="box-tools pull-right">
                                              <?php if($noti['appointmentStatus']=='open'){?>
                                              <button type="button"  class="btn btn-success btn-ft sett-btn " onclick="window.location.href='editAppointment/<?php echo $noti['appointmentId']?>'" data-value="<?php echo $this->security->get_csrf_hash();?>" data-id="<?php echo $appointmentId?>" data-info="<?php echo $noti['notificationId']?>">
                                                Accept</button>
                                              <?php }?>
                                              
                                              
                                            </div>                                      
                                               
                                                    <div class="noti-notes"><?php echo $noti['appointmentNotes']?></div>
                                                    <div><i class="mdi mdi-account user-account"><?php echo ($noti['contractorId']>0)?$noti['contractorName']:'Unassigned'?></i></div>
                                          </div>
                                          
                                    </div>
                                    
                              </div>
                                <?php } }?>
                                    
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        
<!-- stylesheet for this button -->

                <!--**********************************
            Content body end
        ***********************************-->