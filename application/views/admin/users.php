 <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-0">
                        <h4 style="color: red">Hello, <span>Welcome</span></h4>
                    </div>
                    <!--<div class="col p-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Layout</a>
                            </li>
                            <li class="breadcrumb-item active">Blank</li>
                        </ol>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="" style="text-align: center">
                                    <a id="importTenant" href="javascript:void(0)" class="btn button" data-toggle="modal" data-value="<?php echo $this->security->get_csrf_hash();?>" >+ Import Tenant</a>
                                     <a id="exportTenant" href="javascript:void(0)" class="btn button" data-toggle="modal" data-value="<?php echo $this->security->get_csrf_hash();?>" >+ Export Tenant</a>
                                </div>
                                <!-- Modal -->
                                    <div id="load_popup_modal1_show_id" class="modal fade" tabindex="-1"></div>
                                    <div class="table-responsive">
                                <table id="example-ajax" class="display" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Tenant Name</th>
                                            <th>Unit Number</th>
                                            <th>I/C Number</th>
                                            <th>Primary Contact</th>
                                            <th>Emergency contact</th>
                                            <th>Emergency contact number</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    
                                </table>
                                    </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        
<!-- stylesheet for this button -->

                <!--**********************************
            Content body end
        ***********************************-->