 
 <div class="content-body calender-body" >
            <div class="container-fluid">
<div class="row">
                    <div class="col-xl-11" style="margin-left: 0%">
                        <div id="accordion-three" class="accordion">
                                <?php if(!empty($appointmentList)){
                                 
                                        foreach($appointmentList as $appo){
                                                
                                        $appointmentId = $appo['appointmentId'];
                                        ?>
                                        <div>
                              <div class="card notification <?php echo $appo['appointmentStatus'];?>-stage">
                                 
                                    <div class="box">
                                          <div class="box-header with-border">
                                            <h3 class="box-title noti-title"><?php echo ($appo['appointmentType']=='handover')?'Handover':'Defect'.($appo['defectCategory']?'. '.$appo['defectCategory']:'').($appo['defectLevel']?'. '.$appo['defectLevel']:'').($appo['defectIssue']?'. '.$appo['defectIssue']:'');?> for <?php echo $appo['unitNumber']?></h3>
                                            <div class="noti-num">#<?php echo $appo['ticketNo'];?></div>
                                                <div class="noti-time ">
                                                      <span><i class="mdi mdi-clock " style=""></i></span><?php echo date('j M Y',strtotime($appo['appointmentDate'])).' '.$appo['appointmentTime'];?>
                                                </div>
                                                
                                                
                                            <div class="box-tools pull-right">
                                             <?php if($appo['appointmentType']=='handover'){?>
                                              <a href="<?php echo base_url('admin/editAppointment/'.$appointmentId);?>" class="edit-icon"><i class="mdi mdi-pencil-circle noti-icon"></i></a>
                                              <?php }elseif($appo['appointmentType']=='defect') {?>
                                              <a href="<?php echo base_url('admin/editDefectAppointment/'.$appointmentId);?>" class="edit-icon"><i class="mdi mdi-pencil-circle noti-icon"></i></a>
                                              <?php }?>
                                              <button type="button" class="btn btn-box-tool collapsed"  data-toggle="collapse" data-target="#collapseTwo<?php echo $appo['appointmentId'];?>" aria-expanded="false" aria-controls="collapseTwo5">
                                                <i class="fa fa-minus"></i></button>
                                              
                                            </div>
                                            
                                            <div class="noti-notes"><?php echo $appo['appointmentNotes']?$appo['appointmentNotes']:'&nbsp;';?></div>
                                            <div><i class="mdi mdi-account user-account"><?php echo ($appo['contactName'])?$appo['contactName']:'Unassigned';?></i></div>
                                            <div class="pull-right noti-dates"><?php echo date('d M Y',strtotime($appo['creationDate']));?></div>
                                          </div>
                                          
                                    </div>
                                    
                              </div>
                              <div id="collapseTwo<?php echo $appo['appointmentId'];?>" class="collapse" data-parent="#accordion-three">
                                          <div class="box-body">
                                            &nbsp;
                                          </div>
                                          <!-- /.box-body -->
                                          <div class="box-footer">
                                                <?php if(isset($appo['commentDetails'])&&!empty($appo['commentDetails'])) {?>
                                            <div class="timeline_content">
                            
                                                <ul class="timeline comment-timeline">
                                                    <?php 
                                                            foreach($appo['commentDetails'] as $commentDetails){
                                                             if(isset($commentDetails['appointmentCommentId'])){?>
                                                    <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget comment-list">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['appointmentComment'];?></p>
                                                                    <p><?php echo $commentDetails['appointmentReason'];?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php } elseif(isset($commentDetails['logId'])){ ?>
                                                     <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget comment-list">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['logText'];?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                     <?php } }?>
                                                    
                                                </ul>
                                        </div>
                                            <?php }
                                            
                                            if(isset($appo['parentAppointment'])&&!empty($appo['parentAppointment'])) {
                                                foreach($appo['parentAppointment'] as $parent){
                                                ?>
                                            
                                            
                                            <div class="card notification timeline_content" id="parent-accordion">
                                 
                                    <div class="box">
                                          <div class="box-header with-border">
                                            <h3 class="box-title noti-title">Handover</h3>
                                            <div class="noti-num">#<?php echo $parent['ticketNo']?></div>
                                                <div class="noti-time ">
                                                      <span><i class="mdi mdi-clock " style=""></i></span><?php echo $parent['appointmentTime']?>                                               </div>
                                                
                                                
                                                                                           <div class="box-tools pull-right">
                                              <button type="button" class="btn btn-box-tool collapsed" data-toggle="collapse" aria-expanded="false" data-target="#collapseTwo<?php echo $parent['appointmentId'];?>" aria-controls="collapseTwo5">
                                                <i class="fa fa-minus"></i></button>
                                              
                                            </div>
                                        <div class="noti-notes"><?php echo $parent['appointmentNotes'];?></div>
                                          </div>
                                          
                                    </div>
                                    
                              </div>
                                            <div id="collapseTwo<?php echo $parent['appointmentId'];?>" class="collapse" data-parent="#parent-accordion">
                                          <div class="box-body">
                                            &nbsp;
                                          </div>
                                          <!-- /.box-body -->
                                          <div class="box-footer">
                                                <?php if(isset($parent['commentDetails'])&&!empty($parent['commentDetails'])) {?>
                                            <div class="timeline_content">
                            
                                                <ul class="timeline comment-timeline">
                                                    <?php 
                                                            foreach($parent['commentDetails'] as $commentDetails){?>
                                                    <li class="timeline-inverted">
                                                        <div class="timeline-badge">
                                                            <p class=""><strong><?php echo date('d',strtotime($commentDetails['addedOn']));?></strong><p><?php echo date('M',strtotime($commentDetails['addedOn']));?></p>
                                                            <div class="li-bulet"><i class="mdi mdi-checkbox-blank-circle"></i></div>
                                                        </div>
                                                        <div class="timeline-panel">
                                                            <div class="card story-widget comment-list">
                                                                <div class="card-header  d-flex justify-content-between align-items-center">
                                                                    <h4 class="heading-primary mb-0"><?php echo date('j M Y h:i A',strtotime($commentDetails['addedOn']));?></h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p><?php echo $commentDetails['appointmentComment'];?></p>
                                                                    <p><?php echo $commentDetails['appointmentReason'];?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php } ?>
                                                    
                                                </ul>
                                        </div>
                                            <?php }?>
                                          </div>
                                            </div>
                                            <?php } }?>
                                            
                                          </div>
                                          <!-- /.box-footer-->
                                          </div>
                                        </div>
                              <?php } }?>
                              <!--<div class="card notification open-stage">
                                 
                                    <div class="box">
                                          <div class="box-header with-border">
                                            <h3 class="box-title">Title</h3>
                                  
                                            <div class="box-tools pull-right">
                                              <button type="button" class="btn btn-box-tool collapsed"  data-toggle="collapse" data-target="#collapseTwo5" aria-expanded="false" aria-controls="collapseTwo5">
                                                <i class="fa fa-minus"></i></button>
                                              
                                            </div>
                                          </div>
                                          <div id="collapseTwo5" class="collapse" data-parent="#accordion-three">
                                          <div class="box-body">
                                            Start creating your amazing application!
                                          </div>
                                          
                                          <div class="box-footer">
                                            Footer
                                          </div>
                                          
                                          </div>
                                    </div>
                                    
                              </div>-->
                        </div>
                    </div>
                </div>
</div>
            <!-- #/ container -->
        </div>
 <style>
  .calender-body {

    margin-left: 0px;

}
 </style>