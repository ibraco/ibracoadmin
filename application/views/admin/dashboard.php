

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col p-md-0">
                        <h4 style="color: red">Hello, <span>welcome</span></h4>
                    </div>
                    <!--<div class="col p-md-0">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a>
                            </li>
                            <li class="breadcrumb-item active">Real Estate</li>
                        </ol>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-body rounded stat-widget-seven gradient-7">
                                <div class="media pl-xl-5 align-items-center">
                                    <img class="mr-3 mt-2" src="<?php echo base_url('lib/admin/assets/images/icons/12.png')?>" alt="">
                                    <div class="media-body pl-3">
                                        <h2 class="mt-0 mb-2"><?php echo $todayBookedAppointment?></h2>
                                        <h6 class="text-uppercase text-white">NUMBER OF APPOINTMENT BOOKED TODAY</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-body rounded stat-widget-seven gradient-6">
                                <div class="media pl-xl-5 align-items-center">
                                    <img class="mr-3 mt-2" src="<?php echo base_url('lib/admin/assets/images/icons/12.png')?>" alt="">
                                    <div class="media-body pl-3">
                                        <h2 class="mt-0 mb-2"><?php echo $weekBookedAppointment?></h2>
                                        <h6 class="text-uppercase text-white">NUMBER OF APPOINTMENT BOOKED THIS WEEK</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-body rounded stat-widget-seven gradient-5">
                                <div class="media pl-xl-5 align-items-center">
                                    <img class="mr-3 mt-2" src="<?php echo base_url('lib/admin/assets/images/icons/12.png')?>" alt="">
                                    <div class="media-body pl-3">
                                        <h2 class="mt-0 mb-2"><?php echo $resolvedAppointment?></h2>
                                        <h6 class="text-uppercase text-white">SUMMARY NUMBER OF RESOLVED APPOINTMENT</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-body rounded stat-widget-seven gradient-4">
                                <div class="media pl-xl-5 align-items-center">
                                    <img class="mr-3 mt-2" src="<?php echo base_url('lib/admin/assets/images/icons/12.png')?>" alt="">
                                    <div class="media-body pl-3">
                                        <h2 class="mt-0 mb-2"><?php echo $pendingAppointment?></h2>
                                        <h6 class="text-uppercase text-white">SUMMARY NUMBER OF PENDING APPOINTMENT</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row -->
                
                
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card transparent-card m-b-0">
                            <div class="card-header">
                                <h4 class="card-title">Property Overview</h4>
                            </div>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                        <div class="filter-div"><button class="btn button " id="exportExcel">+Export</button>
                                        <select class="form-control status-filter" id="status-filter" >
                                                <option value="booked" >Appointment Booked</option>
                                                <option value="resolved">Appointment Resolved</option>
                                                <option value="pending">Appointment Pending</option>
                                        </select>
                                        <select class="form-control date-filter" id="date-filter">
                                                <option value="all">All</option>
                                                <option value="today" >Today</option>
                                                <option value="weekly">Weekly</option>
                                                <option value="monthly">Monthly</option>
                                                <option value="aging">Aging 14 and more</option>
                                        </select>
                                        <select class="form-control date-filter" id="cat-filter">
                                            <option value="">Select categroty</option>
                                            <?php foreach ($defectList as $key => $value) { ?>

                                                <option value="<?php echo $value['name']?>"><?php echo $value['name'];?></option>
                                            <?php }?>
                                        </select>
                                        <select class="form-control date-filter" id="issue" style="display:none"></select>
                                        </div>
                                    <table id="appointmentList-table" class="table table-padded table-responsive-fix-big property-overview-table">
                                        <thead>
                                            <tr>
                                                <th>Issue ID.</th>
                                                <th>Unit/Block Number</th>
                                                <th>Tenant Name</th>
                                                <th>Appointment Time</th>
                                                <th>Creation Time</th>
                                                <th>Service Request</th>
                                                <th>Service Provided By</th>
                                                <th>Status</th>
                                                <th style="background: none">Update Status</th>
                                            </tr>
                                        </thead>
                                        
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        <script>
    var catData = <?php $level =  defectLevel(); echo json_encode($level);?>
</script>
        