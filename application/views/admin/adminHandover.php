        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                
                <div class="row">
                    <div class="msg" style="width: 100%"></div>
                    <div class="col-xl-6 col-xxl-12 col-sm-12">
                        <div class="card">
                                <?php echo $this->session->flashdata('msg');?>
                         <div class="card-body">
                            <div class="row mt-5">
                                <?php
                                $attribute = array('class'=>'');
                                if($this->uri->segment(3))
                                $url = base_url('admin/updateHandover/'.$this->uri->segment(3));
                                else
                                $url = base_url('admin/addHandoverRequest/');
                                
                                echo form_open_multipart($url);?>
                <div class="col-md-8 set-sm-fit mb-4">
                    
                        <div class="preference-title">
                            <h4>Handover Preferences</h4>
                        </div>
                        <!-- preferences Wrap -->
                        <div class="preferences">
                            <!-- Styled radio btn 1 -->
                            <div class="hd-select preference-radio">
                                <p>Unit/block number</p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 sm-box">
                                        <div class="input-group  insertInfo bitmap " >
                                        <select type="text" class="form-control" name="unitNumber" id="unitNumber">
                                                <option value="">Select Unit Number</option>
                                                <?php if($unitList)
                                                {
                                                        foreach($unitList as $unit){?>
                                                
                                                <option value="<?php echo $unit['customerId']?>" <?php echo (isset($appointmentDetails['unitNumber'])&&$appointmentDetails['unitNumber']==$unit['customerId'])?'selected':'';?>><?php echo $unit['unitNumber']?></option>
                                                <?php } }?>
                                        </select>
                                        
                                      </div>
                                        <span id="appoinmentDateError" class="form-text text-danger"><?php echo form_error('unitNumber');?></span>
                                    </div>
                                    
                                    
                                    
                                    
                                </div>
                            </div>
                            <div class="hd-tnt preference-radio">
                                <p>Tenant Name</p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 sm-box">
                                        <div class="input-group  insertInfo bitmap " >
                                        <label id="tenantName">&nbsp;</label>
                                        
                                      </div>
                                        <p id="appoinmentDateError" class="form-text text-danger"></p>
                                    </div>
                                    
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>
                        
                            <div class="preferences">
                            <div class="preference-radio">
                                <p>Choose a date?</p>
                                <div class="row">
                                    <div class="col-xs-12 col-md-4 sm-box">
                                        <div class="input-group date insertInfo bitmap " data-provide="datepicker">
                                        <input type="text" class="form-control" name="appointmentDate" id="appoinmentDate" value="<?php echo isset($appointmentDetails['appointmentDate'])?date('d/m/Y',strtotime($appointmentDetails['appointmentDate'])):''?>">
                                        <div class="input-group-addon close-button">
                                          <i class="mdi mdi-calendar" style="font-size: 16px ;margin-left:-6px"></i>
                                        </div>
                                      </div>
                                        <span id="appoinmentDateError" class="form-text text-danger"><?php echo form_error('appointmentDate');?></span>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <!--// Styled radio btn 1 -->
                            <!-- Styled radio btn 2 -->
                            <div class="preference-radio mt-4">
                                <p>Available Time</p>
                                <div class="row">
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                         <div class="input-group">
                                         <div class="btn-group radio-group a800am900am1" >
                                            <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='8:30 AM - 10:30 AM')?'active':''?>">
                                              <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='8:30 AM - 10:30 AM')?'checked':''?>  autocomplete="off" value="8:30 AM - 10:30 AM" > 8:30 AM - 10:30 AM
                                            </label>
                                         </div>
                                         <span id="appoinmentTimeError" class="form-text text-danger"><?php echo form_error('appointmentTime');?></span>
                                         </div>
                                         </div>
                                    </div>
                                    <div class="col-md-6 sm-box">
                                        <div class="form-group">
                                           <div class="input-group">
                                           <div class="btn-group radio-group a800am900am1" >
                                             <label class="btn btn-primary  day-btn <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='1:30 PM - 3:30 PM')?'active':''?>">
                                               <input type="radio" name="appointmentTime" <?php echo (isset($appointmentDetails['appointmentTime'])&&$appointmentDetails['appointmentTime']=='1:30 PM - 3:30 PM')?'checked':''?> autocomplete="off" value="1:30 PM - 3:30 PM" > 1:30 PM - 3:30 PM
                                             </label>
                                           </div>
                                           </div>
                                           </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            
                            
                            <div class="preference-radio mt-4">
                                <!-- Comment box -->
                                <p>Contractor PIC</p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <select class="form-control" name="contractorId">
                                                <option value="">Select PIC</option>
                                                <?php if($emailList){
                                                        foreach($emailList as $email){?>
                                                <option value="<?php echo $email['departmentEmailId']?>" <?php echo (isset($appointmentDetails['contractorId'])&&$appointmentDetails['contractorId']==$email['departmentEmailId'])?'selected':''?>><?php echo $email['contactName']?></option>
                                                <?php }}?>
                                        </select>
                                        <span id="notesError" class="form-text text-danger"><?php echo form_error('contractorId');?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--// Styled Check box -->
                            <div class="preference-radio mt-4">
                                <!-- Comment box -->
                                <p>Comments</p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea id="notes" name="notes" class="optinal-textarea" style="width: 465px;height: 85px;"></textarea>
                                        <span id="notesError" class="form-text text-danger"><?php echo form_error('notes');?></span>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <!--// Personal Details -->
                        </div>
                        <!--// preferences Wrap -->
                    
                </div>

                <!-- Booking Summary -->
                <div class="col-md-4 set-sm-fit">
                    <div style="height: auto;"></div><div data-toggle="affix" class="">
                        <!-- data-toggle="affix" -->
                        <div class="preference-title">
                            <h4>Booking Summary</h4>
                        </div>
                        <div class="fesilities">
                            <ul>
                                <li><i class="fa fa-paint-brush" aria-hidden="true"></i>
                                    <p>Handover</p>
                                </li>
                                <li id="handOver-date"><i class="fa fa-calendar" aria-hidden="true"></i>
                                    <p><?php echo isset($appointmentDetails['appointmentDate'])?date('D d M Y',strtotime(str_replace('/','-',$appointmentDetails['appointmentDate']))):'-'?></p>
                                </li>
                                <li id="handOver-time"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <p><?php echo isset($appointmentDetails['appointmentTime'])?$appointmentDetails['appointmentTime']:'-'?></p>
                                </li>
                                <li><i class="fa fa-refresh" aria-hidden="true"></i>
                                    <p>2 hours</p>
                                </li>
                                <li><i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <p id="unitAddress">-</p>
                                </li>
                            </ul>
                            <input type="hidden" id="appointmentId" value="<?php echo isset($appointmentDetails['appointmentId'])?$appointmentDetails['appointmentId']:'';?>">
                            <button type="submit" id="" href="javascript:void(0)" class=" button  submit-handover1" data-toggle="modal" data-value="<?php echo $this->security->get_csrf_hash();?>"> Submit</button>
                        </div>
                    </div>

                </div>
                <!--// Booking Summary -->
                
                
            <?php echo form_close();?>
                            </div>
                        </div>
                         </div>
                    </div>
                    
                    
                    
                </div>
                
                
                
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        