<div id="load_popup_modal_contant" class="" role="dialog">
                                        <div class="modal-dialog" role="document" style="max-width: 680px">

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <!--<h5 class="modal-title">Upload File</h5>-->
                                                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div><p class="">Choose your defect type (You can choose one defect)</p></div>
                                                    <?php $attribute = array('id'=>'form','class'=>'mt-5 mb-5');
                                                    echo form_open_multipart('admin/uploadTenantData',$attribute)?>
                                                    <?php echo validation_errors('<div class="alert alert-danger alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button><b>Error! </b>', '</div>');
                                                    echo $this->session->flashdata('msg');?>
                                                <div id="msg"></div>
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                
                                                    
                                                    <?php foreach($defectList as $key=>$val){?>
                                                    <div class="col-md-3 col-sm-6">
                                                        <div class=" col-md-12">
                                                            <div class="  defect-categories-list"  >
                                            <label class="btn btn-primary  day-btn defect-categories" data-id="<?php echo $key;?>">
                                                <img src="<?php echo base_url('lib/categoryIcon/'.$val['img']);?>">
                                              <input id="cat_<?php echo $key?>" type="radio" name="defect_cat"  autocomplete="off" value="<?php echo $val['name'];?>" >
                                              <div><?php echo $val['name'];?></div>
                                              
                                            </label>
                                         </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                               
                                                </div>
                                                    </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 cat-level">
                                                        
                                                        
                                                            <div class="col-md-5 col-sm-6">
                                                                <p class=" " data-id="">Level</p>
                                                            <select class="form-control" name="level" id="level">
                                                                <option value="">Select level</option>
                                                                <option ></option>
                                                            </select>
                                                            </div>
                                            
                                                            
                                                        <div class="col-md-5 col-sm-6">
                                                                <p class=" " data-id="">Location</p>
                                                            <select class="form-control" name="level" id="location">
                                                                <option value="">Select location</option>
                                                                <option ></option>
                                                            </select>
                                                            </div>
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 cat-level">
                                                        
                                                        
                                                            <div class="col-md-5 col-sm-6">
                                                                <p class=" " data-id="">Issues</p>
                                                            <select class="form-control" name="level" id="issue">
                                                                <option value="">Select issue</option>
                                                                <option ></option>
                                                            </select>
                                                            </div>
                                            
                                                            
                                                        
                                                        
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                                
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 add-cat-button">
                                                        
                                                        
                                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group div-btn">
                                                <button id="addDefectList" data-value="<?php echo $this->security->get_csrf_hash();?>" type="button" class="button">Add to Defect list</button>
                                            </div>
                                                            </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
</div>
<script>
    var catData = <?php $level =  defectLevel(); echo json_encode($level);?>
</script>
<script src="<?php echo base_url('lib/admin/js/action.js?version='.date('his'));?>"></script>

