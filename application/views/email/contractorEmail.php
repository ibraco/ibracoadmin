<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<!--[if gte mso 9]>
	<xml>
		<o:OfficeDocumentSettings>
		<o:AllowPNG/>
		<o:PixelsPerInch>96</o:PixelsPerInch>
		</o:OfficeDocumentSettings>
	</xml>
	<![endif]-->
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="format-detection" content="date=no" />
	<meta name="format-detection" content="address=no" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="x-apple-disable-message-reformatting" />
    <!--[if !mso]><!-->
	<link href="https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i" rel="stylesheet" />
    <!--<![endif]-->
	<title>Approved / Reject email</title>
	<!--[if gte mso 9]>
	<style type="text/css" media="all">
		sup { font-size: 100% !important; }
	</style>
	<![endif]-->
	

	<style type="text/css" media="screen">
		/* Linked Styles */
		body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f4f4f4; -webkit-text-size-adjust:none }
		a { color:#777777; text-decoration:none }
		p { padding:0 !important; margin:0 !important } 
		img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
		.mcnPreviewText { display: none !important; }

		.cke_editable,
		.cke_editable a,
		.cke_editable span,
		.cke_editable a span { color: #000001 !important; }		
		/* Mobile styles */
		@media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
			.mobile-shell { width: 100% !important; min-width: 100% !important; }
			.bg { background-size: 100% auto !important; -webkit-background-size: 100% auto !important; }
			
			.text-header,
			.m-center { text-align: center !important; }
			
			.center { margin: 0 auto !important; }
			.container { padding: 20px 10px !important }
			
			.td { width: 100% !important; min-width: 100% !important; }

			.m-br-15 { height: 15px !important; }
			.p30-15 { padding: 30px 15px !important; }
			.p0-15-30 { padding: 0px 15px 30px 15px !important; }
			.p0-15 { padding: 0px 15px !important; }
			.pt0 { padding-top: 0px !important; }
			.mpb30 { padding-bottom: 30px !important; }
			.mpb15 { padding-bottom: 15px !important; }

			.m-td,
			.m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

			.m-block { display: block !important; }

			.fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }

			.column,
			.column-dir,
			.column-top,
			.column-empty,
			.column-empty2,
			.column-dir-top { float: left !important; width: 100% !important; display: block !important; }

			.column-empty { padding-bottom: 30px !important; }
			.column-empty2 { padding-bottom: 10px !important; }

			.content-spacing { width: 15px !important; }
		}
	</style>
</head>
	<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f4f4f4; -webkit-text-size-adjust:none;">
<!--*|IF:MC_PREVIEW_TEXT|*-->
		<!--[if !gte mso 9]><!-->
		<span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">*|MC_PREVIEW_TEXT|*</span>
		<!--<![endif]-->
	<!--*|END:IF|*-->
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
		<tbody><tr>
			<td align="center" valign="top">
				<!-- Header -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tbody><tr>
						<td align="center" class="p30-15" style="padding: 20px;background-color: #ffffff;">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tbody><tr>
									<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
										<!-- Header -->
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tbody><tr>
												<th class="column" width="145" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tbody><tr>
															<td class="img m-center" style="font-size:0pt;line-height:0pt;text-align: center;"><img src="<?php echo base_url('lib/admin/login/images/logo.png');?>" width="200px" height="auto" mc:edit="image_1" style="max-width:106px;" border="0" alt=""></td>
														</tr>
													</tbody></table>
												</th>
												
												
											</tr>
										</tbody></table>
										
									</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
				</tbody></table>
				

				
				<div mc:repeatable="Select" mc:variant="Intro">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f4f4f4">
						<tbody><tr>
							<td align="center">
								<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
									<tbody><tr>
										<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tbody>
												<tr>
													<td class="bbrr" bgcolor="#f4f4f4" style="border-radius:0px 0px 12px 12px;">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tbody><tr>
																<td class="p30-15" style="padding: 50px 30px 60px 30px;">
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tbody><tr>
																			
																		</tr>
																		<tr>
																			<td class="text-center pb25" style="color:#777777;font-family:'Lato', Arial,sans-serif;font-size:17px;line-height:30px;text-align: left;padding-bottom: 9px;"><div mc:edit="text_3">Dear <?php echo $contractorName?>,<br><br>					
					
<span style="
    font-weight: bold;
"><?php echo (isset($appointmentType)&&$appointmentType=='defect')?'Defect': 'Handover';?> Appointment:</span><br>					
					
<span>
	<?php echo isset($defect)&&($appointmentType=='defect')?'Defect :'.$defect:''?><br>
Unit No			:<?php echo $unitNumber;?><br>					
Owner Name		:<?php echo $ownerName;?><br>				
Date			:<?php echo $appoinmentDate;?><br>				
Time			:<?php echo $appoinmentTime;?><br>
</span>

<br>
					

					
					</div></td>
																		</tr>
																		<!-- Button -->
																		<tr mc:hideable="">
																			<td>
																				<table class="center" border="0" cellspacing="0" cellpadding="0" style="text-align: left;">
																					<tbody><tr>
																						


																					</tr>
																				</tbody></table>
																			</td>
																		</tr>
																		<!-- END Button -->
																	</tbody></table>
																</td>
															</tr>
														</tbody></table>
													</td>
												</tr>
											</tbody></table>
											<!-- END Intro -->
										</td>
									</tr>
								</tbody></table>
							</td>
						</tr>
					</tbody></table>
				</div>

				
				
				

				
				
				

				
				
				

				
				
				

				
				
				

				
				
				
			
				
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FF0000">
					<tbody><tr>
						<td valign="top" align="center" class="p30-15" style="padding: 25px 0px 25px 0px;">
							<table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
								<tbody><tr>
									<td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tbody><!--<tr>
												<td align="center" style="padding-bottom: 30px;">
													<table border="0" cellspacing="0" cellpadding="0">
														<tbody><tr>
															<td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="images/t7_ico_facebook.jpg" width="34" height="34" mc:edit="image_23" style="max-width:34px;" border="0" alt=""></a></td>
															<td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="images/t7_ico_twitter.jpg" width="34" height="34" mc:edit="image_24" style="max-width:34px;" border="0" alt=""></a></td>
															<td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="images/t7_ico_instagram.jpg" width="34" height="34" mc:edit="image_25" style="max-width:34px;" border="0" alt=""></a></td>
															<td class="img" width="34" style="font-size:0pt; line-height:0pt; text-align:left;"><a href="#" target="_blank"><img src="images/t7_ico_linkedin.jpg" width="34" height="34" mc:edit="image_26" style="max-width:34px;" border="0" alt=""></a></td>
														</tr>
													</tbody></table>
												</td>
											</tr>-->
											<tr>
												<td class="text-footer1 pb10" style="color: #ffffff;font-family:'Lato', Arial,sans-serif;font-size:14px;line-height:20px;text-align:center;padding-bottom:10px;"><div mc:edit="text_44">Copyright © 2019 Ibraco, All rights reserved.</div></td>
											</tr>
											<tr>
												<td class="text-footer2 pb20" style="color: #ffffff;font-family:'Lato', Arial,sans-serif;font-size:12px;line-height:26px;text-align:center;padding-bottom:20px;"><div mc:edit="text_45">Our mailing address is:
&nbsp;customerservice@ibraco.com

</div>
												<div mc:edit="text_45">System auto generated email.</div></td>
											</tr>
											
										</tbody></table>
									</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
				</tbody></table>
				
			</td>
		</tr>
	</tbody></table>


</body>
</html>
