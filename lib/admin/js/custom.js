var success1 = '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
var success2 = '</div>';
var fail1 = ' <div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
var fail2 = '</div>';
$(function () {
    
    /*
*@function: function to add vehicle

*/
  var $modal1 = $('#load_popup_modal1_show_id');
$('#importTenant').on('click', function(){ 
      
        var datavalue1 = ($(this).attr('data-value' ));

        $modal1.load(actionUrl+'importTenant/',{'form-no':datavalue1}, 
          function(){ $modal1.modal('show'); 
          }); 

        });

    $('.addAdminSetting').on('click',function(){
        
        var datavalue1 = ($(this).attr('data-value' ));
        var calenderType = $(this).attr('data-info' );
        var calenderSettingId = $(this).attr('data-id' );

        $modal1.load(actionUrl+'calenderSetting/',{'form-no':datavalue1,'calenderType':calenderType,'calenderSettingId':calenderSettingId}, 
          function(){ $modal1.modal('show'); 
          }); 

        });
       
       
$('.addEmail').on('click',function(){
        
        var datavalue1 = ($(this).attr('data-value' ));
        
        var emailId = $(this).attr('data-id' );

        $modal1.load(actionUrl+'addEmail/',{'form-no':datavalue1,'emailId':emailId}, 
          function(){ $modal1.modal('show'); 
          }); 

        });
$('#emailTable').on('click','a.addEmail', function () {
  
 var datavalue1 = ($(this).attr('data-value' ));
        
        var emailId = $(this).attr('data-id' );

        $modal1.load(actionUrl+'addEmail/',{'form-no':datavalue1,'emailId':emailId}, 
          function(){ $modal1.modal('show'); 
          }); 
  });
        
        
$('#emailTable').on('click','a.removeEmail', function () {
  
  
if(confirm('Are you sure, you want to delete this email?'))
{
  var datavalue1 = ($(this).attr('data-value' ));
        
  var emailId = $(this).attr('data-id' );
  
  $.ajax({
    
    url:actionUrl+'removeEmail',
    type:'POST',
    data:{'emailId':emailId,'form-no':datavalue1},
    success:function()
    {
      
      var uTable = $('#emailTable').dataTable();
      uTable.fnStandingRedraw();
    }
    });
}
});

$('#example-ajax').on('click','a.edituser',function(){
  var datavalue1 = ($(this).attr('data-value' ));
        
  var customerId = $(this).attr('data-id' );
   $modal1.load(actionUrl+'addCustomer/',{'form-no':datavalue1,'customerId':customerId}, 
          function(){ $modal1.modal('show'); 
          }); 
  
  });

  $('#example-ajax').on('click','a.removeUser',function(){
    
    if(confirm('Are you sure, you want to delete this tenant?'))
{
  var datavalue1 = ($(this).attr('data-value' ));
        
  var customerId = $(this).attr('data-id' );
  
  $.ajax({
    
    url:actionUrl+'removeTenant',
    type:'POST',
    data:{'customerId':customerId,'customerStatus':'delete','form-no':datavalue1},
    success:function()
    {
      
      var uTable = $('#example-ajax').dataTable();
      uTable.fnStandingRedraw();
    }
    });
}
    });
  
  $('#resetpassword-link').on('click',function(){
   
   $('#resetpassword-form').css('opacity','unset');
   });
  
  $('#resetpassword-btn').on('click',function(){
   
   var oldpassword = $('#oldpassword').val();
   var newpassword = $('#newpassword').val();
   var cpassword = $('#cpassword').val();
   var oldpassword_error=true;
   var newpassword_error = true;
   if(oldpassword=='')
   {
     $('#oldpassword_error').html('Please enter Old password.');
     oldpassword_error =true;
   }else
   {
    $('#oldpassword_error').html('');
     oldpassword_error = false;
   }
   if(newpassword=='')
   {
     $('#newpassword_error').html('Please enter new password.');
     newpassword_error =true;
   }else
   {
    $('#newpassword_error').html('');
     newpassword_error = false;
   }
   if(cpassword=='')
   {
     $('#cpassword_error').html('Please enter confirm password.');
     cpassword_error =true;
   }else if(newpassword!=cpassword)
   {
    $('#cpassword_error').html('New password & confirm password should be match.');
     cpassword_error =true;
   }else
   {
    $('#cpassword_error').html('');
     cpassword_error = false;
   }
   console.log(oldpassword_error+'&&'+newpassword_error+'&&!'+cpassword_error);
   if(!oldpassword_error&&!newpassword_error&&!cpassword_error)
   {
    $.ajax({
     url:actionUrl+'resetpassword',
     type:'post',
     data:{'oldpassword':oldpassword,'newpassword':newpassword,'cpassword':cpassword,'form-no':csrfCode},
     success:function(response)
     {
      var data = JSON.parse(response);
      
      if(data.status=='fail')
      {
       if(data.type=='oldpassword')
       {
         $('#oldpassword_error').html('Please enter correct Old password.');
       }
      }else if(data.status=='success')
      {
        $('.msg').html(success1+'Password updated successfully.'+success2);
        $('#oldpassword').val('');
        $('#newpassword').val('');
         $('#cpassword').val('');
        
      }
     }
     
     });
   }
   });
  
  $('.submit-handover').on('click',function(){
   
   var appoinmentDate = $('#appoinmentDate').val();
   var appoinmentTime = $('input[type="radio"]:checked').val();
   var notes = $('#notes').val();
   var appointmentId = $('#appointmentId').val();
    
   if(appoinmentDate=='')
   {
      $('#appoinmentDateError').html('Please select date.');
   }else if(appoinmentTime ==undefined|| appoinmentTime==''){
    
    $('#appoinmentTimeError').html('Please select time.');
   }else
   {
    $('#preloader').css('display','block');
    $.ajax({
     
     url:actionUrl+'submitHandoverRequest',
     type:'POST',
     data:{'appointmentId':appointmentId,'appoinmentDate':appoinmentDate,'appoinmentTime':appoinmentTime,'notes':notes,'form-no':$(this).attr('data-value')},
     success:function(response)
     {
       var data = JSON.parse(response);
       
      if(data.status=='success')
      {
       
        $('.msg').html(success1+data.message+success2);
        if(appointmentId=='')
        window.location.href=actionUrl+'dashboard';
        
      }else if(data.status=='fail')
      {
       $('.msg').html(fail1+data.message+fail2);
      }
      
      $('#preloader').css('display','none');
     }
     
     });
   }
   
   });
  $('.submit-defect').on('click',function(){
   
   var appoinmentDate = $('#appoinmentDate').val();
   var appoinmentTime = $('input[name="appointmentTime"]:checked').val();
   var notes = $('#notes').val();
   console.log($('#commentfile').prop('files'));
   var file_data = $('#commentfile').prop('files')[0];
        var form_data = new FormData();  // Create a FormData object
        form_data.append('file', file_data);  // Append all element in FormData  object
        form_data.append('form-no',$(this).attr('data-value'));
       
        form_data.append('appoinmentDate',appoinmentDate);
        form_data.append('appoinmentTime',appoinmentTime);
        form_data.append('notes',notes);
        form_data.append('category',category);
        form_data.append('level',level);
        form_data.append('location',location);
        form_data.append('issue',issue);
   //$('input[name="category[]"]').each(function(e){
   // 
   // var category[] = ($(this).val());
   // });
   var category=$('input[name="category[]"]').map(function(){
        return $(this).val();
    }).get();
   var level=$('input[name="level[]"]').map(function(){
        return $(this).val();
    }).get();
   var location =$('input[name="location[]"]').map(function(){
        return $(this).val();
    }).get();
   var issue=$('input[name="issue[]"]').map(function(){
        return $(this).val();
    }).get();
   var appointmentId = $('#appointmentId').val();
   
    form_data.append('appointmentId',appointmentId);
    form_data.append('category',category);
    form_data.append('level',level);
    form_data.append('location',location);
    form_data.append('issue',issue);
    
   if(appoinmentDate=='')
   {
      $('#appoinmentDateError').html('Please select date.');
   }else if(appoinmentTime ==undefined|| appoinmentTime==''){
    
    $('#appoinmentDateError').html('');
    $('#appoinmentTimeError').html('Please select time.');
   }else if(category=='')
   {
     $('#appoinmentTimeError').html('');
    $('#categoryError').html('Please select defect category.');
   }
   else
   {
    $('#preloader').css('display','block');
    $.ajax({
     
     url:actionUrl+'submitDefectRequest',
     dataType    : 'text',           // what to expect back from the PHP script, if anything
     cache       : false,
     contentType : false,
     processData : false,
     type:'POST',
     data:form_data,
     success:function(response)
     {
       var data = JSON.parse(response);
       
      if(data.status=='success')
      {
       
        $('.msg').html(success1+data.message+success2);
        if(appointmentId=='')
        window.location.href=actionUrl+'dashboard';
        
      }else if(data.status=='fail')
      {
       $('.msg').html(fail1+data.message+fail2);
      }
      
      $('#preloader').css('display','none');
     }
     
     });
   }
   
   });
  
  $('.admin-submit-defect').on('click',function(){
   
   var appoinmentDate = $('#appoinmentDate').val();
   var appoinmentTime = $('input[name="appointmentTime"]:checked').val();
   var unitNumber = $('#unitNumber').val();
   var notes = $('#notes').val();
   var contractorId = $('#contractorId').val();
   //$('input[name="category[]"]').each(function(e){
   // 
   // var category[] = ($(this).val());
   // });
   var category=$('input[name="category[]"]').map(function(){
        return $(this).val();
    }).get();
   var level=$('input[name="level[]"]').map(function(){
        return $(this).val();
    }).get();
   var location =$('input[name="location[]"]').map(function(){
        return $(this).val();
    }).get();
   var issue=$('input[name="issue[]"]').map(function(){
        return $(this).val();
    }).get();
   var appointmentId = $('#appointmentId').val();
   
   console.log(appoinmentTime);
  //for(var i=0;i<=category.length;i++)
  //{
  //  
  //}
   if(unitNumber=='')
   {
    $('#unitNumberError').html('Please select unit number.');
   }
   else if(appoinmentDate=='')
   {
      $('#appoinmentDateError').html('Please select date.');
   }else if(appoinmentTime ==undefined|| appoinmentTime==''){
    
    $('#appoinmentTimeError').html('Please select time.');
   }else if(category=='')
   {
    $('#categoryError').html('Please select defect category.');
   }else
   {
    $('#preloader').css('display','block');
    $.ajax({
     
     url:actionUrl+'submitDefectRequest',
     type:'POST',
     data:{'appointmentId':appointmentId,'appoinmentDate':appoinmentDate,'appoinmentTime':appoinmentTime,'notes':notes,'form-no':$(this).attr('data-value'),'category':category,'level':level,'location':location,'issue':issue,'unitNumber':unitNumber,'contractorId':contractorId},
     success:function(response)
     {
       var data = JSON.parse(response);
       
      if(data.status=='success')
      {
       
        $('.msg').html(success1+data.message+success2);
        if(appointmentId=='')
        window.location.href=actionUrl+'defect';
        
      }else if(data.status=='fail')
      {
       $('.msg').html(fail1+data.message+fail2);
      }
      
      $('#preloader').css('display','none');
     }
     
     });
   }
   
   });
  
  $('.removeAppointment').on('click',function(){
   
   if(confirm("Are you sure, you want to delete this record?"))
   {
     var URL = $(this).attr('href');
     var value = $(this).attr('data-value');
     $.ajax({
      url:URL,
      type:'POST',
      data:{'form-no':value},
      success: function(response){
       window.location.href=actionUrl+"dashboard";
      }
      
      });
   }
   
   });
  
  $('.rejectAppointment').on('click',function(){
  
   if(confirm("Are you sure, you want to reject this appointment?"))
   {
     var URL = $(this).attr('href');
     var value = $(this).attr('data-value');
     var id = $(this).attr('data-id');
     $.ajax({
      url:URL,
      type:'POST',
      data:{'form-no':value,'id':id},
      success: function(response){
       window.location.href=actionUrl+"viewTenant/"+id;
      }
      
      });
   }
   
   });
  
  $('.noti-status').on('click',function(){
   
   if(confirm("Are you sure, you want to accept this appointment?"))
   {
    
     var value = $(this).attr('data-value');
     var id = $(this).attr('data-id');
     var ntotiId = $(this).attr('data-info');
     var URL = actionUrl+'acceptAppointment/'+id+'/accept';
     $.ajax({
      url:URL,
      type:'POST',
      data:{'form-no':value,'notificationId':ntotiId},
      success: function(response){
       window.location.href=actionUrl+"notification/";
      }
   
   
   });
   }
  });
  
  $('#exportExcel').on('click',function(){
   var status = $('#status-filter').val();
   var date = $('#date-filter').val();
   window.location=baseurl+"admin/excelLoad?status="+status+"&date="+date;
   });

   $('#example-ajax').on('click','a.sentEmail',function(){
    
    var id = $(this).attr('data-id');
    var value = $(this).attr('data-value');
    if(confirm('Do you want to sent email?')){
    $('#preloader').css('display','block');
    $.ajax({
     
     url:actionUrl+'sendEmail',
     type:'POST',
     data:{'id':id,'form-no':value},
     success:function()
     {
      var uTable = $('#example-ajax').dataTable();
      uTable.fnStandingRedraw();
      $('#preloader').css('display','none');
     }
     
     });
    }
    
    });
   
   $('.btn-defect-list').on('click', function(){ 
      
        var datavalue1 = ($(this).attr('data-value' ));

        $modal1.load(actionUrl+'defectList/',{'form-no':datavalue1}, 
          function(){ $modal1.modal('show'); 
          }); 

        });
   
   
   $('#defectList').on('click','a.deleteAppointment',function(){
    
    if(confirm('Are you sure to remove this defect?'))
    {
       $(this).closest('div.row').remove();
    }
    });
   
   $('#exportTenant').on('click',function(){
  
   window.location=baseurl+"admin/exportTenant";
   });

   $('#cat-filter').on('change',function(){
$('#issue').css('display','inline-block')
    var value = $(this).val();
    var valueData = catData[value];
    var issueData = valueData['issues'];
    var issueHtml = '<option value="">Select issue</option>';
    for(key in issueData)
      {
         
         issueHtml+='<option value="'+issueData[key]+'">'+issueData[key]+'</option>';
      }

     
       $('#issue').html(issueHtml);
   })
   
   $('.gallery1').on('click','a.commentImageDelete',function(){
    
    var href = $(this).attr('data-info');
    
  if(confirm('Do you want to delete this image?'))
  {
     $.ajax({
      url:actionUrl+'deleteCommentImage',
      type:'POST',
      data:{'url':href,'form-no':csrfCode},
      success:function(response)
      {
         var data = JSON.parse(response);
         if(data.status=='success')
         {
           location.reload();
         }
      }
      
      });
  }
    })
});
/*
@function: Called to reload the table on active/inactive the record. 

*/
$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
    //redraw to account for filtering and sorting
    // concept here is that (for client side) there is a row got inserted at the end (for an add)
    // or when a record was modified it could be in the middle of the table
    // that is probably not supposed to be there - due to filtering / sorting
    // so we need to re process filtering and sorting
    // BUT - if it is server side - then this should be handled by the server - so skip this step
    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        //iDisplayStart has been reset to zero - so lets change it back
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }
      
    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};