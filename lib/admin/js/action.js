var success1 = '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
var success2 = '</div>';
var fail1 = ' <div class="alert alert-danger alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
var fail2 = '</div>';

$(function(){
   // Input radio-group visual controls
    $('.defect-categories-list label').on('click', function(){
     //var value = $(this).attr('data-id');
     var id = $(this).attr('data-id');
     console.log(catData);
     console.log($('#cat_'+id).prop('checked'));
     if($('#cat_'+id).prop('checked')){
      
      var value = $('#cat_'+id).val();
    
      var valueData = catData[value];
      var levelData = valueData['level'];
      var locationData = valueData['location'];
      var issueData = valueData['issues'];
      console.log(levelData);
      var levelhtml = '<option value="">Select level</option>';
      var issueHtml = '<option value="">Select issue</option>';
      var locationHtml = '<option value="">Select location</option>';
      for(key in levelData)
      {
         
          
         levelhtml+='<option value="'+levelData[key]+'">'+levelData[key]+'</option>';
      }
      for(key in locationData)
      {
         
          
         locationHtml+='<option value="'+locationData[key]+'">'+locationData[key]+'</option>';
      }
      for(key in issueData)
      {
         
         issueHtml+='<option value="'+issueData[key]+'">'+issueData[key]+'</option>';
      }
      $('#level').html(levelhtml);
      $('#location').html(locationHtml);
      $('#issue').html(issueHtml);
      
      $('.defect-categories-list label').removeClass('active ');
         $(this).addClass('active ');
     }else
     {
      
        $(this).removeClass('active ');
     }
     
        
    });
   
   $('#addDefectList').on('click',function(){
      
      var cat='';
      //console.log($("input[name='defect_cat']:checked").val());
      
         cat = $("input[name='defect_cat']:checked").val();
      
      var level = $('#level').val();
      var location = $('#location').val();
      var issue = $('#issue').val();
      
      console.log(cat+'='+level+'='+location+'='+issue);
      if(cat =='')
      {
         $('#msg').html(fail1+'Please select category'+fail2);
      }else if(level=='')
      {
          $('#msg').html(fail1+'Please select level'+fail2);
      }
      else if(issue=='')
      {
          $('#msg').html(fail1+'Please select issue'+fail2);
      }else {
         
         //$.ajax({
         //   url:actionUrl+'addCategory',
         //   type:'POST',
         //   data:{'category':cat,'level':level,'location':location,'issue':issue,'form-no':csrfCode},
         //   success:function(response)
         //   {
         //      
         //   }
         //   })
         var categoryValue =[];
         $('input[name="categoryValue"]').each(function(){
         
          categoryValue.push($(this).val());
         });
         
         var currentvalue = cat+', '+level+', '+location+', '+issue;
         var err=false;
         $.each(categoryValue,function(index,value){
            
            if(value==currentvalue)
            {
               $('#msg').html(fail1+'You cannot select same defect.'+fail2);
               err=true;
            }
            });
         if(!err){
      var html = '';
      
      html+='<div class="row" id=""><div class="col-md-12 col-sm-12 defect-list"><div class="col-md-10"><input type="hidden" name="category[]" value="'+cat+'"><input type="hidden" name="level[]" value="'+level+'"><input type="hidden" name="location[]" value="'+location+'"><input type="hidden" name="issue[]" value="'+issue+'"><input type= "hidden" name="categoryValue" value="'+cat+', '+level+', '+location+', '+issue+'">'+cat+', '+level+', '+location+', '+issue+'</div><div class="col-md-2"><a href="javascript:void(0)" class="deleteAppointment"><i class="fa fa-trash-o" style="font-size:17px;color:red"></i></a></div></div></div>';
      
      $('#defectList').append(html);
      $('#load_popup_modal1_show_id').modal('hide');
      $('#categoryError').html('');
         }
      }
      
      
      });
   
   $('#submitEmail').on('click',function(){
    
    var value = $(this).attr('data-value');
    var contactName = $('#contactName').val();
    var categories = $('#categories').val();
    var email = $('#email').val();
    var departmentEmailId = $('#departmentEmailId').val();
    if(contactName=='')
    {
        $('#msg').html(fail1+'Please enter contact name.'+fail2);
    }else if(categories=='')
    {
        $('#msg').html(fail1+'Please select categories.'+fail2);
    }else if(email=='')
    {
        $('#msg').html(fail1+'Please enter email.'+fail2);
    }else
    {
        $.ajax({
            
            url:'addDepartmentEmail',
            data:{'contactName':contactName,'categories':categories,'email':email,'departmentEmailId':departmentEmailId,'form-no':value},
            type:'POST',
            success:function(response)
            {
                var data = JSON.parse(response);
                        if(data.status == 'success'){
                            var message = success1+data.message+success2;
                            $("#msg").html(message);
                            if(departmentEmailId==''){
                            $('#contactNumber').val('');
                            $('#email').val('');
                            
                            
                            }
                            var uTable = $('#emailTable').dataTable();
                            uTable.fnStandingRedraw();
                        }else{
                        var message = fail1+data.message+fail2;
                        $("#msg").html(message);
                    }
            }
            
            });
    }
    
    
    });
   
   $('#submitCustomer').on('click', function(){
      
      var value = $(this).attr('data-value');
      var customerId = $('#customerId').val();
      var customerName = $('#customerName').val().trim();
      var unitNumber = $('#unitNumber').val().trim();
      var IC_no = $('#IC_no').val().trim();
      var customerMobile = $('#customerMobile').val().trim();
      var ownerName = $('#ownerName').val().trim();
      var customerAlternatecontactName = $('#customerAlternatecontactName').val().trim();
      var customerAlternateMobile = $('#customerAlternateMobile').val().trim();
      var customerEmail = $('#customerEmail').val().trim();
      var authorized_person = $('#authorized_person').val().trim();
      var authorized_person_contact_no = $('#authorized_person_contact_no').val().trim();
      var vacant_possession_letter_date = $('#vacant_possession_letter_date').val().trim();
      var occupation_permit_date = $('#occupation_permit_date').val().trim();
      var handover_date = $('#handover_date').val().trim();
      var defect_liability_expiry_date = $('#defect_liability_expiry_date').val().trim();
      
       var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
       var mobileRegex = /^(\+?6?01)[0|1|2|3|4|6|7|8|9]\-*[0-9]{7,8}$/;
      
      // if(customerName=='')
      // {
      //    $('#msg').html(fail1+'Please enter customer name.'+fail2);
      // }else if(unitNumber=='')
      // {
      //    $('#msg').html(fail1+'Please enter unit number.'+fail2);
      // }else if(IC_no=='')
      // {
      //    $('#msg').html(fail1+'Please enter I/C number.'+fail2);
      // }else if(customerMobile=='')
      // {
      //    $('#msg').html(fail1+'Please enter Primary number.'+fail2);
      // }else
       if(customerMobile!=''&&!mobileRegex.test(customerMobile))
      {
         $('#msg').html(fail1+'Please enter valid Primary number(+60xxxxxxxxx).'+fail2);
       }//else if(customerAlternatecontactName=='')
      // {
      //    $('#msg').html(fail1+'Please enter Emergency contact.'+fail2);
      // }else if(customerAlternateMobile=='')
      // {
      //    $('#msg').html(fail1+'Please enter Emergency contact number.'+fail2);
      // }else
       if(customerAlternateMobile!=''&&!mobileRegex.test(customerAlternateMobile))
      {
         $('#msg').html(fail1+'Please enter valid Emergency contact number(+60xxxxxxxxx).'+fail2);
       }//else if(customerEmail=='')
      // {
      //    $('#msg').html(fail1+'Please enter email.'+fail2);
      // }else
       if(customerEmail!=''&&!regex.test(customerEmail)) {
         $('#msg').html(fail1+'Please enter valid email.'+fail2);
      }
      /*else if(authorized_person=='')
      {
         $('#msg').html(fail1+'Please enter authorized person.'+fail2);
      }
      else if(authorized_person_contact_no=='')
      {
         $('#msg').html(fail1+'Please enter authorized person contact no.'+fail2);
      }*/
      // else if(vacant_possession_letter_date=='')
      // {
      //    $('#msg').html(fail1+'Please enter vacant possession letter date.'+fail2);
      // }
      // else if(occupation_permit_date=='')
      // {
      //    $('#msg').html(fail1+'Please enter occupation permit date.'+fail2);
      // }
      // else if(handover_date=='')
      // {
      //    $('#msg').html(fail1+'Please enter handover date.'+fail2);
      // }else if(defect_liability_expiry_date=='')
      // {
      //    $('#msg').html(fail1+'Please enter defect liability expiry date.'+fail2);
      // }
      else
      {
         $.ajax({
            url:actionUrl+'submitCustomer',
            type:'POST',
            data:{'customerId':customerId,'customerName':customerName,'unitNumber':unitNumber,'IC_no':IC_no,'customerMobile':customerMobile,'customerAlternatecontactName':customerAlternatecontactName,'customerAlternateMobile':customerAlternateMobile,'customerEmail':customerEmail,'defect_liability_expiry_date':defect_liability_expiry_date,'handover_date':handover_date,'occupation_permit_date':occupation_permit_date,'vacant_possession_letter_date':vacant_possession_letter_date,'authorized_person_contact_no':authorized_person_contact_no,'authorized_person':authorized_person,'ownerName':ownerName,'form-no':value},
            success:function(response){
               
               var data = JSON.parse(response);
                        if(data.status == 'success'){
                            var message = success1+data.message+success2;
                            $("#msg").html(message);
                            if(customerId==''){
                            $('#customerName').val('');
                            $('#unitNumber').val('');
                           $('#IC_no').val('');
                           $('#customerMobile').val('');
      
                           $('#customerAlternatecontactName').val('');
                           $('#customerAlternateMobile').val('');
                           $('#customerEmail').val('');
                            
                            
                            }
                            var uTable = $('#example-ajax').dataTable();
                            uTable.fnStandingRedraw();
                            $('#load_popup_modal1_show_id').modal("hide");
                        }else{
                        var message = fail1+data.message+fail2;
                        $("#msg").html(message);
                        }
               
            }
            
            });
      }
      
      });
    });
/*
@function: Called to reload the table on active/inactive the record. 

*/
$.fn.dataTableExt.oApi.fnStandingRedraw = function(oSettings) {
    //redraw to account for filtering and sorting
    // concept here is that (for client side) there is a row got inserted at the end (for an add)
    // or when a record was modified it could be in the middle of the table
    // that is probably not supposed to be there - due to filtering / sorting
    // so we need to re process filtering and sorting
    // BUT - if it is server side - then this should be handled by the server - so skip this step
    if(oSettings.oFeatures.bServerSide === false){
        var before = oSettings._iDisplayStart;
        oSettings.oApi._fnReDraw(oSettings);
        //iDisplayStart has been reset to zero - so lets change it back
        oSettings._iDisplayStart = before;
        oSettings.oApi._fnCalculateEnd(oSettings);
    }
      
    //draw the 'current' page
    oSettings.oApi._fnDraw(oSettings);
};